/**
 * Module dependencies.
 */
var express = require("express"),
    engine = require("ejs-locals"),
    util = require("util"),
    http = require("http"),
    request = require('request'),
    path = require("path"),
    fs = require('fs'),
    async = require('async'),
    config = require('annulet-config'),
    _ = require('lodash'),
    mo = require('method-override'),
    cors = require('cors'),
    compression = require('compression'),
    routers = require('./routers'),
    useragent = require('express-useragent');

var logger = require('winston');

process.on('uncaughtException', config.middleware.uncaughtException);

var app = express();

logger.info('starting application...');
app.engine("ejs", engine);
app.set("port", process.env.PORT || config.configuration.ports.uiPort());
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(require('body-parser')());
app.use(mo('X-HTTP-Method-Override'));
app.use(cors());
app.use(compression());
app.use(express.static(path.join(__dirname, "theme")));
app.use('/client_app', express.static(path.join(__dirname, 'client_app')));
app.use('/signup_app', express.static(path.join(__dirname, 'signup_app')));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components')));
app.use(require('errorhandler')());

app.use(function(req, res, next) {
    logger.log('info', '', {
        method: req.method,
        url: req.originalUrl
    });
    next();
});


app.use(useragent.express());

var server = http.createServer(app)
    .listen(app.get("port"), function() {
        logger.info("Express server listening on port " + app.get("port"));
    });

app.use('/', config.middleware.clientRequireSetup.client, routers.clientSetup);
app.use('/views/error', config.middleware.clientRequireSetup.client, routers.errorViews);
app.use('/views', config.middleware.clientRequireSetup.client, routers.views);
app.use('/signup', config.middleware.clientRequireSetup.signup, routers.signup);
app.use('/health', routers.health);
app.use('/status', routers.health);

app.use(/\/signup\/.*/, function(req, res, next) {
        logger.silly('signup splat hit');
        next();
    },
    routers.signup);

app.use(/.*/, function(req, res, next) {
        logger.silly('splat splat hit');
        next();
    },
    routers.clientSetup);
