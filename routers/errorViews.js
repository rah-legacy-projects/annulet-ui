var express = require('express'),
    logger = require('winston'),
    loggedIn = require('annulet-config').middleware.loggedIn,
    controllerContext = require('../controllers');

var errorViewRouter = express.Router();
errorViewRouter.get('/unauthorized', controllerContext.views.error.unauthorized);
errorViewRouter.get('/notFound', controllerContext.views.error.notFound);
errorViewRouter.get('/nonpayment', controllerContext.views.error.nonpayment);
errorViewRouter.get('/conflict', controllerContext.views.error.conflict);
errorViewRouter.get('/general', controllerContext.views.error.general);
errorViewRouter.get('/notImplemented', controllerContext.views.error.notImplemented);
errorViewRouter.get('/unavailable', controllerContext.views.error.unavailable);
errorViewRouter.get('/timeout', controllerContext.views.error.timeout);
module.exports = exports = errorViewRouter;
