var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers');

var signupSetupRouter = express.Router();
signupSetupRouter.get('/', controllerContext.signup.home);
signupSetupRouter.get('/apiuri', controllerContext.clientSetup.apiUri);
signupSetupRouter.get('/domain', controllerContext.clientSetup.domain);
signupSetupRouter.get('/authuri', controllerContext.clientSetup.authUri);
module.exports = exports = signupSetupRouter;
