var express = require('express'),
    logger = require('winston'),
    controllerContext = require('../controllers');

var clientSetupRouter = express.Router();
clientSetupRouter.get('/', controllerContext.clientSetup.home);
clientSetupRouter.get('/apiuri', controllerContext.clientSetup.apiUri);
clientSetupRouter.get('/domain', controllerContext.clientSetup.domain);
clientSetupRouter.get('/authuri', controllerContext.clientSetup.authUri);
clientSetupRouter.get('/stripekey', controllerContext.clientSetup.stripePublicKey);
module.exports = exports = clientSetupRouter;
