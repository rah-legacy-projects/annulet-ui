var express = require('express'),
    logger = require('winston'),
    middleware = require('annulet-config')
    .middleware,
    util = require('util'),
    controllerContext = require('../controllers');

var middlewareChain = [
    middleware.loggedIn,
    middleware.navigationActivityTracker,
    middleware.customerValid,
    middleware.paymentRequired
];

logger.silly('middleware: ' + util.inspect(middleware));
var viewRouter = express.Router();


viewRouter.get('/admin/operatingProcedure/list', middlewareChain, controllerContext.views.admin.operatingProcedure.list);
viewRouter.get('/admin/auxiliaryDocument/list', middlewareChain, controllerContext.views.admin.auxiliaryDocument.list);
viewRouter.get('/admin/auxiliaryDocument/detail', middlewareChain, controllerContext.views.admin.auxiliaryDocument.detail);
viewRouter.get('/admin/alert/list', middlewareChain, controllerContext.views.admin.alert.list);
viewRouter.get('/admin/alert/detail', middlewareChain, controllerContext.views.admin.alert.detail);

viewRouter.get('/dashboard', middlewareChain, controllerContext.views.dashboard.get);
viewRouter.get('/workflow/detail', middlewareChain, controllerContext.views.workflow.detail);
viewRouter.get('/workflow/list', middlewareChain, controllerContext.views.workflow.list);
viewRouter.get('/quiz', middlewareChain, controllerContext.views.quiz.get);
viewRouter.get('/quiz/summary', middlewareChain, controllerContext.views.quiz.getSummary);
viewRouter.get('/training', middlewareChain, controllerContext.views.training.get);
viewRouter.get('/operatingProcedure/detail', middlewareChain, controllerContext.views.operatingProcedure.detail);
viewRouter.get('/operatingProcedure/list', middlewareChain, controllerContext.views.operatingProcedure.list);
viewRouter.get('/login', controllerContext.views.auth.login);
viewRouter.get('/customerSelection', middleware.loggedIn, middleware.navigationActivityTracker, controllerContext.views.auth.customerSelection);
viewRouter.get('/userManagement/list', middlewareChain, controllerContext.views.userManagement.list);
viewRouter.get('/userManagement/edit', middlewareChain, controllerContext.views.userManagement.edit);
viewRouter.get('/complaintList', middlewareChain, controllerContext.views.complaint.list);
viewRouter.get('/complaintDetail', middlewareChain, controllerContext.views.complaint.detail);
viewRouter.get('/account', middlewareChain, controllerContext.views.account.account);
viewRouter.get('/auxiliaryDocument/list', middlewareChain, controllerContext.views.auxiliaryDocument.list);
viewRouter.get('/forgotPassword/set', middleware.navigationActivityTracker, controllerContext.views.forgotPassword.setPassword);
viewRouter.get('/forgotPassword/submitted', middleware.navigationActivityTracker, controllerContext.views.forgotPassword.submitted);
viewRouter.get('/forgotPassword/success', middleware.navigationActivityTracker, controllerContext.views.forgotPassword.success);
viewRouter.get('/forgotPassword/forgot', middleware.navigationActivityTracker, controllerContext.views.forgotPassword.forgot);
viewRouter.get('/forgotPassword/cancel', middleware.navigationActivityTracker, controllerContext.views.forgotPassword.cancel);
viewRouter.get('/activation/set', middleware.navigationActivityTracker, controllerContext.views.activation.setPassword);
viewRouter.get('/activation/success', middleware.navigationActivityTracker, controllerContext.views.activation.success);
viewRouter.get('/signup/companyInformation', middleware.navigationActivityTracker, controllerContext.views.signup.companyInformation);
viewRouter.get('/signup/privacy', middleware.navigationActivityTracker, controllerContext.views.signup.privacy);
viewRouter.get('/signup/payment', middleware.navigationActivityTracker, controllerContext.views.signup.payment);
viewRouter.get('/signup/disclaimer', middleware.navigationActivityTracker, controllerContext.views.signup.disclaimer);
viewRouter.get('/signup/ownerInformation', middleware.navigationActivityTracker, controllerContext.views.signup.ownerInformation);
viewRouter.get('/signup/termsOfUse', middleware.navigationActivityTracker, controllerContext.views.signup.termsOfUse);
viewRouter.get('/signup', middleware.navigationActivityTracker, controllerContext.views.signup.signup);
viewRouter.get('/signup/done', middleware.navigationActivityTracker, controllerContext.views.signup.done);
viewRouter.get('/alert/list', middlewareChain, controllerContext.views.alert.instanceList);
viewRouter.get('/alertAdministration/list', middlewareChain, controllerContext.views.alert.definitionList);
viewRouter.get('/alertAdministration/detail', middlewareChain, controllerContext.views.alert.definitionDetail);
viewRouter.get('/errorSampler', middlewareChain, controllerContext.views.errorSampler.get);
viewRouter.get('/problem', middlewareChain, controllerContext.views.problem.get);
viewRouter.get('/itemActivity/adminOutstandingOPs', middlewareChain, controllerContext.views.itemActivity.adminOutstandingOPs);
viewRouter.get('/itemActivity/adminOutstandingTraining', middlewareChain, controllerContext.views.itemActivity.adminOutstandingTraining);
viewRouter.get('/itemActivity/adminOPActivity', middlewareChain, controllerContext.views.itemActivity.adminOPActivity);
viewRouter.get('/itemActivity/adminTrainingActivity', middlewareChain, controllerContext.views.itemActivity.adminTrainingActivity);



module.exports = exports = viewRouter;
