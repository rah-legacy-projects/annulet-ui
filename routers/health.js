var express = require('express'),
    logger = require('winston'),
    config = require('annulet-config'),
    controllerContext = require('../controllers');

var healthRouter = express.Router();
healthRouter.get('/', controllerContext.health.get);
healthRouter.get('/heapdump', config.middleware.internal, controllerContext.health.heapdump);
healthRouter.get('/forceGC', config.middleware.internal, controllerContext.health.forceGC);

module.exports = exports = healthRouter;

