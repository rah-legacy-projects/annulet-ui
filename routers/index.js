module.exports = exports = {
    clientSetup: require("./clientSetup"),
    views: require("./views"),
    signup: require('./signup'),
    errorViews: require('./errorViews'),
    health: require('./health')
};
