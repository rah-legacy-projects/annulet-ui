define(function(require, exports, module) {
    var store = require('store'),
        _ = require('lodash'),
        async = require('async'),
        logger = require('./logger'),
        api = require('./api/api'),
        ko = require('knockout');


    var Chrome = function() {
        var self = this;
        self.processing = ko.observable(true);
        self.error = ko.observable();


        self.handleError = function(options, err, cb) {
            //only require app when needed
            var app = require('./app');
            logger.silly('error received: ' + JSON.stringify(err, null, 4));

            if(!!err.responseJSON){
                self.error(err.responseJSON);
            }
            else if (!!err.responseObject){
                self.error(err.responseObject);
            }
            else if (!!err.responseText) {
                try {
                    self.error(JSON.parse(err.responseText));
                } catch (err) {
                    //eat the json exception
                }
            }

            switch (err.status) {
                case 401:
                case 403:
                    if (!!options && options.isLogin) {
                        cb(err);
                        break;
                    } else {
                        app.router.setLocation('/error/unauthorized');
                        break;
                    }
                case 404:
                    app.router.setLocation('/error/notFound');
                    break;
                case 402:
                    if (!!options && options.isLogin) {
                        cb(err);
                        break;
                    } else {
                        app.router.setLocation('/error/nonpayment');
                    }
                    break;
                case 409:
                case 500:
                case 501:
                case 502:
                case 503:
                case 504:
                    if (!!options && options.isLogin) {
                        cb(err);
                        break;
                    } else {
                        //is error fatal?
                        //if (!!err.fatal) {
                        app.router.setLocation('/error/general');
                        break;
                        //}
                    }
                    //if not, fall through to default case
                default:
                    if (!!cb) {
                        cb(err);
                    } else {
                        app.router.setLocation('/error/general');
                    }
                    break;
            }
        };


    };

    module.exports = exports = new Chrome();

});
