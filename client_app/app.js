define(function(require, exports, module) {
    var sammy = require('sammy'),
        store = require('store'),
        ko = require('knockout'),
        URI = require('URI'),
        _ = require('lodash'),
        models = require('./models/index'),
        routers = require('./routers/index'),
        async = require('async'),
        logger = require('./logger'),
        Chrome = require('./chrome'),
        toastr = require('toastr'),
        api = require('./api/api');

    require('ko.stringTemplateEngine');
    require('./binding_handlers/ko.dropzone');
    require('./binding_handlers/scroll');
    require('./binding_handlers/ko.heightHack');

    var app = new(function() {
        var self = this;

        self.Chrome = Chrome;

        self.initializing = ko.observable(true);

        self.area = ko.observable();


        self.determinedArea = ko.computed(function() {
            if (self.area() == 'userManagement' || self.area() == 'complaint' || self.area() == 'alertAdministration' || self.area() == 'auxiliaryDocuments' || self.area() == 'operatingProcedure') {
                return self.area();
            } else {
                return 'compliance'
            }
        });

        self.setup = function(cb) {
            self.getTemplate = function(value, cb) {
                logger.silly('looking for ' + value);
                if (!ko.templates[value]) {
                    logger.silly('template not found, retrieving');
                    var view = value.split('.')
                        .reduce(function(obj, i) {
                                return obj[i];
                            }, api
                            .views)
                    if (!!view) {
                        logger.silly('view route exists.');
                        view(function(err, templateString) {
                            logger.silly('view retrieved');
                            ko.templates[value] = templateString;
                            cb(null);
                        });
                    } else {
                        logger.warn('view ' + value + ' not found!');
                        cb('notFound');
                    }
                } else {
                    cb(null);
                }

            };

            self.setTemplateName = function(value, cb) {
                logger.debug('[set template] ' + value);
                self.getTemplate(value, function(err) {
                    if (!!err) {
                        self._templateName(err);
                    } else {
                        self._templateName(value);
                    }
                    if (!!cb) {
                        cb(null);
                    }
                });
            };

            self._templateName = ko.observable();
            self.templateName = ko.computed({
                read: function() {
                    var trip = self._templateName();
                    if (!self.initializing.peek()) {
                        logger.silly('retrieving template name: ' + self._templateName());
                        if (!self._templateName()) {
                            logger.warn('template name not defined, going to not found ');
                            self.setTemplateName('notFound');
                        } else {
                            return self._templateName();
                        }
                    }
                    return null;
                },
                write: function(value) {
                    throw Error('Do not attempt to set template name directly, use setTemplateName instead')
                }
            });

            self.router = sammy();

            self.lastLocation = ko.observable('/dashboard');

            self.router.before(function() {
                logger.debug('path: ' + location);
            });

            self.router.around(function(cb) {
                //hack: clear toasts from the page on any page transition.
                toastr.clear();
                cb();
            });

            self.router.around(function(cb) {
                logger.silly('am - running around');
                if (/login$/.test(location)) {
                    logger.silly('*am* skipping login check');
                    cb();
                } else if (/forgot/.test(location)) {
                    logger.silly('*am* forgot... skipping login check');
                    cb();
                } else if(/activation/.test(location)){
                    logger.silly('*am* activation, skipping login check');
                    cb();
                } else {
                    //special case: logout, customerSelection, errors cannot be last visited
                    if (!/(?:logout|customerSelection|error\/\w+)$/.test(this.path)) {
                        logger.silly('setting last path: ' + this.path);
                        self.lastLocation(this.path);
                    }
                    if (!store.get('auth-token')) {
                        logger.silly('*am* auth does not exist, redirect');
                        this.redirect('/login');
                        return false;
                    } else {
                        logger.silly('am - getting user info');
                        if (/customerSelection/.test(this.path)) {
                            logger.silly('*am* auth exists, but need to pick customer');
                            cb();
                        } else {
                            self.auth()
                                .setUpCustomers(false, function(err) {
                                    self.auth()
                                        .getUserInfo(function(err, data) {
                                            logger.silly('am - got user inf');
                                            if (!!err) {
                                                logger.silly('*am* user retrieval caused error, redirect');
                                                this.redirect('/login');
                                                return false;
                                            } else {
                                                logger.silly('*am* auth looks good, continuing');
                                                self.auth()
                                                    .loggedIn(true);
                                                self.auth()
                                                    .showError(false);
                                                cb();
                                            }
                                        });
                                });
                        }
                    }
                }
            });

            self.router.get('/', function() {
                this.redirect('/dashboard');
            });

            self.router.notFound = function() {
                self.router.setLocation('/error/notFound');
            }

            window.onerror = function(err, url, line) {
                self.router.setLocation('/error/general');
            };


            async.series({
                apis: function(cb) {
                    self.admin = {};
                    routers.admin.OperatingProcedure(self);
                    routers.admin.AuxiliaryDocument(self);
                    routers.admin.Alert(self);

                    routers.Dashboard(self);
                    routers.Auth(self);
                    routers.Workflow(self);
                    routers.Quiz(self);
                    routers.Training(self);
                    routers.UserManagement(self);
                    routers.OperatingProcedure(self);
                    routers.Complaint(self);
                    routers.Error(self);
                    routers.ErrorSampler(self);
                    routers.Alert(self);
                    routers.Account(self);
                    routers.AuxiliaryDocument(self);
                    routers.ForgotPassword(self);
                    routers.Activation(self);
                    routers.Problem(self);
                    routers.ItemActivity(self);
                    cb(null);
                },
            }, function(err) {
                logger.silly('initializing complete!');
                logger.silly('running route');
                self.router.run();
                self.initializing(false);
                logger.silly('init set to false');
                cb(err);

            });
        };

        return self;
    })();

    logger.silly('application created.');
    module.exports = exports = app;
});
