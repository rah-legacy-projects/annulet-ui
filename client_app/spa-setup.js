define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        bs3 = require('bootstrap'),
        knockstrap = require('knockstrap'),
        logger = require('./logger'),
        app = require('./app'),
        stripe = require('stripe'),
        models = require('./models/index'),
        toastr = require('./toastr'),
        api = require('./api/api');

    require('./binding_handlers/datePicker');

    api.setup(function(err) {
        logger.setup();
        logger.silly('logger set up');
        stripe.setPublishableKey('pk_test_U9CjPQcjUb6HKJ7FVUQCgc4x');
        logger.silly('stripe public key set');

        toastr.options = {
            "closeButton": true,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": true,
            "timeOut": 0,
            "extendedTimeOut": 0
        };
        logger.silly('toastr set up');
        ko.validation.init({
            errorElementClass: 'has-error',
            errorMessageClass: 'help-block',
            decorateElement: true,

            messagesOnModified: false,
            decorateElementOnModified: false,
            grouping: {
                deep: true,
                live: true,
                observable: true
            }
        });
        logger.warn(_.keys(models)
            .join(', '));
        app.setup(function(err) {
            ko.applyBindings(app, $('#application')
                .get(0));
            logger.silly('bindings applied');
        });
    });
});
