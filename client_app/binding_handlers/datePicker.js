define(function(require, exports, module) {
    var ko = require('knockout'),
        moment = require('moment'),
        $ = require('jquery');
    ko.bindingHandlers.datePicker = {
        init: function(element, valueAccessor, allBindingsAccessor) {
            $(element)
                .datetimepicker({
                    format: valueAccessor()
                        .format,
                })
                .on('dp.change', function(e) {
                    var observable = valueAccessor()
                        .value;
                    if (!!e.timeStamp) {
                        var pickedDate = $(this)
                            .data("DateTimePicker")
                            .date();

                        if (!pickedDate) {
                            observable(pickedDate);
                        } else {
                            var thisMoment =pickedDate;
                            observable(thisMoment.format(valueAccessor()
                                .format));
                        }

                    }
                });

            //set up the timepicker to reflect the selected time
            $(element)
                .data('DateTimePicker')
                .date(moment(valueAccessor()
                    .value()));

        },
        update: function(element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (!!value.value) {
                $(element)
                    .data("DateTimePicker")
                    .date(value.value());
            }
        }
    };
});
