define(function(require, exports, module) {
    var ko = require('knockout'),
        _ = require('lodash'),
        $ = require('jquery'),
        logger = require('../logger'),
        Dropzone = require('dropzone');
        logger.silly('propping up dropzone binding...');
    ko.bindingHandlers.dropzone = {
        init: function(element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            var valueOptions = {};
            var events = [];
            //refine the options to non-event options
                _.each(_.keys(value), function(key) {
                    if (/^on/.test(key)) {
                        var eventName = key.slice(2)
                            .charAt(0)
                            .toLowerCase() + key.slice(3);
                        events.push({
                            event: eventName,
                            handler: value[key]
                        });
                    } else {
                        valueOptions[key] = value[key]
                    }
                });

            var options = {
                //todo: default options for dz
            };
            $.extend(options, valueOptions);
            $(element)
                .addClass('dropzone');
            var dropzone = new Dropzone(element, options);

            //add the events to the dropzone
            _.each(events, function(event) {
                dropzone.on(event.event, event.handler);
            });
        }
    };
});
