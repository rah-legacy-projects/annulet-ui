define(function(require, exports, module) {
    var ko = require('knockout');
    //define a template source that tries to key into an object first to find a template string
    var templates = {},
        data = {},
        engine = new ko.nativeTemplateEngine();

    //{{{ hack: prop up knockstrap's templates
    templates.alert = "<div class=\"alert fade in\" data-bind=\"css: type, template: innerTemplate\"> </div>";
    templates.alertInner = "<button class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button> <p data-bind=\"text: message\"></p>";
    templates.carousel = "<!-- ko template: indicatorsTemplate --> <!-- /ko --> <div class=\"carousel-inner\"> <!-- ko foreach: items --> <div class=\"item\" data-bind=\"with: $parent.converter($data), css: { active: $index() == 0 }\"> <img data-bind=\"attr: { src: src, alt: alt }\"> <div class=\"container\"> <div class=\"carousel-caption\"> <!-- ko template: { name: $parents[1].itemTemplateName, data: $data, templateEngine: $parents[1].templateEngine, afterRender: $parents[1].afterRender, afterAdd: $parents[1].afterAdd, beforeRemove: $parents[1].beforeRemove } --> <!-- /ko --> </div> </div> </div> <!-- /ko --> </div> <!-- ko template: controlsTemplate --> <!-- /ko --> ";
    templates.carouselContent = "<div data-bind=\"text: content\"></div>";
    templates.carouselControls = "<a class=\"left carousel-control\" data-bind=\"attr: { href: id }\" data-slide=\"prev\"> <span class=\"icon-prev\"></span> </a> <a class=\"right carousel-control\" data-bind=\"attr: { href: id }\" data-slide=\"next\"> <span class=\"icon-next\"></span> </a>";
    templates.carouselIndicators = "<ol class=\"carousel-indicators\" data-bind=\"foreach: items\"> <li data-bind=\"attr: { 'data-target': $parent.id, 'data-slide-to': $index }\"></li> </ol> ";
    templates.modal = "<div class=\"modal-dialog\" data-bind=\"css: dialogCss\"> <div class=\"modal-content\"> <div class=\"modal-header\" data-bind=\"template: headerTemplate\"> </div> <div class=\"modal-body\" data-bind=\"template: bodyTemplate\"> </div> <!-- ko if: footerTemplate --> <div class=\"modal-footer\" data-bind=\"template: footerTemplate\"> </div> <!-- /ko --> </div> </div>";
    templates.modalBody = "<div data-bind=\"html: content\"> </div>";
    templates.modalFooter = "<!-- ko if: $data.action --> <a href=\"#\" class=\"btn btn-primary\" data-bind=\"click: action, html: primaryLabel\"></a> <!-- /ko --> <a href=\"#\" class=\"btn btn-default\" data-bind=\"html: closeLabel\" data-dismiss=\"modal\"></a>";
    templates.modalHeader = "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button> <h3 data-bind=\"text: label\"></h3> ";
    templates.progress = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" data-bind=\"style: { width: barWidth }, attr: { 'aria-valuenow': value }, css: innerCss\"> <span data-bind=\"css: { 'sr-only': textHidden }\"> <span data-bind=\"text: value\"></span>% <span data-bind=\"text: text\"></span> </span> </div> ";
    //}}}

    ko.templateSources.stringTemplate = function(template) {
        this.templateName = template;
    };

    ko.utils.extend(ko.templateSources.stringTemplate.prototype, {
        data: function(key, value) {
            console.log('[tmpl] in data');
            data[this.templateName] = data[this.templateName] || {};

            if (arguments.length === 1) {
                return data[this.templateName][key];
            }

            data[this.templateName][key] = value;
        },
        text: function(value) {
            console.log('[tmpl] in text');
            if (arguments.length === 0) {
                var template = templates[this.templateName];

                if (typeof(template) === "undefined") {
                    throw Error("Template not found: " + this.templateName);
                }

                return template;
            }

            templates[this.templateName] = value;
        }
    });

    engine.makeTemplateSource = function(template, doc) {
        console.log('[tmpl] making template source');
        var elem;
        if (typeof template === "string") {
            elem = (doc || document)
                .getElementById(template);

            if (elem) {
                return new ko.templateSources.domElement(elem);
            }

            return new ko.templateSources.stringTemplate(template);
        } else if (template && (template.nodeType == 1) || (template.nodeType == 8)) {
            return new ko.templateSources.anonymousTemplate(template);
        }
    };

    //make the templates accessible
    ko.templates = templates;

    //make this new template engine our default engine
    console.log('using string tmpl engine');
    ko.setTemplateEngine(engine);
});
