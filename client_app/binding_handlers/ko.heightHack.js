define(function(require, exports, module) {
    var ko = require('knockout'),
        _ = require('lodash'),
        $ = require('jquery'),
        logger = require('../logger');
    ko.bindingHandlers.heightHack = {
        init: function(element, valueAccessor, allBindingsAccessor) {
            var handler = function() {
                var lowResOffset = 0,
                    highResOffset = 0,
                    height = 0;
                if (!!valueAccessor()
                    .offset) {
                    var v = ko.unwrap(valueAccessor()
                        .offset);
                    if (_.isFunction(v)) {
                        lowResOffset = v();
                        highResOffset = v();
                    } else if (_.isNumber(v)) {
                        lowResOffset = v;
                        highResOffset = v;
                    } else if (_.isString(v)) {
                        //hack: parse height int
                        //todo: check for % or px or em
                        lowResOffset = parseInt(v, 10);
                        highResOffset = parseInt(v, 10);
                    }
                }

                if (!!valueAccessor()
                    .lowresOffset) {
                    var v = ko.unwrap(valueAccessor()
                        .lowresOffset);
                    if (_.isFunction(v)) {
                        lowResOffset = v();
                    } else if (_.isNumber(v)) {
                        lowResOffset = v;
                    } else if (_.isString(v)) {
                        //hack: parse height int
                        //todo: check for % or px or em
                        lowResOffset = parseInt(v, 10);
                    }
                }

                if (!!valueAccessor()
                    .highresOffset) {
                    var v = ko.unwrap(valueAccessor()
                        .highresOffset);
                    if (_.isFunction(v)) {
                        highResOffset = v();
                    } else if (_.isNumber(v)) {
                        highResOffset = v;
                    } else if (_.isString(v)) {
                        //hack: parse height int
                        //todo: check for % or px or em
                        highResOffset = parseInt(v, 10);
                    }
                }



                if (!!valueAccessor()
                    .height) {
                    var v = ko.unwrap(valueAccessor()
                        .height);
                    if (_.isFunction(v)) {
                        height = v();
                    } else if (_.isNumber(v)) {
                        height = v;
                    } else if (_.isString(v)) {
                        //assume jquery selector
                        height = $(v)
                            .height();
                    }
                }

                if ($(window)
                    .width() < 768) {
                    $(element)
                        .height(height - lowResOffset);
                } else {
                    $(element)
                        .height(height - highResOffset);
                }


            };
            $(window)
                .resize(function(event) {
                    handler();
                });

            handler();
        }
    };
});
