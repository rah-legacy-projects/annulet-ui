define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        api = require('../../api/api');
        require('knockout-validation');

    var setPassword = function(slug) {
        var self = this;
        self.email = ko.observable();
        self.slug = ko.observable(slug);
        self.password = ko.observable();
        self.passwordAgain = ko.observable();

        self.errors = ko.validation.group(self);
        self.enforceErrors = ko.observable(false);
        self.processing = ko.observable(false);

        self.password.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.passwordAgain.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            equal: {
                params: self.password,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        })

        self.submit = function() {
            self.enforceErrors(true);
            self.processing(true);
            if (self.errors()
                .length === 0) {
                api.forgotPassword.changePassword(self.slug(), {
                    email: self.email(),
                    password: self.password()
                }, function(err) {
                    require('../../app')
                        .router.setLocation('/forgot/success');
                    self.processing(false);
                });
            } else {
                self.processing(false);
            }
        };


    };
    module.exports = exports = setPassword;
});
