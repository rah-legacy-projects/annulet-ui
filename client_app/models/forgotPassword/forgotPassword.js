define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        toastr = require('toastr'),
        api = require('../../api/api');
    require('knockout-validation');

    var forgotPassword = function() {
        var self = this;
        self.email = ko.observable();

        self.errors = ko.validation.group(self);
        self.enforceErrors = ko.observable(false);
        self.processing = ko.observable(false);
        self.email.extend({
            email: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.submit = function() {
            self.enforceErrors(true);
            self.processing(true);
            if (self.errors()
                .length === 0) {
                api.forgotPassword.forgot(self.email(), function(err, data) {
                    if (!!data.problem) {
                        toastr.error(data.problem);
                    } else {
                        require('../../app')
                            .router.setLocation('/forgot/submitted');
                    }
                    self.processing(false);
                });
            } else {
                self.processing(false);
            }
        };


    };
    module.exports = exports = forgotPassword;
});
