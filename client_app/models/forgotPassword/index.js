define(function(require, exports, module) {
    module.exports = exports = {
        ForgotPassword: require("./forgotPassword"),
        SetPassword: require("./setPassword"),
    };
});
