define(function(require, exports, module) {
    module.exports = exports = {
        WorkflowItem: require("./workflowItem"),
        WorkflowList: require('./workflowList'),

        workflowItemTypes: require("./workflowItemTypes/index"),
        Workflow: require("./workflow"),
    };
});
