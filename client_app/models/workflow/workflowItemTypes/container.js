define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout');

    ko.mapping = require('ko-mapping');


    var ContainerWorkflowItem = function(data) {
        var self = this;
        logger.silly('constructing container workflow item');
        self.url = ko.observable();
        return self;
    };

    module.exports = exports = ContainerWorkflowItem;

});
