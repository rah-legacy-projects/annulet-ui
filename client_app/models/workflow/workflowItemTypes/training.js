define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout');

    ko.mapping = require('ko-mapping');


    var TrainingWorkflowItem = function(data) {
        var self = this;

        logger.silly('constructing training workflow item');
        self._id = ko.observable(data._id);
        self.training = ko.observable(data.training);
        self.url = ko.computed(function() {
            return '/training/' + self.training();
        });

        return self;
    };

    module.exports = exports = TrainingWorkflowItem;

});
