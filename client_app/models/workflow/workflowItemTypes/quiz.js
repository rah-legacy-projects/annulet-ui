define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout');

    ko.mapping = require('ko-mapping');


    var QuizWorkflowItem = function(data) {
        var self = this;

        logger.silly('constructing quiz workflow item');
        self._id = ko.observable(data._id);
        self.quiz = ko.observable(data.quiz);
        self.url = ko.computed(function(){
            return '/quiz/'+self.quiz();
        });

        return self;
    };

    module.exports = exports = QuizWorkflowItem;

});
