define(function(require, exports, module){
    module.exports = exports = {
       quiz: require('./quiz'),
       training: require('./training'),
       root: require('./root'),
       container: require('./container')
    };
});
