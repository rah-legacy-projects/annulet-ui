define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout');

    ko.mapping = require('ko-mapping');


    var RootWorkflowItem = function(data) {
        var self = this;
        logger.silly('constructing root workflow item');
        self.url = ko.observable();
        return self;
    };

    module.exports = exports = RootWorkflowItem;

});
