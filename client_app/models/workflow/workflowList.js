define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        //hack: for now use the quickview model
        workflowStatus = require('../itemActivity/workflowStatus');
    require('knockout-validation');

    var workflowList = function() {
        var self = this;
        self.workflowList = ko.observableArray([]);
        self.setup = function(limit, cb) {
            api.workflow.list(function(err, workflows) {
                logger.silly("got workflows: " + workflows.length);
                async.parallel(
                    _.map(workflows, function(workflow) {
                        return function(cb) {
                            var a = new workflowStatus();
                            a.setup(workflow, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, workflows) {
                        self.workflowList(workflows);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = workflowList;
});
