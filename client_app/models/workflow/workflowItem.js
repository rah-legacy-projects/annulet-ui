define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        workflowItemTypes = require('./workflowItemTypes/index');

    ko.mapping = require('ko-mapping');


    var WorkflowItem = function(sequenceContainer, itemsContainer, data) {
        data.type = data.__t || 'workflowItem';
        var workflowItemTypeName = data.type.split('.')
            .reverse()[0];
        workflowItemTypeName = workflowItemTypeName.charAt(0)
            .toLowerCase() + workflowItemTypeName.slice(1);

        logger.silly('type: ' + workflowItemTypeName);
        var self = this;

        if (!!workflowItemTypes[workflowItemTypeName]) {
            //if the type exists, use that constructor to make self
            self = new workflowItemTypes[workflowItemTypeName](data);
        }else{
            logger.silly('no type found for ' + workflowItemTypeName);
        }

        self.template = ko.observable(workflowItemTypeName);
        self.isAvailable = ko.observable(false);
        self.completedDate = ko.observable(data.completedDate);
        self._isCompleted = ko.observable(data.isCompleted);
        self.isCompleted = ko.computed({
            read: function() {
                return self._isCompleted() || !!self.completedDate();
            },
            write: function(value) {
                self._isCompleted(value);
            }
        });
        self.title = ko.observable(data.title);
        self.description = ko.observable(data.description);
        self.id = ko.observable(data._id);

        //make items if they exist
        if (!!data.items && _.isArray(data.items) && data.items.length > 0) {
            self.items = [];
            _.each(data.items, function(item) {
                self.items.push(new WorkflowItem(null, self, item));
            });
        }
        //make sequence if they exist
        if (!!data.sequence && _.isArray(data.sequence) && data.sequence.length > 0) {
            self.sequence = [];
            _.each(data.sequence, function(sequence) {
                self.sequence.push(new WorkflowItem(self, null, sequence));
            });
        }

        self.updateComplete = function() {
            //update item completeness
            _.each(self.items, function(item) {
                if (!!item.updateComplete) {
                    item.updateComplete();
                }
            });

            //update sequence completess
            _.each(self.sequence, function(sequence) {
                if (!!sequence.updateComplete) {
                    sequence.updateComplete();
                }
            });

            //if this has items or sequence,
            if ((!!self.items || !!self.sequence) && (self.items.length > 0 || self.sequence.length > 0)) {
                //use their completeness to determine my completeness
                var allItemsCompleted = _.all(self.items, function(item) {
                    return !!item.completedDate() || item.isCompleted();
                });
                var allSequenceCompleted = _.all(self.sequence, function(sequence) {
                    return !!sequence.completedDate() || sequence.isCompleted();
                });
                self.isCompleted(allItemsCompleted && allSequenceCompleted);
            } else if (!self.completedDate() && !self.isCompleted()) {
                //if this doesn't have completeness or completeness is false, set to false
                self.isCompleted(false);
            }

            return !!self.completedDate() || !!self.isCompleted();
        };

        self.updateAvailable = function() {
            //corner case: root: it's always available
            if (!sequenceContainer && !itemsContainer) {
                self.isAvailable(true);;
            }

            if (!!sequenceContainer) {
                var ix = _.findIndex(sequenceContainer.sequence, function(item) {
                    return self.id == item.id;
                });
                if (ix == 0) {
                    //if this is the first sequence, it's available if the sequence container is
                    self.isAvailable(sequenceContainer.isAvailable());
                } else {
                    //sequence must be concluded in order.  Is this' previous 'sibling' complete?
                    self.isAvailable(!!sequenceContainer.sequence[ix - 1].completedDate() || sequenceContainer.sequence[ix - 1].isCompleted());
                }
            }

            if (!!itemsContainer) {
                //if this is a contained item, it's available if the items container is
                self.isAvailable(itemsContainer.isAvailable());
            }

            //update items availability
            _.each(self.items, function(item) {
                item.updateAvailable();
            });
            //update sequence availability
            _.each(self.sequence, function(sequence) {
                sequence.updateAvailable();
            });

            return self.isAvailable();
        }

        self.update = function() {
            self.updateComplete();
            self.updateAvailable();
        };

        return _.defaults(self, data);

    };

    module.exports = exports = WorkflowItem;
});
