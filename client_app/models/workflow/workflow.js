define(function(require, exports, module) {
    var _ = require('lodash'),
        async = require('async'),
        ko = require('knockout'),
        store = require('store'),
        logger = require('../../logger'),
        WorkflowItem = require('./workflowItem'),
        api = require('../../api/api');

    module.exports = exports = function() {
        var self = this;

        self.workflowItem = ko.observable();

        self.setup = function(workflowId, cb) {
            if (!!workflowId) {
                logger.silly('getting workflow ' + workflowId);
                api.workflow.get(workflowId, function(err, loadedWorkflow) {
                    logger.silly('wf received: ' + JSON.stringify(loadedWorkflow,null,3));
                    var workflowItem = new WorkflowItem(null, null, loadedWorkflow);
                    workflowItem.update();
                    self.workflowItem(workflowItem);
                    if (!!cb) {
                        cb(err);
                    }
                });
            }
        };
    };
});
