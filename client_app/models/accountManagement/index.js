define(function(require, exports, module) {
    module.exports = exports = {
        AccountManagement: require("./accountManagement"),
        CreditCard: require("./creditCard"),
        Customer: require("./customer"),
    };
});
