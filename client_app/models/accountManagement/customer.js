define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        toastr = require('toastr'),
        api = require('../../api/api');

    require('ko.stringTemplateEngine');
    require('knockout-validation');
    var customer = function(data) {
        data = data || {address:{}};
        var self = this;
        self.isOpenForEdit = ko.observable(false);

        self.rollback = _.clone(data);

        self.name = ko.observable(data.name);
        self.address = {
            street1: ko.observable(data.address.street1),
            street2: ko.observable(data.address.street2),
            city: ko.observable(data.address.city),
            state: ko.observable(data.address.state),
            zip: ko.observable(data.address.zip)
        };
        self.phone = ko.observable(data.phone);
        self.numberOfLocations = ko.observable(data.numberOfLocations);
        self.numberOfEmployees = ko.observable(data.numberOfEmployees);

        self.enforceErrors = ko.observable(false);

        self.name.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.street1.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.city.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.state.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.zip.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.phone.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.numberOfLocations.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            number: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.numberOfEmployees.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            number: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.errors = ko.validation.group(self, {deep:true});
        self.edit = function() {
            self.isOpenForEdit(true);
        };
        self.cancel = function() {
            self.name(self.rollback.name);
            self.address.street1(self.rollback.address.street1);
            self.address.street2(self.rollback.address.street2);
            self.address.city(self.rollback.address.city);
            self.address.state(self.rollback.address.state);
            self.address.zip(self.rollback.address.zip);
            self.phone(self.rollback.phone);
            self.numberOfLocations(self.rollback.numberOfLocations);
            self.numberOfEmployees(self.rollback.numberOfEmployees);

            self.isOpenForEdit(false);
        };

        self.submit = function(cb) {
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {
                api.account.updateCustomer(JSON.parse(ko.toJSON(self)), function(err, customer) {
                    if (!!err) {
                        toastr.error('Problem updating customer: ' + err);
                    } else {
                        toastr.success('Customer information saved.');
                        self.enforceErrors(false);
                        self.rollback = ko.toJS(self);
                        self.isOpenForEdit(false);
                    }
                    if (!!cb && _.isFunction(cb)) {
                        cb(err, customer);
                    }
                });
            }
        };


        return self;
    };

    module.exports = exports = customer;
});
