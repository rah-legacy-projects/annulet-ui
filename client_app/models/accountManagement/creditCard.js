define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async'),
        toastr = require('toastr'),
        api = require('../../api/api'),
        stripe = require('stripe');

    require('knockout-validation');

    var creditCard = function(data) {
        data = data || {};
        var self = this;

        self.isOpenForEdit = ko.observable(false);
        //set up the stripe info?
        self.stripe = {
            lastFour: ko.observable(data.last4),
            brand: ko.observable(data.brand),
            expiryMonth: ko.observable(data.exp_month),
            expiryYear: ko.observable(data.exp_year),

        };

        self.stripe.expiryDate = ko.computed(function() {
            return self.stripe.expiryMonth() + '/' + self.stripe.expiryYear();
        });

        self.paymentErrors = ko.observable();
        self.creditCardNumber = ko.observable();
        self.cvc = ko.observable();
        self.expiryMonth = ko.observable();
        self.expiryYear = ko.observable();
        self.enforceErrors = ko.observable(false);

        self.paymentErrors.extend({
            validation: {
                validator: function(validatedValue, validatedAgainst) {
                    return !validatedValue;
                },
                onlyIf: function() {
                    return self.enforceErrors();
                },
            }
        });

        self.creditCardNumber.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
            minLength: {
                params: 16,
                onlyIf: function() {
                    logger.silly('\t\tcc check show: ' + self.enforceErrors());
                    return self.enforceErrors();
                },
            }
        });
        self.cvc.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
            minLength: {
                params: 2,
                onlyIf: function() {
                    return self.enforceErrors();
                },
            }
        });

        self.expiryMonth.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
        });
        self.expiryYear.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
        });

        self.months = ko.observableArray([{
            value: '01',
            display: 'Jan (01)'
        }, {
            value: '02',
            display: 'Feb (02)'
        }, {
            value: '03',
            display: 'Mar (03)'
        }, {
            value: '04',
            display: 'Apr (04)'
        }, {
            value: '05',
            display: 'May (05)'
        }, {
            value: '06',
            display: 'Jun (06)'
        }, {
            value: '07',
            display: 'Jul (07)'
        }, {
            value: '08',
            display: 'Aug (08)'
        }, {
            value: '09',
            display: 'Sep (09)'
        }, {
            value: '10',
            display: 'Oct (10)'
        }, {
            value: '11',
            display: 'Nov (11)'
        }, {
            value: '12',
            display: 'Dec (12)'
        }]);

        self.years = ko.observableArray([]);
        var year = (new Date())
            .getFullYear();
        for (var i = 0; i < 10; i++) {
            self.years.push({
                value: (year + i)
                    .toString(),
                display: (year + i)
                    .toString()
            });
        }

        self.errors = ko.validation.group(self);

        self.edit = function() {
            self.isOpenForEdit(true);
        };
        self.cancel = function() {
            self.isOpenForEdit(false);
        };

        self.submit = function(cb) {
            self.paymentErrors(null);
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {

                async.waterfall([

                    function(cb) {
                        logger.silly('creating token...');
                        //create the token
                        stripe.card.createToken({
                            number: self.creditCardNumber(),
                            cvc: self.cvc(),
                            exp_month: self.expiryMonth()
                                .value,
                            exp_year: self.expiryYear()
                                .value
                        }, function(status, response) {
                            if (!!response.error) {
                                logger.silly('stripe problem: ' + response.error.message);
                                logger.error(response.error);
                                self.paymentErrors(response.error.message);
                                toastr.error('Problem processing payment: ' + response.error.message);
                                cb(response.error);
                            } else {
                                cb(null, {
                                    stripeResponse: response
                                });
                            }
                        });
                    },
                    function(p, cb) {
                        logger.silly('saving customer...');
                        //save the new card to the customer
                        api.account.updatePayment(p.stripeResponse.id, function(err, response) {
                            logger.silly('customer saved');
                            p.customerResponse = response;
                            cb(err, p);
                        });
                    },
                ], function(err, p) {
                    logger.silly('stripe/customer done');
                    if (!!err) {
                        toastr.error('Problem processing payment: ' + err);
                    } else {
                        toastr.success('Payment update complete!');
                        self.enforceErrors(false);
                        self.stripe.lastFour(p.stripeResponse.card.last4);
                        self.stripe.brand(p.stripeResponse.card.brand);
                        self.stripe.expiryMonth(p.stripeResponse.card.exp_month);
                        self.stripe.expiryYear(p.stripeResponse.card.exp_year);
                        self.isOpenForEdit(false);
                    }

                    if (!!cb && _.isFunction(cb)) {
                        cb(err);
                    }
                });
            }

        };

        return self;
    };

    module.exports = exports = creditCard;
});
