define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        Customer = require('./customer'),
        CreditCard = require('./creditCard'),
        api = require('../../api/api'),
        toastr = require('toastr'),
        stripe = require('stripe');

    logger.silly('creating signup');

    require('knockout-validation');
    var accountManagement = function() {
        var self = this;
        self.showDeactivate = ko.observable(false);
        self.customer = ko.validatedObservable(new Customer());
        self.creditCard = ko.validatedObservable(new CreditCard());
        self.section = ko.observable('Payment');

        self.processing = ko.observable(false);

        self.deactivateAccount = function(){
            api.account.deactivate(function(err){
                if(!err){
                    //log out.
                    //hack: require app jit and use existing logout
                    require('../../app').auth().logOut();
                }
            });
        };

        self.setup = function(cb){
            api.account.paymentDetail(function(err, data){
                console.log(data);
                logger.silly('payment detail retrieved');
                self.customer(new Customer(data.customer));
                self.creditCard(new CreditCard(data.stripeCustomer.sources.data[0]));
                if(!!cb){
                    logger.silly('account management set up, calling back');
                    cb(err, data);
                }
            });
        };
        return self;
    };

    module.exports = exports = accountManagement;
});
