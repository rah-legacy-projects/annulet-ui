define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        api = require('../../api/api'),
        app = require('../../app'),
        store = require('store'),
        async = require('async'),
        User = require('./user');

    var userManagement = function() {
        var self = this;
        self.users = ko.observableArray([]);


        self.addNewUser = function() {
            var newUser = new LocalUser(self, {});
            newUser.edit();
            newUser.isNew(true);
            self.users.push(newUser);
        };

        self.canGrant = ko.observableArray([]);

        var access = store.get('auth-customer-access')
            .access;
        logger.silly('access: ' + JSON.stringify(access));
        if (_.any(access, function(a) {
            return a == 'Owner';
        })) {
            self.canGrant.push('Owner');
            self.canGrant.push('Administrator');
            self.canGrant.push('User');
        } else if (_.any(access, function(a) {
            return a == 'Administrator';
        })) {
            self.canGrant.push('Administrator');
            self.canGrant.push('User');
        }


        self.setup = function(cb) {
            logger.silly('about to issue request for users');
            api.userManagement.getUsers(function(err, users) {

                async.parallel(_.map(users, function(user) {
                    return function(cb) {
                        var u = new User();
                        u.setup(self, user, function(err) {
                            cb(err, u);
                        });
                    };
                }), function(err, users) {
                    self.users(users);
                    if (!!cb) {
                        cb(err);
                    }
                });
            });

        }

        logger.silly('returning user management object');
        return self;
    }

    logger.silly('exporting user management');
    module.exports = exports = userManagement;
});
