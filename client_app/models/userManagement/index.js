define(function(require, exports, module) {
    module.exports = exports = {
        User: require("./user"),
        UserManagement: require("./userManagement"),
    };
});
