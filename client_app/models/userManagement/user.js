define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        toastr = require('toastr'),
        Chrome = require('../../chrome'),
        api = require('../../api/api');

    var user = function() {
        var self = this;
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.email = ko.observable();
        self.access = ko.observableArray([]);
        self.management = ko.observable();
        self._id = ko.observable();

        self.error = ko.observable();
        self.enforceErrors = ko.observable(false);
        self.firstName.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.lastName.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.email.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            email: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.grant = function(permission) {
            //for now, users can only have one role at a time
            if (self['canGrant' + permission]()) {
                self.access([permission]);
            }
        };

        //hack: for now, use computeds to show access
        self.isOwner = ko.computed(function() {
            return _.any(self.access(), function(a) {
                return a == 'Owner';
            });
        });
        self.isAdministrator = ko.computed(function() {
            return _.any(self.access(), function(a) {
                return a == 'Administrator';
            });
        });
        self.isUser = ko.computed(function() {
            return _.any(self.access(), function(a) {
                return a == 'User';
            });
        });

        self.isSomething = ko.computed(function() {
            return self.isOwner() || self.isAdministrator() || self.isUser();
        });

        self.isSomething.extend({
            equal: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors() && !self.removed();
                }
            }
        });

        //hack: for now, use computeds to show what can be granted
        self.canGrantOwner = ko.computed(function() {
            if (!!self.management()) {
                return _.any(self.management()
                    .canGrant(), function(a) {
                        return a == 'Owner';
                    })
            }
            return false;
        });
        self.canGrantAdministrator = ko.computed(function() {
            if (!!self.management()) {
                return _.any(self.management()
                    .canGrant(), function(a) {
                        return a == 'Owner' || a == 'Administrator';
                    })
            }
            return false;
        });
        self.canGrantUser = ko.computed(function() {
            if (!!self.management()) {
                return _.any(self.management()
                    .canGrant(), function(a) {
                        return a == 'Owner' || a == 'Administrator';
                    })
            }
            return false;
        });

        self.editUri = ko.computed(function() {
            return '/userManagement/edit/' + self._id();
        });

        self.isNew = ko.observable(false);
        self.removed = ko.observable(false);

        self.save = function() {
            logger.warn('saving user');
            self.error(null);
            self.enforceErrors(true);
            if (self.errors()
                .length === 0) {
                    Chrome.processing(true);
                api.userManagement.saveUser({
                        firstName: self.firstName(),
                        lastName: self.lastName(),
                        email: self.email(),
                        _id: self._id()
                    },
                    self.access(),
                    function(err, user) {
                        logger.silly(JSON.stringify(user));
                        if (!!err) {
                            self.error(err);
                            Chrome.processing(false);
                        } else if(!!user.recreation){
                            toastr.error("Could not save: User already exists.  Click <a href='/userManagement/edit/"+user._id+"'>here</a> to edit.");
                            Chrome.processing(false);
                        }else {
                            self.isNew(false);
                            self._id(user._id);
                            require('../../app')
                                .router.setLocation('/userManagement');
                        }
                    });
            }
        };

        self.showRemove = ko.observable(false);
        self.removeUser = function() {
            api.userManagement.removeUser({
                firstName: self.firstName(),
                lastName: self.lastName(),
                email: self.email(),
                _id: self._id()
            }, function(err) {
                self.removed(true);
                self.showRemove(false);
            });
        };
        self.removeQuestion = ko.computed(function(){
            return "Remove " + self.firstName() + ' ' + self.lastName() + '?';
        });

        self.setup = function(management, user, cb) {
            user = user || {};
            self.firstName(user.firstName);
            self.lastName(user.lastName);
            self.email(user.email);
            logger.silly('user access: ' + JSON.stringify(user));
            self.access(user.access);
            self.management(management);
            self._id(user._id);
            if (!!cb) {
                cb();
            }
        };

        self.errors = ko.validation.group(self);
        logger.silly('done constructing user');
        return self;
    }
    module.exports = exports = user;
});
