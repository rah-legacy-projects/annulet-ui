define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api');
    require('knockout-validation');

    var alert = function() {
        var self = this;

        self.markdown = ko.observable('');
        self.starts = ko.observable();
        self.ends = ko.observable();
        self.alertLevel = ko.observable();
        self.customer = ko.observable();
        self.dismissed = ko.observable();
        self._id = ko.observable();
        self._rangeId = ko.observable();

        self.renderedMarkdown = ko.computed(function() {
            return marked(self.markdown());
        });

        self.alertClass = ko.computed(function() {
            return 'alert-info';
        });

        self.dismiss = function() {
            api.alert.dismiss(self._rangeId(), function(err, r) {
                self.dismissed(true);
            });
        }

        self.setup = function(data, cb) {
            self.markdown(data.markdown);
            self.starts(data.starts);
            self.ends(data.ends);
            self.alertLevel(data.alertLevel);
            self.customer(data.customer);
            self.dismissed(data.dismissed);
            self._id(data._id);
            self._rangeId(data._rangeId);
            cb();
        }
    };

    module.exports = exports = alert;
});
