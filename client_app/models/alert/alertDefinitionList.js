define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        Alert = require('./alertDefinition');
    require('knockout-validation');

    var alertList = function() {
        var self = this;
        self.alertList = ko.observableArray([]);

        self.displayList = ko.computed(function() {
            return _.filter(self.alertList, function(alert) {
                return !alert.deleted();
            });
        });

        self.add = function() {
            logger.silly('adding');
            var a = new Alert();
            a.setup({
                markdown: ''
            }, function(err) {
                logger.silly('new alert set up.');
                a.isNew(true);
                a.edit();
                self.alertList.push(a);
            });
        };

        self.setup = function(cb) {
            api.alertAdministration.list(function(err, alerts) {
                async.parallel(
                    _.map(alerts, function(alert) {
                        return function(cb) {
                            var a = new Alert();
                            a.setup(alert, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, alerts) {
                        self.alertList(alerts);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = alertList;
});
