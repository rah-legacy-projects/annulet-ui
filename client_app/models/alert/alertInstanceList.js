define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        Alert = require('./alertInstance');
    require('knockout-validation');

    var alertList = function() {
        var self = this;
        self.alertList = ko.observableArray([]);
        self.displayDismissed = ko.observable(false);
        self.limit = ko.observable();

        self.displayList = ko.computed(function() {
            var list = _.filter(self.alertList(), function(alert) {
                if (self.displayDismissed()) {
                    return true;
                } else {
                    return !alert.dismissed();
                }
            });

            if (!!self.limit()) {
                list = list.slice(0, self.limit());
            }
            logger.silly('list length: ' + list.length);
            return list;
        });

        self.howManyMore = ko.computed(function() {
            var filtered = _.filter(self.alertList(), function(alert) {
                return !alert.dismissed();
            });
            var left = filtered.length - (self.limit() || 0);
            if (left < 0) {
                left = filtered.length;
            }
            return left;
        });

        self.hasMore = ko.computed(function() {
            var filtered = _.filter(self.alertList(), function(alert) {
                return !alert.dismissed();
            });
            return (filtered.length - (self.limit() || 0)) > 0;

        });

        self.setup = function(limit, cb) {
            self.limit(limit);
            api.alert.list(function(err, alerts) {
                async.parallel(
                    _.map(alerts, function(alert) {
                        return function(cb) {
                            var a = new Alert();
                            a.setup(alert, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, alerts) {
                        self.alertList(alerts);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = alertList;
});
