define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api'),
        moment = require('moment');
    require('knockout-validation');

    var alert = function() {
        var self = this;

        self.markdown = ko.observable('');
        self.starts = ko.observable();
        self.ends = ko.observable();
        self.alertLevel = ko.observable();
        self.customer = ko.observable();
        self._id = ko.observable();
        self.deleted = ko.observable();
        self.isOpenForEdit = ko.observable(false);

        self.isNew = ko.observable(false);
        self._originalData = ko.observable();
        self.error = ko.observable();
        self.enforceErrors = ko.observable(false);
        self.errors = ko.validation.group(self);
        self.processing = ko.observable(false);


        self.renderedMarkdown = ko.computed(function() {
            logger.silly('md: ' + self.markdown());
            return marked(self.markdown());
        });

        self.delete = function() {
            api.alertAdministration.delete(ko.toJSON(self), function(err, r) {
                self.deleted(true);
            });
        };

        self.startsDisplay = ko.computed(function() {
            return moment(self.starts())
                .format('MM/DD/YYYY');
        });
        self.endsDisplay = ko.computed(function() {
            return moment(self.ends())
                .format('MM/DD/YYYY');
        });

        self.alertClass = ko.computed(function() {
            return 'alert-info';
        });

        self.alertLevelOptions = ko.observableArray([
            'Information'
        ]);


        self.markdown.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.starts.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.ends.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.alertLevel.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.edit = function() {
            self._originalData(ko.toJS(self));
            self.isOpenForEdit(true);
        };
        self.cancel = function() {
            logger.silly('cancel');
            if (self.isNew()) {
                self.deleted(true);
            } else {

                self.markdown(self._originalData()
                    .markdown);
                self.starts(self._originalData()
                    .starts);
                self.ends(self._originalData()
                    .ends);
                self.alertLevel(self._originalData()
                    .alertLevel);
            }
            self.isOpenForEdit(false);
        };

        self.makeParameters = function() {
            return {
                markdown: self.markdown(),
                starts: self.starts(),
                ends: self.ends(),
                alertLevel: self.alertLevel(),
                _id: self._id()
            }
        };

        self.save = function() {
            logger.silly('saving alert');
            self.error(null);
            self.processing(true);
            self.enforceErrors(true);
            if (self.errors()
                .length === 0) {
                if (self.isNew()) {
                    api.alertAdministration.create(self.makeParameters(), function(err, saved) {
                        self._id(saved._id);
                        self.processing(false);
                        self.isOpenForEdit(false);
                    });
                } else {
                    api.alertAdministration.edit(self.makeParameters(), function(err, saved) {
                        self.processing(false);
                        self.isOpenForEdit(false);
                    });
                }
            }
        };


        self.delete = function() {
            logger.silly('deleting alert');
            self.processing(true);
            api.alertAdministration.delete(self.makeParameters(), function(err, deleted) {
                self.deleted(true);
                self.processing(false);
            });
        };

        self.setup = function(data, cb) {
            self.markdown(data.markdown);
            self.starts(data.starts);
            self.ends(data.ends);
            self.alertLevel(data.alertLevel);
            self.customer(data.customer);
            self._id(data._id);
            self.deleted(data.deleted);
            cb();
        }
    };

    module.exports = exports = alert;
});
