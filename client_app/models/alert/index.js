define(function(require, exports, module) {
    module.exports = exports = {
        AlertDefinition: require("./alertDefinition"),
        AlertDefinitionList: require("./alertDefinitionList"),
        AlertInstance: require("./alertInstance"),
        AlertInstanceList: require("./alertInstanceList"),
    };
});
