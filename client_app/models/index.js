define(function(require, exports, module) {
    module.exports = exports = {
        admin: require('./admin/index'),
        Auth: require('./auth'),
        Dashboard: require('./dashboard'),
        quiz: require('./quiz/index'),
        workflow: require('./workflow/index'),
        training: require('./training/index'),
        userManagement: require('./userManagement/index'),
        operatingProcedure: require('./operatingProcedure/index'),
        complaint: require('./complaint/index'),
        alert: require('./alert/index'),
        accountManagement: require('./accountManagement/index'),
        auxiliaryDocument: require('./auxiliaryDocument/index'),
        forgotPassword: require('./forgotPassword/index'),
        activation: require('./activation/index'),
        itemActivity: require('./itemActivity/index')
    };
});
