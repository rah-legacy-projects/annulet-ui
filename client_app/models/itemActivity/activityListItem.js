define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        async = require('async'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api'),
        moment = require('moment');
    require('knockout-validation');

    var activity = function() {
        var self = this;
        self.email = ko.observable();
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.activity = ko.observable();
        self.type = ko.observable();
        self.title = ko.observable();
        self.timestamp = ko.observable();

        self.displayName = ko.computed(function(){
            return self.firstName() + ' ' + self.lastName() + ' (' + self.email() + ')';
        });

        self.displayTimestamp = ko.computed(function(){
            return moment(self.timestamp()).format('MM/DD/YYYY [at] h:mm a');
        });

        self.displayType = ko.computed(function(){
            return (self.type()||'').split('.').reverse()[0];
        });

        self.activityText = ko.computed(function(){
            return self.activity() + ' ' + self.title() + ' on ' + self.displayTimestamp();
        });

        self.setup = function(data, cb) {
            self.email(data.message.email);
            self.firstName(data.message.firstName);
            self.lastName(data.message.lastName);
            self.activity(data.state);            
            self.type(data.itemType);
            self.title(data.message.itemTitle);
            self.timestamp(data.modified);
            cb();
        }
    };

    module.exports = exports = activity;
});
