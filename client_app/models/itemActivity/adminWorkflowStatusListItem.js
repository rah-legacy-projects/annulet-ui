define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api'),
        WorkflowStatusList = require('./workflowStatusList');
    require('knockout-validation');

    var workflowStatus = function() {
        var self = this;
        self.workflowStatusList = ko.observable();
        self.email = ko.observable();
        self.firstName = ko.observable();
        self.lastName = ko.observable();

        self.displayName = ko.computed(function(){
            return self.firstName() + ' ' + self.lastName() + ' (' + self.email() + ')';
        });

        self.setup = function(data, cb) {
            self.email(data.user.email);
            self.firstName(data.user.firstName);
            self.lastName(data.user.lastName);
    
            logger.silly("status lus: " + JSON.stringify(data.workflowStatusList, null, 2));
        
            self.workflowStatusList(ko.mapping.fromJS(data.workflowStatusList));
            cb();
        }
    };

    module.exports = exports = workflowStatus;
});
