define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api');
    require('knockout-validation');

    var workflowStatus = function() {
        var self = this;

        self.definitionId = ko.observable();
        self.instanceId = ko.observable();
        self.title = ko.observable();
        self.percentageComplete = ko.observable();
        self.description = ko.observable();

        self.url = ko.computed(function(){
            if(!!self.instanceId()){
                return '/workflow/'+self.instanceId();
            }else{
                return '/workflow/'+self.definitionId();
            }
        });

        self.isCompleted = ko.computed(function(){
            return self.percentageComplete() === 100;
        });

        self.displayName = ko.computed(function(){
            return self.title() + ' - ' + self.percentageComplete() + '% done';
        });

        self.setup = function(data, cb) {
            logger.silly('wf status data: ' + JSON.stringify(data, null, 2));
            self.definitionId(data.definitionId);
            self.instanceId(data.instanceId);
            self.title(data.title);
            self.percentageComplete(data.percentageComplete);
            self.description(data.description);

            cb();
        }
    };

    module.exports = exports = workflowStatus;
});
