define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        AdminOPStatusListItem = require('./adminOPStatusListItem');
    require('knockout-validation');

    var opList = function() {
        var self = this;
        self.opList = ko.observableArray([]);

        self.limit = ko.observable();

        self.displayList = ko.computed(function() {
            var list = _.filter(self.opList(), function(op) {
                return true;
            });

            if (!!self.limit()) {
                list = list.slice(0, self.limit());
            }
            return list;
        });

        self.howManyMore = ko.computed(function() {

            var filtered = _.filter(self.opList(), function(op) {
                return true;
            });
            var left = filtered.length - (self.limit() || 0);
            if (left < 0) {
                left = filtered.length;
            }
            return left;
        });

        self.hasMore = ko.computed(function() {
            if (!self.limit()) {
                //user can see everything in this list instance, hide the footer
                return false;
            }
            var filtered = _.filter(self.opList(), function(op) {
                return true;
            });
            return (filtered.length - (self.limit() || 0)) > 0;

        });

        self.setup = function(limit, cb) {
            self.limit(limit);
            api.quickView.adminOutstandingOPs(function(err, customerUsers) {
                logger.silly('got outstanding ops for admin: ' + customerUsers.length);
                async.parallel(
                    _.map(customerUsers, function(customerUser) {
                        return function(cb) {
                            var a = new AdminOPStatusListItem();
                            a.setup(customerUser, function(err) {
                                logger.silly('\t\t\t admin op set up');
                                cb(err, a);
                            });
                        }
                    }), function(err, ops) {
                        logger.silly('admin op list made.');
                        self.opList(ops);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = opList;
});
