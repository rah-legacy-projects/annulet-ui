define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api');
    require('knockout-validation');

    var opStatus = function() {
        var self = this;

        self.definitionId = ko.observable();
        self.instanceId = ko.observable();
        self.title = ko.observable();
        self.description = ko.observable();
        self.completedDate = ko.observable();

        self.completed = ko.computed(function(){return !!self.completedDate();});

        self.url = ko.computed(function(){
            if(!!self.instanceId()){
                return '/operatingProcedure/'+self.instanceId();
            }else{
                return '/operatingProcedure/byDef/'+self.definitionId();
            }
        });

        self.setup = function(data, cb) {
            self.definitionId(data.definitionId);
            self.instanceId(data.instanceId);
            self.title(data.title);
            self.description(data.description);
            self.completedDate(data.completedDate);

            cb();
        }
    };

    module.exports = exports = opStatus;
});
