define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        ActivityListItem = require('./activityListItem');
    require('knockout-validation');

    var opList = function() {
        var self = this;
        self.limit = ko.observable();
        self.activityList = ko.observableArray();
        self.displayList = ko.computed(function() {
            var list = _.filter(self.activityList(), function(op) {
                return true;
            });

            if (!!self.limit()) {
                list = list.slice(0, self.limit());
            }
            return list;
        });

        self.howManyMore = ko.computed(function() {

            var filtered = _.filter(self.activityList(), function(op) {
                return true;
            });
            var left = filtered.length - (self.limit() || 0);
            if (left < 0) {
                left = filtered.length;
            }
            return left;
        });

        self.hasMore = ko.computed(function() {
            if (!self.limit()) {
                //user can see everything in this list instance, hide the footer
                return false;
            }
            var filtered = _.filter(self.activityList(), function(op) {
                return true;
            });
            return (filtered.length - (self.limit() || 0)) > 0;

        });

        self.setup = function(limit, cb) {
            self.limit(limit);
            api.quickView.adminOPActivity(function(err, activities) {
                async.parallel(
                    _.map(activities, function(activity) {
                        return function(cb) {
                            var a = new ActivityListItem();
                            a.setup(activity, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, activities) {
                        self.activityList(activities);
                        cb();
                    });
            });
        };
    };
    module.exports = exports = opList;
});
