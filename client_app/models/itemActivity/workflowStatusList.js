define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        workflowStatus = require('./workflowStatus');
    require('knockout-validation');

    var workflowList = function() {
        var self = this;
        self.workflowList = ko.observableArray([]);

        self.displayDismissed = ko.observable(false);
        self.limit = ko.observable();

        self.displayList = ko.computed(function() {
            var list = _.filter(self.workflowList(), function(workflow) {
                return true;
            });

            if (!!self.limit()) {
                list = list.slice(0, self.limit());
            }
            return list;
        });

        self.howManyMore = ko.computed(function() {
            var filtered = _.filter(self.workflowList(), function(workflow) {
                return true;
            });
            var left = filtered.length - (self.limit() || 0);
            if (left < 0) {
                left = filtered.length;
            }
            return left;
        });

        self.hasMore = ko.computed(function() {
            if (!self.limit()) {
                //user can see everything in this list instance, hide the footer
                return false;
            }
            var filtered = _.filter(self.workflowList(), function(workflow) {
                return true;
            });
            return (filtered.length - (self.limit() || 0)) > 0;

        });

        self.setup = function(limit, cb) {
            self.limit(limit);
                logger.silly('getting outstanding workflows...');
            api.quickView.userOutstandingWorkflows(function(err, workflows) {
                logger.silly("got workflows: " + workflows.length);
                async.parallel(
                    _.map(workflows, function(workflow) {
                        return function(cb) {
                            var a = new workflowStatus();
                            a.setup(workflow, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, workflows) {
                        self.workflowList(workflows);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = workflowList;
});
