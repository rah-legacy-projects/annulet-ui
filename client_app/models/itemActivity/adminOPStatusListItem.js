define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        async = require('async'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api'),
        OPStatusList = require('./opStatusList');
    require('knockout-validation');

    var opStatus = function() {
        var self = this;
        self.opStatusList = ko.observable();
        self.email = ko.observable();
        self.firstName = ko.observable();
        self.lastName = ko.observable();

        self.displayName = ko.computed(function(){
            return self.firstName() + ' ' + self.lastName() + ' (' + self.email() + ')';
        });

        self.setup = function(data, cb) {
            logger.silly('making admin status op list item');
            self.email(data.user.email);
            self.firstName(data.user.firstName);
            self.lastName(data.user.lastName);
            
            self.opStatusList(ko.mapping.fromJS(data.opStatusList));
            cb();
        }
    };

    module.exports = exports = opStatus;
});
