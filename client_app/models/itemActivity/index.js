define(function(require, exports, module) {
	module.exports = exports = {
		WorkflowStatusList: require("./workflowStatusList"),
		AdminWorkflowStatusListItem: require("./adminWorkflowStatusListItem"),
		ActivityListItem: require("./activityListItem"),
		OpStatus: require("./opStatus"),
		AdminWorkflowActivityList: require("./adminWorkflowActivityList"),
		AdminOPStatusListItem: require("./adminOPStatusListItem"),
		AdminWorkflowStatusList: require("./adminWorkflowStatusList"),
		OpStatusList: require("./opStatusList"),
		AdminOPActivityList: require("./adminOPActivityList"),
		AdminOPStatusList: require("./adminOPStatusList"),
		WorkflowStatus: require("./workflowStatus"),
	};
});
