define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        async = require('async'),
        api = require('../../api/api'),
        AdminWorkflowStatusListItem = require('./adminWorkflowStatusListItem');
    require('knockout-validation');

    var workflowStatusList = function() {
        var self = this;
        self.workflowStatusList = ko.observableArray([]);

        self.limit = ko.observable();

        self.displayList = ko.computed(function() {
            var list = _.filter(self.workflowStatusList(), function(workflow) {
                return true;
            });

            if (!!self.limit()) {
                list = list.slice(0, self.limit());
            }
            return list;
        });

        self.howManyMore = ko.computed(function() {

            var filtered = _.filter(self.workflowStatusList(), function(workflow) {
                return true;
            });
            var left = filtered.length - (self.limit() || 0);
            if (left < 0) {
                left = filtered.length;
            }
            return left;
        });

        self.hasMore = ko.computed(function() {
            if (!self.limit()) {
                //user can see everything in this list instance, hide the footer
                return false;
            }
            var filtered = _.filter(self.workflowStatusList(), function(workflow) {
                return true;
            });
            return (filtered.length - (self.limit() || 0)) > 0;

        });

        self.setup = function(limit, cb) {
            self.limit(limit);
            api.quickView.adminOutstandingWorkflows(function(err, customerUsers) {
                async.parallel(
                    _.map(customerUsers, function(customerUser) {
                        return function(cb) {
                            var a = new AdminWorkflowStatusListItem();
                            a.setup(customerUser, function(err) {
                                cb(err, a);
                            });
                        }
                    }), function(err, workflows) {
                        self.workflowStatusList(workflows);
                        cb();
                    });
            });
        }
    };

    module.exports = exports = workflowStatusList;
});
