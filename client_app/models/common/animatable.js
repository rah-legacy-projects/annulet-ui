define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async');
        require('knockout-animate');

    module.exports = exports = function() {
        var self = this;
        self.isVisible = ko.observable(false);
        self.animateObject = ko.observable(false);
        self.animation = ko.observableArray([]);
        self.toggleState = function() {
            self.animateObject(!self.animateObject());
        };

        self.before = ko.observable(function() {});
        self.after = ko.observable(function() {});
        return self;
    };
});
