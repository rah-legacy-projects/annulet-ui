define(function(require, exports, module) {
    module.exports = exports = {
        AuxiliaryDocumentAdminList: require("./auxiliaryDocumentAdminList"),
        AuxiliaryDocument: require("./auxiliaryDocument"),
        AuxiliaryDocumentList: require("./auxiliaryDocumentList"),
        RecentlyModifiedAuxiliaryDocumentList: require('./recentlyModifiedAuxiliaryDocumentList')
    };
});
