define(function(require, exports, module){
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async =require('async'),
        marked = require('marked'),
        api = require('../../api/api'),
        AuxiliaryDocument = require('./auxiliaryDocument'),
        moment = require('moment');
    require('knockout-validation');

    var auxDocList = function() {
        var self = this;
        
        self.fileList = ko.observableArray([]);

        self.setup = function(cb){
            api.auxiliaryDocument.list(function(err, docs){
                async.parallel(_.map(docs, function(doc){
                    return function(cb){
                        var x = new AuxiliaryDocument();
                        x.setup(doc, function(err){
                            cb(err, x);
                        });
                    };
                }), function(err, r){
                    self.fileList(r);
                    cb(err);
                });
            });
        };
    };

    exports = module.exports = auxDocList;
});
