define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async'),
        marked = require('marked'),
        api = require('../../api/api'),
        moment = require('moment'),
        store = require('store'),
        AuxiliaryDocument = require('./auxiliaryDocument'),
        Dropzone = require('dropzone');
    require('knockout-validation');

    var auxDocAdminList = function() {
        var self = this;
        self.processing = ko.observable(false);

        var listDocuments = function(cb) {
            api.auxiliaryDocument.list(function(err, docs) {
                async.parallel(_.map(docs, function(doc) {
                    return function(cb) {
                        var x = new AuxiliaryDocument();
                        x.setup(doc, function(err) {
                            cb(err, x);
                        });
                    };
                }), function(err, r) {
                    logger.silly('docs retrieved: ' + r.length);
                    self.fileList(r);
                    cb(err);
                });
            });
        };

        self.dropzone = {
            url: ko.observable(api.baseApiUriString + '/admin/auxiliaryDocument/add'),
            headers: ko.observable({
                'annulet-auth-token': store.get('auth-token'),
                'annulet-auth-customer': store.get('auth-customer')
            }),
            addedFile: function() {
                self.processing(true);
            },
            complete: function(file) {
                logger.silly('auxiliary document finished, relisting documents');
                listDocuments(function(err) {
                    self.processing(false);
                })
            }
        };

        self.fileList = ko.observableArray([]);
        self.setup = function(cb) {
            self.processing(true);
            async.parallel({
                listing: function(cb) {
                    listDocuments(cb);
                },
            }, function(err, r) {
                self.processing(false);
                cb(err, r);
            });
        };
    };

    exports = module.exports = auxDocAdminList;
});
