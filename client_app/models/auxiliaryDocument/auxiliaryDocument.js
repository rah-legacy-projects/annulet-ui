define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        moment = require('moment');
    require('knockout-validation');

    var auxDoc = function() {
        var self = this;

        self._id = ko.observable();
        self.file = ko.observable();
        self.displayName = ko.observable();
        self.description = ko.observable();
        self.modified = ko.observable();

        self.lastModifiedDisplay = function() {
            return "Last modified " + moment(self.modified())
                .format('MM/DD/YYYY h:mm a');
        };

        self.isDeleted = ko.observable(false);
        self.descriptionDisplay = ko.computed(function() {
            if ((self.description() || '') == '') {
                return 'No description given.';
            }
            return self.description();
        });

        self.setup = function(data, cb) {
            self._id(data._id);
            self.file(data.file);
            self.displayName(data.displayName);
            self.description(data.description);
            self.modified(data.modified);
            cb();
        };

        self.processing = ko.observable(false);
        self.isOpenForEdit = ko.observable(false);
        self._originalData = ko.observable();
        self.edit = function() {
            self.isOpenForEdit(true);
            self._originalData(ko.toJS(self));
        };
        self.cancel = function() {
            self.file(self._originalData()
                .file);
            self.displayName(self._originalData()
                .displayName);
            self.description(self._originalData()
                .description);
            self.isOpenForEdit(false);
        };
        self.save = function() {
            self.processing(true);
            api.auxiliaryDocument.update({
                _id: self._id(),
                file: self.file(),
                displayName: self.displayName(),
                description: self.description()
            }, function(err, data) {
                self.isOpenForEdit(false);
                self.processing(false);
            });
        };


        self.showRemove = ko.observable(false);
        self.removeQuestion = ko.computed(function() {
            return "Remove " + self.displayName() + '?';
        });
        self.remove = function() {
            self.processing(true);
            api.auxiliaryDocument.delete(self._id(), function(err) {
                self.processing(false);
                self.isDeleted(true);
            });
        };

        self.href = ko.computed(function() {
            return (new URITemplate(new URI(api.baseApiUriString)
                    .valueOf() + 'auxiliaryDocument/{file}?access_token={token}&annulet_auth_customer={customer}')
                .expand({
                    file: self._id(),
                    token: store.get('auth-token'),
                    customer: store.get('auth-customer')
                }));
        });

    };
    module.exports = exports = auxDoc;
});
