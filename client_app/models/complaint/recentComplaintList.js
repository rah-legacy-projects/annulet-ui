define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        api = require('../../api/api'),
        Comment = require('./comment'),
        Complaint = require('./complaint'),
        Change = require('./change'),
        async = require('async');

    var recentComplaintList = function() {
        var self = this;
        self.complaints = ko.observableArray([]);
        self.howManyMore = ko.observable();

        self.setup = function(cb) {
            api.complaint.recent(function(err, p) {
                self.howManyMore(p.howManyMore);
                async.parallel(_.map(p.complaints, function(complaint) {
                    return function(cb) {
                        var c = new Complaint();
                        c.setup(complaint, function(err) {
                            cb(err, c);
                        });
                    };
                }), function(err, complaints) {
                    self.complaints(complaints);
                    if (!!cb) {
                        cb();
                    }
                });

            });
        };
    }

    module.exports = exports = recentComplaintList;
});
