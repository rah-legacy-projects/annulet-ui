define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        api = require('../../api/api'),
        Comment = require('./comment'),
        moment = require('moment'),
        Change = require('./change');

    ko.mapping = require('ko-mapping');
    require('knockout-validation');

    var change = function() {
        var self = this;

        self.before = ko.observable();
        self.after = ko.observable();
        self.madeBy = {
            firstName: ko.observable(),
            lastName: ko.observable(),
            email: ko.observable()

        };
        self.created = ko.observable();
        self.field = ko.observable();
        self.changeType = ko.observable();


        self.changeDisplay = ko.computed(function() {
            var change = '';
            if (!!self.before && !!self.before()) {
                change += 'From ' + self.before() + ' ';
            }
            if (!!self.after && !!self.after()) {
                change += 'to ' + self.after();
            }
            return change;
        });
        self.changeVisible = ko.computed(function() {
            return self.changeDisplay() !== '';
        });

        self.madeByDisplay = ko.computed(function() {
            return self.madeBy
                .firstName() + ' ' + self.madeBy
                .lastName() + ' (' + self.madeBy
                .email() + ')';
        });

        self.madeOnDisplay = ko.computed(function() {
            return moment(self.created())
                .format('MM/DD/YYYY h:mm a');
        });

        self.fieldDisplay = ko.computed(function() {
            var nameMapping = {
                text: 'Text',
                complainerName: 'Complainer Name',
                status: 'Status',
                product: 'Product',
                subProduct: 'Subproduct',
                issueType: 'Issue Type',
                subIssueType: 'Subissue Type',
                state: 'State',
                zip: 'ZIP',
                submittedVia: 'Submitted Via',
                dateReceived: 'Date Received',
                dateResponseSent: 'Date Response Sent',
                companyResponseNote: 'Company Response Note',
                timelyResponse: 'Timely Repsonse',
                consumerDisputed: 'Consumer Disputed'
            };

            return nameMapping[self.field()] || self.field();
        });

        self.fieldVisible = ko.computed(function() {
            return !!self.fieldDisplay() && self.fieldDisplay() != '';
        });

        self.setup = function(data, cb) {
            self.before(data.before);
            self.after(data.after);
            self.madeBy.firstName(data.madeBy.firstName);
            self.madeBy.lastName(data.madeBy.lastName);
            self.madeBy.email(data.madeBy.email);
            self.created(data.created);
            self.field(data.field);
            self.changeType(data.changeType);
            if(!!cb){
                cb();
            }
        };

        return self;
    };

    module.exports = exports = change;
});
