define(function(require, exports, module) {
    var _ = require('lodash'),
        moment = require('moment'),
        logger = require('../../logger'),
        ko = require('knockout'),
        api = require('../../api/api'),
        Comment = require('./comment'),
        Change = require('./change');
    require('knockout-validation');

    var comment = function() {
        var self = this; // ko.mapping.fromJS(data);

        self.text = ko.observable();
        self.madeBy = {
            firstName: ko.observable(),
            lastName: ko.observable(),
            email: ko.observable()
        };
        self.created = ko.observable();
        self._id = ko.observable();
        self.enforceErrors = ko.observable(false);
        self.show = ko.observable(true);
        self.errors = ko.validation.group(self);
        self.complaint = ko.observable();
        self.text.extend({
            isRequired: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.isOpenForEdit = ko.observable(false);
        self.madeByDisplay = ko.computed(function() {
            if (!!self.madeBy) {
                return self.madeBy
                    .firstName() + ' ' + self.madeBy
                    .lastName() + ' (' + self.madeBy
                    .email() + ')';
            }
        });

        self.header = ko.computed(function() {
            if (!!self.created) {
                var date = moment(self.created())
                    .format('MM/DD/YYYY h:mm a');
                return self.madeByDisplay() + ' on ' + date + ' commented:';
            }
        });

        self.save = function() {
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {
                api.complaint.addComment(self.complaint()._id(), {
                    text: self.text()
                }, function(err, comment) {
                    console.log('comment: ' + JSON.stringify(comment,null,4));
                    self.madeBy.firstName(comment.madeBy.firstName);
                    self.madeBy.lastName(comment.madeBy.lastName);
                    self.madeBy.email(comment.madeBy.email);
                    self.isOpenForEdit(false);
                });
            }
        };

        self.cancel = function() {
            if (!self._id()) {
                self.complaint().comments.remove(self);
            } else {
                //todo: for editing comments
            }
        };

        self.setup = function(complaint, data, cb) {
            data = data || {
                madeBy: {}
            };
            self.text(data.text);
            self.madeBy.firstName(data.madeBy.firstName);
            self.madeBy.lastName(data.madeBy.lastName);
            self.madeBy.email(data.madeBy.email)
            self.created(data.created);
            self._id(data._id);
            self.complaint(complaint);
            if (!!cb) {
                cb();
            }
        };

        return self;
    };

    module.exports = exports = comment;
});
