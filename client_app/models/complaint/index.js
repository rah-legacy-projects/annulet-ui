define(function(require, exports, module) {
    module.exports = exports = {
        Change: require("./change"),
        Comment: require("./comment"),
        Complaint: require("./complaint"),
        RecentComplaintList: require('./recentComplaintList')
    };
});
