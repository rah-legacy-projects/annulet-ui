define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        moment = require('moment'),
        api = require('../../api/api'),
        Comment = require('./comment'),
        Complaint = require('./complaint'),
        Change = require('./change'),
        Table = require('../../binding_handlers/ko.datatable'),
    require('knockout-validation');

    var complaintList = function() {
        var self = this;
        self.complaintTable = new Table({
            rowModel: models.complaint.Complaint,
            dataMethod: function(parameters, cb) {
                api.complaint.listComplaints(parameters, cb);
            },
            defaultSortBy: 'created'
        });

        self.setup = function(cb){
            //keeps things consistent with other models, makes routing slightly easier
            if(!!cb){
                cb();
            }
        };
    }
});
