define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async'),
        moment = require('moment'),
        api = require('../../api/api'),
        Comment = require('./comment'),
        Change = require('./change');
    require('knockout-validation');
    require('bs3-datetime-picker');
    require('../../binding_handlers/datePicker');

    var complaint = function() {
        var self = this;


        //{{{ dropdown list options
        self.possibleStatuses = ko.observableArray([
            'Closed',
            'Closed - Explanation',
            'Closed - Monetary Relief',
            'Closed - Non-monetary Relief',
            'Closed - Relief',
            'Closed - No Relief',
            'In Progress',
            'Untimely Response'
        ]);

        self.possibleProducts = ko.observableArray([
            'Bank Account',
            'Consumer Loan',
            'Credit Card',
            'Credit Report',
            'Debt Collection',
            'Money Transfers',
            'Meeting',
            'Student Loan'
        ]);

        self.possibleSubProducts = ko.observableArray([
            'Checking Account',
            'Credit Card',
            'Installment Loan',
            'Medical',
            'Student Loan',
            'Other',
            'Payday Loan',
            'LOC',
            'Savings',
            'Auto Lease',
            'Auto Loan'
        ]);

        self.possibleIssues = ko.observableArray([
            'Account Open/Close',
            'Account Terms',
            'Adverisement/Marketing',
            'Application Delay',
            'Application Original',
            'APR/Interest',
            'Arbitration',
            'Bankruptcy',
            'Billing Dispute',
            'Statement',
            'Account Closure/Conclusion',
            'Debt Collection Dispute',
            'Collection Practice',
            'Communication Tactic',
            'Collection Not Owed',
            'Credit Decision',
            'Disclosure Error',
            'Extend Credit',
            'Credit Monitor',
            'Credit Report',
            'CRA Investigation',
            'Customer Service',
            'Delinquent Account',
            'Disclose Debt',
            'False Represenation',
            'Forbearance',
            'Fraud/Scam',
            'ID Theft Information',
            'Improper Use Information',
            'Improper CRA',
            'Late Fee',
            'Loan Modification',
            'Loan Servicing',
            'Fees',
            'Payoff',
            'Privacy'
        ]);

        self.possibleSubIssues = ko.observableArray([]);
        self.possibleSubmissions = ko.observableArray([
            'Email',
            'Fax',
            'Phone',
            'Postal Mail',
            'Referral',
            'Web'
        ]);

        //}}}
        self.text = ko.observable();
        self.complainerName = ko.observable();
        self.status = ko.observable();
        self.product = ko.observable();
        self.subProduct = ko.observable();
        self.issueType = ko.observable();
        self.subIssueType = ko.observable();
        self.state = ko.observable();
        self.zip = ko.observable();
        self.submittedVia = ko.observable();
        self.dateReceived = ko.observable();
        self.dateResponseSent = ko.observable();
        self.companyResponseNote = ko.observable();
        self.timelyResponse = ko.observable();
        self.consumerDisputed = ko.observable();
        self.createdByCustomerUser = ko.observable();
        self.customer = ko.observable();
        self._id = ko.observable();
        self.modified = ko.observable();

        self.errors = ko.validation.group(self);
        self.enforceErrors = ko.observable(false);
        self.isOpenForEdit = ko.observable(false);

        //{{{ model validation
        self.text.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.status.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.product.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.issueType.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.state.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.zip.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.submittedVia.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.dateReceived.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        // }}}

        self.comments = ko.observableArray([]);
        self.changes = ko.observableArray([]);

        self.clearDateResponseSent = function() {
            self.dateResponseSent(null);
        };


        self.commentsAndChangesVisible = ko.computed(function() {
            return !!self._id()
        });

        self.addComment = function() {
            var newComment = new Comment();
            newComment.setup(self, null, function(err) {
                newComment.isOpenForEdit(true);
                self.comments.push(newComment);
            });
        };

        self.lastModifiedDisplay = function() {
            var lastChange = self.changes()
                .reverse()[0];
            return 'Change made by ' + lastChange.madeByDisplay() + ' on ' + lastChange.madeOnDisplay();
        };

        self.header = ko.computed(function() {
            var name = !!self.complainerName ? (self.complainerName() || '[anonymous]') : 'Anonymous';
            var date = moment(self.dateReceived())
                .format('MM/DD/YYYY');
            return name + ' on ' + date + ': ' + self.issueType() + ' on ' + self.product();
        });

        self.mixed = ko.computed(function() {
            //map comments to date,type,data
            var mappedComments = _.map(self.comments(), function(comment) {
                return {
                    date: ko.observable(comment.created()),
                    type: ko.observable('comment'),
                    data: ko.observable(comment)
                };
            });
            //map changes to date,type,data
            logger.silly('changes: ' + self.changes()
                .length);
            var mappedChanges = _.map(self.changes(), function(change) {
                return {
                    date: ko.observable(change.created()),
                    type: ko.observable('change'),
                    data: ko.observable(change)
                };
            });
            //concat mappings
            var everything = mappedComments.concat(mappedChanges);
            //sort
            var everything = _.sortBy(everything, function(i) {
                return moment(i.date())
                    .format('YYYYMMDDhhmmss');
            });
            //return sorted listing
            return everything;
        });

        self.previousVersion = null;
        self.edit = function() {
            self.previousVersion = _.clone(ko.toJS(self));
            self.isOpenForEdit(true);
        };
        self.cancel = function() {
            //corner case: what if this is a new complaint?
            if (!self._id()) {
                //redirect to listing?   
                require('../../app')
                    .router.setLocation('/complaints');
            } else {
                //revert!
                self.text(self.previousVersion.text);
                self.complainerName(self.previousVersion.complainerName);
                self.status(self.previousVersion.status);
                self.product(self.previousVersion.product);
                self.subProduct(self.previousVersion.subProduct);
                self.issueType(self.previousVersion.issueType);
                self.subIssueType(self.previousVersion.subIssueType);
                self.state(self.previousVersion.state);
                self.zip(self.previousVersion.zip);
                self.submittedVia(self.previousVersion.submittedVia);
                self.dateReceived(self.previousVersion.dateReceived);
                self.dateResponseSent(self.previousVersion.dateResponseSent);
                self.companyResponseNote(self.previousVersion.companyResponseNote);
                self.timelyResponse(self.previousVersion.timelyResponse);
                self.consumerDisputed(self.previousVersion.consumerDisputed);
                self.isOpenForEdit(false);
            }

        };
        self.save = function() {
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {
                var toCommit = {};
                toCommit.text = self.text();
                toCommit.complainerName = self.complainerName();
                toCommit.status = self.status();
                toCommit.product = self.product();
                toCommit.subProduct = self.subProduct();
                toCommit.issueType = self.issueType();
                toCommit.subIssueType = self.subIssueType();
                toCommit.state = self.state();
                toCommit.zip = self.zip();
                toCommit.submittedVia = self.submittedVia();
                toCommit.dateReceived = self.dateReceived();
                toCommit.dateResponseSent = self.dateResponseSent();
                toCommit.companyResponseNote = self.companyResponseNote();
                toCommit.timelyResponse = self.timelyResponse();
                toCommit.consumerDisputed = self.consumerDisputed();
                toCommit._id = self._id();

                api.complaint.updateComplaint(toCommit, function(err, complaint) {
                    if (!self._id()) {
                        require('../../app')
                            .router.setLocation('/complaint/' + complaint._id);
                    } else {
                        self.isOpenForEdit(false);
                        async.parallel(_.map(complaint.changes, function(change) {
                            return function(cb) {
                                var c = new Change();
                                c.setup(change, function(err) {
                                    cb(err, c);
                                });
                            };
                        }), function(err, changes) {
                            self.changes(changes);
                        });
                    }
                });
            }
        };

        self.setup = function(data, cb) {
            data = data || {};

            self.text(data.text);
            self.complainerName(data.complainerName);
            self.status(data.status);
            self.product(data.product);
            self.subProduct(data.subProduct);
            self.issueType(data.issueType);
            self.subIssueType(data.subIssueType);
            self.state(data.state);
            self.zip(data.zip);
            self.submittedVia(data.submittedVia);
            self.dateReceived(data.dateReceived);
            self.dateResponseSent(data.dateResponseSent);
            self.companyResponseNote(data.companyResponseNote);
            self.timelyResponse(data.timelyResponse);
            self.consumerDisputed(data.consumerDisputed);
            self.createdByCustomerUser(data.createdByCustomerUser);
            self.customer(data.customer);

            //hack: modified should really be computed, but since it's not actually user modified, use moment to make a friendly date here
            self.modified(moment(data.modified)
                .format('MM-DD-YYYY'));

            self._id(data._id);

            async.parallel({
                comments: function(cb) {
                    async.parallel(_.map(data.comments, function(comment) {
                        return function(cb) {
                            var c = new Comment();
                            c.setup(self, comment, function(err) {
                                cb(err, c);
                            });
                        };
                    }), function(err, comments) {
                        self.comments(comments);
                        cb(err);
                    });
                },
                changes: function(cb) {

                    async.parallel(_.map(data.changes, function(change) {
                        return function(cb) {
                            var c = new Change();
                            c.setup(change, function(err) {
                                cb(err, c);
                            });
                        };
                    }), function(err, changes) {
                        self.changes(changes);
                        cb(err);
                    });
                }
            }, function(err, r) {
                if (!!cb) {
                    cb();
                }
            });
        };

        return self;
    };

    module.exports = exports = complaint;
});
