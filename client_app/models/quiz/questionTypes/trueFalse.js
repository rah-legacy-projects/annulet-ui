define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        async = require('async'),
        api = require('../../../api/api'),
        Animatable = require('../../common/animatable'),
        ko = require('knockout');
    Answer = require('../answer');

    var trueFalseQuestion = function() {
        var self = new Animatable();
        self._id = ko.observable();
        self.questionText = ko.observable();
        self.answer = ko.observable();
        self.enforceErrors = ko.observable(false);
        self.markForReview = ko.observable(false);

        self.isFinished = ko.computed(function() {
            return (self.answer() === true || self.answer() === false)
        });

        self.isFinished.extend({
            equal: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }

        });

        self.save = function(cb) {
            api.quiz.saveAnswer({
                questionType: 'TrueFalse',
                question: self._id(),
                answers: self.answer()
            }, function(err) {
                if (!!cb) {
                    cb(err);
                }
            });
        };

        self.setup = function(data, cb) {
            self._id(data._id);
            self.questionText(data.questionText);
            self.answer(data.selectedAnswer);
            if (!!cb) {
                cb();
            }
        };

        self.enforceErrors = ko.observable(false);

        self.highlightAsIncomplete = ko.computed(function() {
            return !self.isFinished() && self.enforceErrors();
        });

        return self;
    };

    module.exports = exports = trueFalseQuestion;
});
