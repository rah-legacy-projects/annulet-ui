define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        async = require('async'),
        Animatable = require('../../common/animatable'),
        Answer = require('../answer');

    var matchingQuestion = function() {
        var self = new Animatable();
        self.questionText = ko.observable();
        self.lefthand = ko.observableArray([])
        self.righthand = ko.observableArray([]);

        self.colors = ko.observableArray([
            '#8F2910',
            '#CBE536',
            '#596F81',
            '#F51F45',
            '#C2F0DF'
        ]);

        self.leftHandSelections = ko.observableArray([]);
        self.rightHandSelections = ko.observableArray([]);

        self.toggle = function(side, item) {
            if (side == 'left') {
                var index = _.findIndex(self.leftHandSelections(), function(a) {
                    return a.answerText() == item.answerText();
                });

                var nullIndex = _.findIndex(self.leftHandSelections(), function(a) {
                    return a.answerText() == item.answerText();
                });

                if (index >= 0) {
                    //existing left-hand side selection, null it
                    self.lefthandSelections[ix] = null;
                    item.selectionColor(null);
                } else if (nullIndex >= 0) {
                    self.lefthandSelections[ix] = item;
                    item.selectionColor(self.colors()[ix]);
                } else {

                    if (self.lefthandSelections()
                        .length <= self.rightHandSelections.length()) {
                        //new lefthand side selection or selection that corresponds to a right hand collection
                        self.lefthandSelections.push(item);
                        item.selectionColor(self.colors()[self.lefthandSelections()
                            .length - 1]);
                    } else {
                        //move the current selection to the selected item
                        self.leftHandSelections[self.rightHandSelections()
                            .length - 1].selectionColor(null);
                        self.lefthandSelections[self.lefthandSelections()
                            .length - 1] = item;
                        item.selectionColor(self.colors()[self.lefthandSelections()
                            .length - 1]);
                    }
                }
            } else if (side == 'right') {
                var index = _.findIndex(self.rightHandSelections(), function(a) {
                    return a.answerText() == item.answerText();
                });

                var nullIndex = _.findIndex(self.rightHandSelections(), function(a) {
                    return a.answerText() == item.answerText();
                });

                if (index >= 0) {
                    //existing right-hand side selection, null it
                    self.righthandSelections[ix] = null;
                    item.selectionColor(null);
                } else if (nullIndex >= 0) {
                    self.righthandSelections[ix] = item;
                    item.selectionColor(self.colors()[ix]);
                } else {

                    if (self.righthandSelections()
                        .length <= self.leftHandSelections.length()) {
                        //new righthand side selection or selection that corresponds to a right hand collection
                        self.righthandSelections.push(item);
                        item.selectionColor(self.colors()[self.righthandSelections()
                            .length - 1]);
                    } else {
                        //move the current selection to the selected item
                        self.rightHandSelections[self.rightHandSelections()
                            .length - 1].selectionColor(null);
                        self.rightHandSelections[self.rightHandSelections()
                            .length - 1] = item;
                        item.selectionColor(self.colors()[self.righthandSelections()
                            .length - 1]);
                    }
                }
            }
        }

        self.setup = function(data, cb) {
            self.questionText = ko.observable(data.questionText);

            async.parallel({
                left: function(cb) {
                    async.parallel(_.chain(data.answers)
                        .shuffle()
                        .map(function(answer) {
                            return function(cb) {
                                var a = new Answer();
                                a.setup(answer.left, function(err) {
                                    cb(err, a);
                                });
                            }
                        })
                        .value(), function(err, answers) {
                            cb(err, answers);
                        });
                },
                right: function(cb) {
                    async.parallel(_.chain(data.answers)
                        .shuffle()
                        .map(function(answer) {
                            return function(cb) {
                                var a = new Answer();
                                a.setup(answer.right, function(err) {
                                    cb(err, a);
                                });
                            }
                        })
                        .value(), function(err, answers) {
                            cb(err, answers);
                        });
                }

            }, function(err, r) {

                self.lefthand(r.left);
                self.righthand(r.right);
                //set up the existing answers, if there are any
                _.each(data.selectedAnswers, function(sa) {
                    var left = _.find(self.answers(), function(a) {
                        return a.answerText() == sa.left;
                    });
                    var right = _.find(self.answers(), function(a) {
                        return a.answerText() == sa.right;
                    });
                    self.toggle('left', left);
                    self.toggle('right', right);
                });

                if (!!cb) {
                    cb();
                }
            });

        }


        return self;
    };

    module.exports = exports = matchingQuestion;
});
