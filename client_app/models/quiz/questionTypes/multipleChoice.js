define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        async = require('async'),
        api = require('../../../api/api'),
        Animatable = require('../../common/animatable'),
        ko = require('knockout');
    Answer = require('../answer');

    var multipleChoiceQuestion = function() {
        var self = new Animatable();
        logger.silly('seif is animatable, has visible: ' + !!self.isVisible);
        self._id = ko.observable();
        self.questionText = ko.observable();
        self.answers = ko.observableArray([]);
        self.enforceErrors = ko.observable(false);
        self.markForReview = ko.observable(false);
        
        self.isFinished = ko.computed(function() {
            var trip = self.answers();
            var x = _.any(self.answers(), function(a) {
                return a.isSelected();
            });
            logger.silly('\t\t ' + (self.questionText()||'').slice(0, 20) + '... ' + x);
            return x;
        });

        self.isFinished.extend({
            equal:{
                params:true,
                onlyIf:function(){
                    return self.enforceErrors();
                }
            }

        });
        
        self.save = function(cb) {
            api.quiz.saveAnswer({
                questionType: 'MultipleChoice',
                question: self._id(),
                answers: _.chain(self.answers())
                    .filter(function(a) {
                        return a.isSelected();
                    })
                    .map(function(a) {
                        return {
                            _id: a._id()
                        };
                    })
                    .value()
            }, function(err) {
                if (!!cb) {
                    cb(err);
                }
            });
        };

        self.letterConverter = function(number) {
            var str = '',
                map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            while (number >= 0) {
                str += map.charAt(number % 26);
                number = Math.floor(number / 26) - 1;
            }
            return str.split('')
                .reverse()
                .join('');
        };


        self.setup = function(data, cb) {
            self._id(data._id);
            self.questionText(data.questionText);

            async.parallel(_.map(data.answers, function(answer) {
                return function(cb) {
                    var a = new Answer();
                    a.setup(answer, function(err) {
                        cb(err, a);
                    });
                };
            }), function(err, r) {
                self.answers(r);
                //toggle already-selected answers
                logger.silly('selected answers: ' +JSON.stringify(data.selectedAnswers));
                _.each(data.selectedAnswers, function(sa) {
                    _.find(self.answers(), function(a) {
                        return a._id() == sa;
                    })
                        .isSelected(true);
                });

                if (!!cb) {
                    cb(err);
                }

            });
        };

        self.enforceErrors = ko.observable(false);

        self.highlightAsIncomplete = ko.computed(function() {
            return !self.isFinished() && self.enforceErrors();
        });

        return self;
    };

    module.exports = exports = multipleChoiceQuestion;
});
