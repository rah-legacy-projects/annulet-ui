define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        async = require('async'),
        api = require('../../../api/api'),
        Animatable = require('../../common/animatable'),
        ko = require('knockout');
    Answer = require('../answer');

    var multipleChoiceQuestion = function() {
        var self = new Animatable();
        logger.silly('seif is animatable, has visible: ' + !!self.isVisible);
        self._id = ko.observable();
        self.answers = ko.observableArray([]);
        self.questionText = ko.observable();
        self.enforceErrors = ko.observable(false);
        self.markForReview = ko.observable(false);

        self.isFinished = ko.computed(function() {
            var trip = self.answers();
            var x = _.any(self.answers(), function(a) {
                return a.isSelected();
            });
            return x;
        });

        self.isFinished.extend({
            equal: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }

        });

        self.save = function(cb) {
            var answer = _.find(self.answers(), function(answer) {
                return answer.isSelected();
            });
            if (!!answer) {
                api.quiz.saveAnswer({
                    questionType: 'OneChoice',
                    question: self._id(),
                    answers: {
                        _id: answer._id()
                    },
                }, function(err) {
                    if (!!cb) {
                        cb(err);
                    }
                });
            } else {
                if (!!cb) {
                    cb();
                }
            }
        };

        self.toggle = function(id) {
            _.each(self.answers(), function(answer) {
                if (answer._id() == id) {
                    answer.isSelected(true);
                } else {
                    answer.isSelected(false);
                }
            });
        }

        self.letterConverter = function(number) {
            var str = '',
                map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            while (number >= 0) {
                str += map.charAt(number % 26);
                number = Math.floor(number / 26) - 1;
            }
            return str.split('')
                .reverse()
                .join('');
        };


        self.setup = function(data, cb) {
            self._id(data._id);
            self.questionText(data.questionText);

            async.parallel(_.map(data.answers, function(answer) {
                return function(cb) {
                    var a = new Answer();
                    a.setup(answer, function(err) {
                        cb(err, a);
                    });
                };
            }), function(err, r) {
                self.answers(r);
                //toggle already-selected answers
                logger.silly('selected answers: ' + JSON.stringify(data.selectedAnswer, null, 4));
                var selected = _.find(self.answers(), function(a) {
                    return a._id() == data.selectedAnswer;
                });
                if (!!selected) {
                    selected.isSelected(true);
                }
            });

            if (!!cb) {
                cb();
            }
        };

        self.enforceErrors = ko.observable(false);

        self.highlightAsIncomplete = ko.computed(function() {
            return !self.isFinished() && self.enforceErrors();
        });

        return self;
    };

    module.exports = exports = multipleChoiceQuestion;
});
