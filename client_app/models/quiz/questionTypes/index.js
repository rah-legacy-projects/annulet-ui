define(function(require, exports, module) {
    module.exports = exports = {
        Matching: require("./matching"),
        MultipleChoice: require("./multipleChoice"),
        TrueFalse: require('./trueFalse'),
        OneChoice: require('./oneChoice')
    };
});
