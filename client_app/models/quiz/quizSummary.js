define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        api = require('../../api/api'),
        ko = require('knockout');

    ko.mapping = require('ko-mapping');

    module.exports = exports = function(quiz) {
        var self = this;
        self.scores = ko.observableArray([]);

        self.overallScore = ko.observable();
        self.quizTitle = ko.observable();
        self.processing = ko.observable(true);
        self.workflowItem = ko.observable();
        self.passingPercentage = ko.observable();
        self.workflowItem = ko.observable();
        self.workflowUrl = ko.computed(function() {
            return '/workflow/' + self.workflowItem();
        });

        self.pass = ko.computed(function(){
            return self.passingPercentage() <= self.overallScore();
        });

        self.setup = function(attempt, cb) {
            api.quiz.getQuizSummary(attempt, function(err, scoreSummary) {
                console.log('quiz summary: ' + JSON.stringify(scoreSummary, null, 3));
                self.scores(_.map(scoreSummary.scores, function(score) {
                    return ko.mapping.fromJS(score);
                }));
                self.passingPercentage(scoreSummary.passingPercentage);

                //hack: round overall score to 2 decimal places
                scoreSummary.overallScore = scoreSummary.overallScore.toFixed(2);

                self.overallScore(scoreSummary.overallScore);
                self.quizTitle(scoreSummary.quizTitle);
                self.workflowItem(scoreSummary.workflowItem);
                logger.silly('setting workflow to ' + scoreSummary.workflowItem);
                self.workflowItem(scoreSummary.workflowItem);
                if (!!cb) {
                    cb();
                }
            });
        }
    };
});
