define(function(require, exports, module) {
    module.exports = exports = {
        Answer: require("./answer"),
        questionTypes: require("./questionTypes/index"),
        Attempt: require("./attempt"),
        QuizSummary: require('./quizSummary')
    };
});
