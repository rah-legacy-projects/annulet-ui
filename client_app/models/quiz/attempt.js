define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async'),
        $ = require('jquery'),
        toastr = require('toastr'),
        questionTypes = require('./questionTypes/index'),
        api = require('../../api/api'),
        Chrome = require('../../chrome');

    var quiz = function() {
        var self = this;
        self.directions = ko.observable();
        self.title = ko.observable();
        self.questions = ko.observableArray([]);
        self._id = ko.observable();
        self.animationInProgress = ko.observable(false);
        self.animationDelay = ko.observable(0);

        self.selectedQuestion = ko.observable(0);
        self.processing = ko.observable(true);
        self.offCanvasActive = ko.observable(false);

        self.isLast = ko.computed(function() {
            if (!!self.questions && !!self.questions()) {
                return self.selectedQuestion() == (self.questions()
                    .length - 1);
            }
            return false;
        });

        self.questionsUnfinished = ko.computed(function() {
            return _.filter(self.questions(), function(q) {
                return !q.question()
                    .isFinished();
            });
        });
        self.questionsMarked = ko.computed(function() {
            return _.filter(self.questions(), function(q) {
                return q.question()
                    .markForReview();
            });
        });
        self.unfinishedDisplay = ko.computed(function() {
            var numbers = [];
            _.each(self.questions(), function(q, ix) {
                if (_.any(self.questionsUnfinished(), function(uq) {
                    return uq.question()
                        ._id() == q.question()
                        ._id();
                })) {
                    numbers.push(ix);
                }
            });
            //make a string of the numbers and slice off the trailing comma
            return _.reduce(numbers, function(memo, s) {
                    return memo + ' #' + (s + 1) + ',';
                }, '')
                .slice(0, -1);
        });
        self.markedDisplay = ko.computed(function() {
            var numbers = [];
            _.each(self.questions(), function(q, ix) {
                if (_.any(self.questionsMarked(), function(uq) {
                    return uq.question()
                        ._id() == q.question()
                        ._id();
                })) {
                    numbers.push(ix);
                }
            });
            //make a string of the numbers and slice off the trailing comma
            return _.reduce(numbers, function(memo, s) {
                    return memo + ' #' + (s + 1) + ',';
                }, '')
                .slice(0, -1);
        });

        self.userFriendlyUnfinishedDisplay = ko.computed(function() {
            if (self.questionsUnfinished()
                .length > 1) {
                return "There are " + self.questionsUnfinished()
                    .length + " questions that require answers: " + self.unfinishedDisplay();
            }else{
                return "There is " + self.questionsUnfinished()
                    .length + " question that requires answers: " + self.unfinishedDisplay();
            }
        });

        self.userFriendlyMarkedDisplay = ko.computed(function(){
            if (self.questionsMarked()
                .length > 1) {
                return "There are " + self.questionsMarked()
                    .length + " questions marked for review: " + self.markedDisplay();
            }else{
                return "There is " + self.questionsMarked()
                    .length + " question marked for review: " + self.markedDisplay();
            }
        });

        self.isFinishable = ko.computed(function() {
            return self.questionsUnfinished()
                .length == 0;
        });
        self.needsReview = ko.computed(function() {
            return self.questionsMarked()
                .length > 0;
        });

        self.toIndex = function(leavingAnimation, comingAnimation, ix, cb) {
            if (self.animationInProgress()) {
                return;
            }

            var leavingThing = self.questions()[self.selectedQuestion()].question();
            var comingThing = self.questions()[ix].question();
            self.animationInProgress(true);

            async.parallel({
                leaving: function(cb) {
                    leavingThing.animation([leavingAnimation]);
                    leavingThing.toggleState();
                    leavingThing.after(function(animation) {
                        leavingThing.after(function() {});
                        leavingThing.isVisible(false);
                        cb();
                    });
                },
                coming: function(cb) {
                    setTimeout(function() {
                        comingThing.animation([comingAnimation]);
                        comingThing.isVisible(true);
                        comingThing.toggleState();
                        comingThing.after(function(animation) {
                            comingThing.after(function() {});
                            cb();
                        });
                    }, self.animationDelay());
                }
            }, function(err, r) {
                self.selectedQuestion(ix);
                self.animationInProgress(false);
                logger.silly('animation complete!')
                if (!!cb) {
                    cb();
                }
            });
        };

        self.goTo = function(ix) {
            //hack: use jquery to scroll to the top of the application div
            $('#application').scrollTop(0);

            self.processing(true);
            self.questions()[self.selectedQuestion()].question()
                .save(function(err) {
                    self.processing(false);
                    if (!err) {

                        logger.silly('going to ' + ix);
                        if (ix < self.selectedQuestion()) {
                            self.toIndex('bounceOutRight', 'bounceInLeft', ix);
                        } else if (ix > self.selectedQuestion()) {
                            self.toIndex('bounceOutLeft', 'bounceInRight', ix);
                        } else {
                            //do nothing.  indices are equal.
                            //could do a wobble or something.
                        }
                    }
                });
        };


        self.next = function() {
            if (!self.isLast()) {
                self.goTo(self.selectedQuestion() + 1);
            }
        };
        self.previous = function() {
            //if this is the first question, return
            if (self.selectedQuestion() == 0) {
                return;
            }
            self.goTo(self.selectedQuestion() - 1);
        };

        self._showPreFinishSummary = ko.observable(false);
        self.showPreFinishSummary = ko.computed({
            read: function() {
                return self._showPreFinishSummary();
            },
            write: function(value) {
                self._showPreFinishSummary(value);
                if (value) {
                    _.each(self.questions(), function(question) {
                        question.question()
                            .enforceErrors(true);
                    });
                }
            }
        });

        self.finish = function() {
            self.processing(true);
            self.questions()[self.selectedQuestion()].question()
                .save(function(err) {
                    if (!err) {
                        logger.silly('finish - question saved');
                        //verify all questions have answers
                        if (_.all(self.questions(), function(qw) {
                            return qw.question()
                                .isFinished();
                        })) {
                            logger.silly('finish - quiz finished');
                            //if so, finish
                            Chrome.processing(true);
                            api.quiz.finalize(self._id(), function(err) {
                                logger.silly('finish - quiz finalized');
                                if (!!err) {
                                    self.processing(false);
                                    Chrome.processing(false);
                                    logger.error('problem finalizing quiz: ' + JSON.stringify(err));
                                    //todo: nice ui helpers to show what the server issue was
                                } else {
                                    self.processing(false);
                                    self.showPreFinishSummary(false);
                                    //go to summary
                                    require('../../app')
                                        .router.setLocation('/quiz/summary/' + self._id());
                                }
                            });
                        } else {
                            logger.silly('finish - quiz not finished');
                            self.processing(false);
                            //this shouldn't happen with the nice modal and all
                            //todo: nice UI helpers to show what needs attention
                        }
                    } else {
                        logger.error(err);
                    }
                });
        };

        self.setup = function(workflowItem, cb) {
            logger.silly('getting quiz');
            api.quiz.getQuiz(workflowItem, function(err, data) {
                self.directions(data.directions);
                self.title(data.quizDefinitionRange.title);
                self._id(data._id);

                async.parallel(_.map(data.questions, function(question) {
                    return function(cb) {
                        var questionType = question.__t.split('.').reverse()[0];
                        logger.silly(questionType + " exists: " + !!questionTypes[questionType]);
                        var q = new questionTypes[questionType]();
                        q.setup(question, function(err) {
                            cb(err, {
                                questionType: ko.observable(questionType),
                                question: ko.observable(q)
                            });
                        });
                    };
                }), function(err, questions) {
                    logger.silly('questions set up, setting questions');
                    self.questions(questions);
                    self.allowNext = ko.computed(function() {
                        //for now, allow skipping
                        return true;
                    });

                    //hack: is explicitly setting the selected question required at setup?
                    self.selectedQuestion(0);
                    logger.silly(!!self.questions + ' ' + !!self.selectedQuestion + ' ' + !!self.questions()[self.selectedQuestion()].isVisible);
                    self.questions()[self.selectedQuestion()].question()
                        .isVisible(true);
                    self.animationDelay(1000);

                    logger.info('quiz setup complete.');
                    if (!!cb) {
                        cb();
                    }

                });

            });
        };
        return self;
    };

    module.exports = exports = quiz;
});
