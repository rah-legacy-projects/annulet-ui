define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout');

    var answer = function() {
        var self = this;
        self._id = ko.observable();
        self.answerText = ko.observable();
        self.isSelected = ko.observable(false);
        self.toggle = function() {
            self.isSelected(!self.isSelected());
        };

        self.selectionColor = ko.observable();

        self.setup = function(data, cb) {
            self._id(data._id);
            self.answerText(data.answerText);
            if(!!cb){
                cb();
            }
        };

        return self;
    };

    module.exports = exports = answer;
});
