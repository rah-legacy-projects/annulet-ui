define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        sectionTypes = require('./sectionTypes/index'),
        async = require('async'),
        api = require('../../api/api'),
        OperatingProcedureListItem = require('./operatingProcedureListItem'),
        toastr = require('toastr');

    var operatingProcedureList = function() {
        var self = this;

        self.operatingProcedures = ko.observableArray([]);

        self.setup = function(cb){
            api.operatingProcedure.getOperatingProcedureList(function(err, operatingProcedures){
                logger.silly('operating procedures received: ' + operatingProcedures.length);
                self.operatingProcedures(_.map(operatingProcedures, function(op){
                    return new OperatingProcedureListItem(op);
                }));
                cb(err);
            });
        };
    };
    module.exports = exports = operatingProcedureList;
});
 
