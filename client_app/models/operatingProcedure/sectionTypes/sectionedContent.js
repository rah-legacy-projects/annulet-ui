define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        Content = require('./content');
    ko.mapping = require('ko-mapping');
    module.exports = exports = function(data) {
        var self = new Content();
        self.subSections = ko.observableArray([]);
        self._contentSetup = self.setup;
        self.setup = function(data, cb) {
            self.subSections(_.map(data.subSections, function(subSection) {
                return {
                    title: ko.observable(subSection.title),
                    content: ko.observable(subSection.content),
                    renderedMarkdown:ko.computed(function() {
                        if (!!subSection.content) {
                            return marked(subSection.content);
                        } else {
                            return marked('');
                        }
                    })
                };
            }));
            self._contentSetup(data, function(err) {
                if (!!cb) {
                    cb();
                }
            });
        }

        return self;

    };
});
