define(function(require, exports, module) {
    module.exports = exports = {
        Content: require("./content"),
        Definitions: require("./definitions"),
        Overview: require("./overview"),
        RelatedProcedures: require("./relatedProcedures"),
        SectionedContent: require("./sectionedContent"),
    };
});
