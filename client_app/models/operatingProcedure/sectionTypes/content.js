define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        api = require('../../../api/api'),
        ko = require('knockout'),
        marked = require('marked'),
        Animatable = require('../../common/animatable');
    require('knockout-validation');

    module.exports = exports = function() {
        var self = new Animatable();
        self.sectionType = ko.observable();
        self.title = ko.observable();
        self.content = ko.observable('');
        self._id = ko.observable();
        self._rangeId = ko.observable();
        self.views = ko.observable();
        self.renderedMarkdown = ko.computed(function() {
            if(!!self.content()){
            return marked(self.content());
            }else{
                return marked('');
            }
        });

        self.save = function(cb) {
            logger.silly('saving ' + self.title() + ' ('+self._rangeId()+')');
            api.operatingProcedure.saveView({
                sectionType: self.sectionType(),
                sectionRange: self._rangeId(),
            }, function(err) {
                logger.silly(self.title() + ' saved.');
                if (!!cb) {
                    cb(err);
                }
            });
        };

        self.enforceErrors = ko.observable(false);

        self.views.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            min: {
                params: 1,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        self.highlightAsUnread = ko.computed(function() {
            return !self.views.isValid() && self.enforceErrors();
        });

        self.setup = function(data, cb) {
            self.sectionType('content');
            self.title(data.title);
            self.content(data.content);
            self._id(data._id);
            self._rangeId(data._rangeId);
            self.views(data.views);
            if(!!cb){
                cb();
            }
        };

        return self;
    };
});
