define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        Content = require('./content');
    ko.mapping = require('ko-mapping');
    module.exports = exports = function(data) {
        var self = new Content();
        self.terms = ko.observableArray();
        self._contentSetup = self.setup;
        self.setup = function(data, cb) {
            logger.silly('setting up definitions');
            self._contentSetup(data, function(err) {
                var termDef = function(data) {
                    var self = this;
                    self.term = ko.observable(data.term);
                    self.meaning = ko.observable(data.meaning);
                    self.renderedMarkdown = ko.computed(function() {
                        if (!!self.meaning && !!self.meaning()) {
                            return marked(self.meaning());
                        } else {
                            return marked('');
                        }
                    });
                    return self;
                };

                self.terms(_.map(data.terms, function(term) {
                    return new termDef(term);
                }));

                if (!!cb) {
                    cb();
                }
            });
        }

        return self;
    };
});
