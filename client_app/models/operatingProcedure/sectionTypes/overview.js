define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        Content = require('./content');
    ko.mapping = require('ko-mapping');
    module.exports = exports = function(data) {
        var self = new Content();
        self._contentSetup = self.setup;
        self.setup = function(data, cb) {
            self._contentSetup(data, function(err) {
                if (!!cb) {
                    cb();
                }
            });
        }

        return self;
    };
});
