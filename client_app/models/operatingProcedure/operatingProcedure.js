define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        sectionTypes = require('./sectionTypes/index'),
        async = require('async'),
        api = require('../../api/api'),
        toastr = require('toastr'),
        $ = require('jquery'),
        Chrome = require('../../chrome');
    require('knockout-validation');
    require('knockout-animate');

    var operatingProcedure = function(operatingProcedure) {
        var self = this;
        self.title = ko.observable();
        self._id = ko.observable();
        self._rangeId = ko.observable();

        self.selectedSection = ko.observable();
        self.processing = ko.observable(true);
        self.animationInProgress = ko.observable(false);
        self.animationDelay = ko.observable(0);
        self.sections = ko.observableArray([]);
        self.offCanvasActive = ko.observable(false);

        //self.error = ko.observable();

        self.tableOfContents = ko.computed(function() {
            return _.map(self.sections(), function(section) {
                return {
                    title: ko.observable(section.title())
                };
            });
        });

        self.errors = ko.validation.group(self);

        self._selectedSection = ko.computed(function() {
            var trip = self.selectedSection();
            if (self.sections()
                .length > 0 && (!!self.selectedSection() || self.selectedSection() == 0)) {
                self.processing(true);
                self.sections()[self.selectedSection()].views(self.sections()[self.selectedSection()].views() + 1);
                self.sections()[self.selectedSection()]
                    .save(function(err) {
                        if (!err) {
                            self.processing(false);
                        }
                    });
            }
        });

        self.isLast = ko.computed(function() {
            if (!!self.sections && !!self.sections()) {
                return self.selectedSection() == (self.sections()
                    .length - 1);
            }
            return false;
        });

        self.toIndex = function(leavingAnimation, comingAnimation, ix, cb) {
            if (self.animationInProgress()) {
                return;
            }

            var leavingThing = self.sections()[self.selectedSection()];
            var comingThing = self.sections()[ix];
            self.animationInProgress(true);

            async.parallel({
                leaving: function(cb) {
                    logger.silly('leaving - ' + leavingThing.title());
                    leavingThing.animation([leavingAnimation]);
                    leavingThing.toggleState();
                    leavingThing.after(function(animation) {
                        leavingThing.after(function() {});
                        leavingThing.isVisible(false);
                        logger.silly('left - ' + leavingThing.title());
                        cb();
                    });
                },
                coming: function(cb) {
                    setTimeout(function() {
                        logger.silly('coming - ' + comingThing.title());
                        comingThing.animation([comingAnimation]);
                        comingThing.isVisible(true);
                        comingThing.toggleState();
                        logger.silly('coming - state toggled');
                        comingThing.after(function(animation) {
                            logger.silly('came - ' + comingThing.title());
                            comingThing.after(function() {});
                            cb();
                        });
                    }, self.animationDelay());
                }
            }, function(err, r) {
                self.selectedSection(ix);
                self.animationInProgress(false);
                logger.silly('animation complete!')
                if (!!cb) {
                    cb();
                }
            });
        };

        self.goTo = function(ix) {
            //hack: use jquery to scroll to the top of the application div
            $('#application').scrollTop(0);

            logger.silly('going to ' + ix);
            if (ix < self.selectedSection()) {
                self.toIndex('bounceOutRight', 'bounceInLeft', ix);
            } else if (ix > self.selectedSection()) {
                self.toIndex('bounceOutLeft', 'bounceInRight', ix);
            } else {
                //do nothing.  indices are equal.
                //could do a wobble or something.
            }
        };
        self.next = function() {
            if (!self.isLast()) {
                logger.silly('next!');
                self.goTo(self.selectedSection() + 1);
            }
        };
        self.previous = function() {
            //if this is the first section, return
            if (self.selectedSection() == 0) {
                return;
            }
            self.goTo(self.selectedSection() - 1);
        };

        self.finish = function() {
            _.each(self.sections(), function(section) {
                section.enforceErrors(true);
            });

            var everythingViewed = (self.errors()
                .length === 0);
            logger.silly('everything viewed? ' + everythingViewed);

            if (self.allowNext() && everythingViewed) {
                Chrome.processing(true);
                self.processing(true);
                //if so, finish
                api.operatingProcedure.finalize(self._rangeId(), function(err, data) {
                    logger.silly('finish - operatingProcedure finalized');
                    if (!!err) {
                        self.processing(false);
                        logger.error('problem finalizing operatingProcedure: ' + JSON.stringify(err));
                        //todo: nice ui helpers to show what the server issue was
                    } else {
                        self.processing(false);
                        //go to summary
                        require('../../app')
                            .router.setLocation('/operatingProcedure');
                    }
                });
            } else {
                ///self.error(true);
                toastr.error('Please make sure you read all sections before completing this operating procedure.');
            }
        };

        self.setup = function(operatingProcedure, cb) {

            logger.silly('getting operatingProcedure');
            api.operatingProcedure.getOperatingProcedure(operatingProcedure, function(err, data) {
                self.title(data.title);
                self._rangeId(data._rangeId);
                self._id(data._id);

                async.parallel(_.map(data.sections, function(section) {
                    return function(cb) {
                        section.type = section.__t || 'content';
                        var sectionTypeName = section.type.split('.')
                            .reverse()[0];
                        sectionTypeName = sectionTypeName.charAt(0)
                            .toUpperCase() + sectionTypeName.slice(1);

                        var createdSection = null;
                        logger.silly('section type: ' + sectionTypeName);
                        if (!!sectionTypes[sectionTypeName]) {
                            //if the type exists, use that constructor to make self
                            createdSection = new sectionTypes[sectionTypeName]();
                        } else {
                            createdSection = new sectionTypes.content();
                        }

                        createdSection.setup(section, function(err) {
                            createdSection.sectionType(sectionTypeName);
                            cb(err, createdSection);
                        });
                    }
                }), function(err, sections) {
                    self.sections(sections);
                    self.allowNext = ko.computed(function() {
                        return true;
                    });


                    self.title = ko.observable(data.title);
                    self.selectedSection(0);
                    self.sections()[self.selectedSection()].isVisible(true);
                    self.animationDelay(1000);
                    self.processing(false);

                    if (!!cb) {
                        cb();
                    }

                });

            });
        };
        return self;
    };

    module.exports = exports = operatingProcedure;
});
