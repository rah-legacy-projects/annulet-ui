define(function(require, exports, module) {
    module.exports = exports = {
        sectionTypes: require("./sectionTypes/index"),
        OperatingProcedure: require("./operatingProcedure"),
        OperatingProcedureList: require('./operatingProcedureList'),
    };
});
