define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async');

    var operatingProcedureListItem = function(item) {
        var self = this;
        self.title = ko.observable(item.title);
        self.instanceId = ko.observable(item.instanceId);
        self.definitionId = ko.observable(item.definitionId);
        self.shortName = ko.observable(item.shortName);
        self.completedDate = ko.observable(item.completedDate);
        self.isCompleted = ko.computed(function() {
            return !!self.completedDate();
        });
        self.created = ko.observable(item.created);
        self.description = ko.observable(item.description);
        self.url = ko.computed(function() {
            return '/operatingProcedure/' + self.definitionId();
        });
    };

    module.exports = exports = operatingProcedureListItem;

});
