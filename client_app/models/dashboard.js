define(function(require, exports, module) {
    var _ = require('lodash'),
        async = require('async'),
        ko = require('knockout'),
        store = require('store'),
        logger = require('../logger'),
        alerts = require('./alert/index'),
        complaints = require('./complaint/index'),
        status = require('./itemActivity/index'),
        auxiliaryDocuments = require('./auxiliaryDocument/index'),
        api = require('../api/api');

    ko.mapping = require('ko-mapping');
    module.exports = exports = function() {
        var self = this;

        self.selectedSection = ko.observable('My Outstanding Items');
        self.offCanvasActive = ko.observable(false);

        self.workflows = ko.observableArray();
        self.alerts = ko.observable();
        self.complaints = ko.observable();
        self.auxiliaryDocuments = ko.observable();
        self.userOutstandingOPs = ko.observable();
        self.userOutstandingWorkflows = ko.observable();
        self.adminOutstandingOPs = ko.observable();
        self.adminOutstandingWorkflows = ko.observable();
        self.adminOPActivity = ko.observable();
        self.adminWorkflowActivity = ko.observable();

        self.setup = function(cb) {

            async.parallel({
                alerts: function(cb) {
                    logger.silly('setting up alerts');
                    var al = new alerts.AlertInstanceList();
                    al.setup(5, function(err) {
                        logger.silly('alert list set up, setting alerts....');
                        self.alerts(al);
                        cb(err);
                    });
                },
                complaints: function(cb) {
                    logger.silly('setting up complaints');
                    var cl = new complaints.RecentComplaintList();
                    cl.setup(function(err) {
                        self.complaints(cl);
                        cb(err);
                    });
                },

                auxiliaryDocuments: function(cb) {
                    logger.silly('setting up auxdox');
                    var aux = new auxiliaryDocuments.RecentlyModifiedAuxiliaryDocumentList();
                    aux.setup(function(err) {
                        self.auxiliaryDocuments(aux);
                        cb(err);
                    });
                },
                userOutstandingOPs: function(cb) {
                    logger.silly('setting up user outstanding OPs');
                    var userOutstandingOPs = new status.OpStatusList();
                    userOutstandingOPs.setup(null, function(err) {
                        userOutstandingOPs.limit(5);
                        self.userOutstandingOPs(userOutstandingOPs);
                        cb(err);
                    });
                },
                userOutstandingWorkflows: function(cb) {
                    logger.silly('setting up user outstanding wfs');
                    var ut = new status.WorkflowStatusList();
                    ut.setup(null, function(err) {
                        ut.limit(5);
                        self.userOutstandingWorkflows(ut);
                        cb(err);
                    });
                },
                adminOutstandingOPs: function(cb) {
                    if (require('../app')
                        .auth()
                        .canAdministerUsers()) {
                        var a = new status.AdminOPStatusList();
                        a.setup(5, function(err) {
                            logger.silly('got admin op status list');
                            self.adminOutstandingOPs(a);
                            cb(err);
                        });
                    } else {
                        cb();
                    }
                },
                adminOutsandingWorkflows: function(cb) {
                    if (require('../app')
                        .auth()
                        .canAdministerUsers()) {
                        var a = new status.AdminWorkflowStatusList();
                        a.setup(5, function(err) {
                            self.adminOutstandingWorkflows(a);
                            cb(err);
                        });
                    } else {
                        cb();
                    }
                },
                adminOPActivity: function(cb) {
                    if (require('../app')
                        .auth()
                        .canAdministerUsers()) {
                        var a = new status.AdminOPActivityList();
                        a.setup(5, function(err) {
                            self.adminOPActivity(a);
                            cb(err);
                        });
                    } else {
                        cb();
                    }
                },
                adminWorkflowActivity: function(cb) {
                    if (require('../app')
                        .auth()
                        .canAdministerUsers()) {
                        var a = new status.AdminWorkflowActivityList();
                        a.setup(5, function(err) {
                            self.adminWorkflowActivity(a);
                            cb(err);
                        });
                    } else {
                        cb();
                    }
                }
            }, function(err) {
                logger.silly('dashboard setup complete.');
                if (!!cb) {
                    cb();
                }
            });
        };

        return self;
    };
});
