define(function(require, exports, module){
    var ko = require('knockout'),
        logger = require('../logger');
    //hack: pass the authref in to get around require troubles
    module.exports = exports = function(data, authRef, appRef){
        var self = this;
        self.customerName = ko.observable(data.customerName);
        self.id = ko.observable(data.id);
        self.access =ko.observable(data.access);
        self.customerSelect = function(){
            logger.silly('setting up customer');
            authRef.customerAndAccess(data);
            logger.silly('heading to: ' + appRef.lastLocation());
            appRef.router.setLocation(appRef.lastLocation());
        };
        return self;
    };
});
