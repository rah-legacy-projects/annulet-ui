define(function(require, exports, module) {
    var _ = require('lodash'),
        async = require('async'),
        ko = require('knockout'),
        store = require('store'),
        logger = require('../logger'),
        Customer = require('./customer'),
        api = require('../api/api'),
        Chrome = require('../chrome');

    ko.mapping = require('ko-mapping');
    module.exports = exports = function(app, options) {
        var self = this;

        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.email = ko.observable();
        self.password = ko.observable();

        self._token = ko.observable();
        self.token = ko.computed({
            read: function() {
                var unwrapper = self._token();
                return store.get('auth-token');
            },
            write: function(value) {
                store.set('auth-token', value);
                self._token(value);
            }
        });

        self.loggedIn = ko.observable(false);

        self.errorMessage = ko.observable("Please try again.");
        self.showError = ko.observable(false);

        self.customers = ko.observableArray([]);

        self._customer = ko.observable();
        self.customer = ko.computed({
            read: function() {
                self._customer();
                return store.get('auth-customer');
            },
            write: function(value) {
                store.set('auth-customer', value);
                self._customer(value);
            }
        });

        self._customerAndAccess = ko.observable();
        self.customerAndAccess = ko.computed({
            read: function() {
                self._customerAndAccess();
                var access = store.get('auth-customer-access');
                return access;
            },
            write: function(value) {
                if (!!value) {
                    store.set('auth-customer-access', value);
                } else {
                    store.remove('auth-customer-access');
                }
                if (!!value && !!value.id) {
                    self.customer(value.id);
                } else {
                    self.customer(null);
                }
                self._customerAndAccess(value);;
            }
        });

        self.canAdministerUsers = ko.computed(function() {
            if (!!self.customerAndAccess()) {
                return _.any(self.customerAndAccess()
                    .access, function(a) {
                        logger.silly('\t\t ' + a);
                        return a == 'Owner' || a == 'Administrator';
                    });
            }
        });

        self.login = function() {
            self.token(null);
            self.customerAndAccess(null);
            logger.silly('about to issue request for login');
            api.auth.login(self.email(), self.password(), function(err, token) {
                logger.silly('log in returned')
                self.password('');
                if (!!err) {
                    if (!!err.responseJSON) {
                        self.errorMessage(err.responseJSON.err.message);
                    } else {
                        self.errorMessage('An error occurred.  Please contact your Annulet administrators.');
                    }
                    self.showError(true);
                } else {
                    Chrome.processing(true);
                    logger.silly('setting token: ' + token);
                    self.token(token);

                    self.setUpCustomers(true, function(err) {
                        self.loggedIn(true);
                        app.router.setLocation(app.lastLocation());
                    });
                }
            });
        };

        self.logOut = function() {
            api.auth.logout(function(err) {
                logger.silly('logging out');
                self.firstName(null);
                self.lastName(null);
                self.email(null);
                self.token(null);
                self.customerAndAccess(null);

                self.loggedIn(false);
                app.lastLocation('/dashboard');
                app.router.setLocation('/login');
            });
        };

        self.getUserInfo = function(cb) {
            logger.silly('model get user info');
            api.auth.getUserInfo(function(err, userInfo) {
                logger.silly('got model info');
                //todo: do something with get user info error
                self.email(userInfo.email);
                self.firstName(userInfo.firstName);
                self.lastName(userInfo.lastName);
                cb(err, userInfo);
            });
        };

        self.setUpCustomers = function(fromLogin, cb) {
            if (fromLogin || !self.customerAndAccess()) {
                api.customer.getCustomers(function(err, customers) {
                    logger.silly('customers received: ' + JSON.stringify(customers));
                    if (customers.length == 0) {
                        //user is active but not in a customer?
                        logger.warn('user is active but does not have valid customers');
                        self.token(null);
                        self.loggedIn(false);
                        self.errorMessage('User is not active for any customers.  Please contact support.');
                        self.showError(true);
                        return app.router.setLocation('/login');
                    } else if (customers.length == 1) {
                        logger.silly('one customer: ' + JSON.stringify(customers));
                        //only one customer, set up the customer and carry on
                        self.customerAndAccess(customers[0]);
                    } else if (customers.length > 1) {
                        //more than one customer, redirect user to customer selection
                        logger.warn('user is active but does not has more than one valid customer');
                        self.customers(_.map(customers, function(customer) {
                            return new Customer(customer, self, app);
                        }));
                        return app.router.setLocation('/customerSelection');
                    }
                    if (!!cb) {
                        return cb();
                    }
                });
            } else {
                if (!!cb) {
                    return cb();
                }
            }

        };

        return self;
    };
});
