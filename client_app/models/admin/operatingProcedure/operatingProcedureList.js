define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        async = require('async'),
        api = require('../../../api/api'),
        OperatingProcedureListItem = require('./operatingProcedureListItem'),
        toastr = require('toastr');

    var operatingProcedureList = function() {
        var self = this;

        self.operatingProcedures = ko.observableArray([]);

        self.setup = function(cb){
            api.admin.operatingProcedure.list({includeAll: true}, function(err, operatingProcedures){
                logger.silly('operating procedures received: ' + operatingProcedures.length);
                self.operatingProcedures(_.map(operatingProcedures, function(op){
                    //hack: for now, use the published tuple only
                    return new OperatingProcedureListItem(op.published);
                }));
                cb(err);
            });
        };
    };
    module.exports = exports = operatingProcedureList;
});
 
