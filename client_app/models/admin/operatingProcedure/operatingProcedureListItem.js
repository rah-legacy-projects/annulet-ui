define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        api = require('../../../api/api'),
        Chrome = require('../../../chrome'),
        async = require('async');

    var operatingProcedureListItem = function(item) {
        console.log('item: ' + JSON.stringify(item, null, 3))
        var self = this;
        self.title = ko.observable(item.title);
        self._id = ko.observable(item._id);
        self.created = ko.observable(item.created);
        self.description = ko.observable(item.description);
        self.active = ko.observable(item.active);
        self.deleted = ko.observable(item.deleted);

        self.processing = ko.observable(false);

        self.activate = function() {
            logger.silly('activate!');
            if (!self.processing()) {
                self.processing(true);
                api.admin.operatingProcedure.activate(self._id(), function(err, data) {
                    self.active(true);
                    self.processing(false);
                });
            }
        };
    
        self.deactivate = function() {
            logger.silly('deactivate!');
            if (!self.processing()) {
                self.processing(true);
                api.admin.operatingProcedure.deactivate(self._id(), function(err, data) {
                    self.active(false);
                    self.processing(false);
                });
            }
        };
    };

    module.exports = exports = operatingProcedureListItem;

});
