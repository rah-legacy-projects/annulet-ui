define(function(require, exports, module) {
    module.exports = exports = {
        OperatingProcedureListItem: require("./operatingProcedureListItem"),
        OperatingProcedureList: require('./operatingProcedureList'),
    };
});
