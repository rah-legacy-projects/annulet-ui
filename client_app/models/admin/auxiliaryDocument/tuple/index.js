define(function(require, exports, module) {
	module.exports = exports = {
		Published: require("./published"),
		Draft: require("./draft"),
		Tuple: require("./tuple"),
	};
});
