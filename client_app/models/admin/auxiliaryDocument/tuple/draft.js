define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        toastr = require('toastr'),
        moment = require('moment');
    require('bs3-datetime-picker');
    //require('ko-blockUI');
    require('knockout-validation');

    var auxDoc = function() {
        var self = this;

        self._id = ko.observable();
        self._rangeId = ko.observable();
        self.file = ko.observable();
        self.displayName = ko.observable();
        self.description = ko.observable();
        self.extension = ko.observable();
        self.startDate = ko.observable();
        self.endDate = ko.observable();
        self.startForever = ko.observable();
        self.endForever = ko.observable();
        self.modified = ko.observable();
        self.isDeleted = ko.observable();
        self.showPublishModal = ko.observable(false);

        self.errors = ko.validation.group(self);
        self.enforceErrors = ko.observable(false);
        //{{{ model validation
        self.file.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.displayName.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.description.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.startDate.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors() && !!self.startForever();
                }
            }
        });
        self.endDate.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors() && !!self.endForever();
                }
            }
        });
        //}}}

        self.lastModifiedDisplay = function() {
            return "Last modified " + moment(self.modified())
                .format('MM/DD/YYYY h:mm a');
        };

        self.descriptionDisplay = ko.computed(function() {
            if ((self.description() || '') == '') {
                return 'No description given.';
            }
            return self.description();
        });

        self.setup = function(data, cb) {
            logger.silly('data: ' + JSON.stringify(data, null, 3))
            self._id(data._id);
            self._rangeId(data._rangeId);
            self.file(data.file);
            self.displayName(data.displayName);
            self.description(data.description);
            self.modified(data.modified);
            self.extension(data.extension);
            if (!/forever/i.test(data.startDate)) {
                self.startDate(moment(data.startDate)
                    .toDate());
            } else {
                self.startForever(true);
            }
            if (!/forever/i.test(data.endDate)) {
                self.endDate(moment(data.endDate)
                    .toDate());
            } else {
                self.endForever(true);
            }

            cb();
        };

        self.startDateDisplay = ko.computed(function(){
            if(self.startForever()){
                return 'forever'
            }
            return moment(self.startDate()).format('MM-DD-YYYY');
        });
        self.endDateDisplay = ko.computed(function(){
            if(self.endForever()){
                return 'forever'
            }
            return moment(self.endDate()).format('MM-DD-YYYY');
        });

        self.processing = ko.observable(false);

        self.save = function(cb) {
            self.processing(true);
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {

                var saveBody = {
                    _id: self._id(),
                    _rangeId: self._rangeId(),
                    file: self.file(),
                    displayName: self.displayName(),
                    description: self.description(),
                    extension: self.extension(),
                    startDate: self.startForever() ? 'forever' : self.startDate(),
                    endDate: self.endForever() ? 'forever' : self.endDate()
                };
                logger.silly('saving: ' + JSON.stringify(saveBody, null, 3));

                api.admin.auxiliaryDocument.update(saveBody, function(err, data) {
                    if (!!cb && _.isFunction(cb)) {
                        cb();
                    } else {
                        toastr.success('Document saved.');
                        self.processing(false);
                    }
                });
            } else {
                toastr.error('Please correct the required data and try again.');
                logger.silly('there are auxdox errors still');
                self.processing(false);
            }
        };

        //hack: need a better name for publish click or better convention for event handlers
        self.publishClick = function() {
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {
                self.showPublishModal(true);
            } else {
                //errors are enforced.
            }
        };

        self.publish = function() {
            self.processing(true);
            self.showPublishModal(false);
            self.save(function(err) {
                api.admin.auxiliaryDocument.publish({
                    _id: self._id(),
                    _rangeId: self._rangeId()
                }, function(err, data) {
                    require('../../../../app')
                        .router.setLocation('/administration/auxiliaryDocuments');
                    self.processing(false);
                });
            });
        };


        self.showRemove = ko.observable(false);
        self.removeQuestion = ko.computed(function() {
            return "Remove " + self.displayName() + '?';
        });
        self.publishQuestion = ko.computed(function() {
            return "Publish " + self.displayName() + '?';
        });

        self.discard = function(cb) {
            self.processing(true);
            api.admin.auxiliaryDocument.close(self._id(), function(err) {
                self.isDeleted(true);
                if (!!cb) {
                    cb(err);
                } else {
                    self.processing(false);
                }
            });
        };

        self.detailDiscard = function() {
            self.discard(function(err) {
                require('../../../../app')
                    .router.setLocation('/administration/auxiliaryDocuments');
                self.processing(false);
            });
        };

        self.editLink = ko.computed(function() {
            return '/administration/auxiliaryDocuments/' + self._id();
        });
        self.downloadLink = ko.computed(function() {
            return (new URITemplate(new URI(api.baseApiUriString)
                    .valueOf() + 'auxiliaryDocument/{file}?access_token={token}&annulet_auth_customer={customer}')
                .expand({
                    file: self._id(),
                    token: store.get('auth-token'),
                    customer: store.get('auth-customer')
                }));
        });

        self.dropzone = {
            url: ko.observable(api.baseApiUriString + '/admin/auxiliaryDocument/upload'),
            dictDefaultMessage: ko.computed(function(){
                logger.silly('def msg');
                if(!self.file()){
                    return "Drop file or click here to upload."
                }

                logger.silly('file exists for def msg');
                return "File already uploaded.  To overwrite, drop antother file or click here to overwrite.";
            }),
            headers: ko.observable({
                'annulet-auth-token': store.get('auth-token'),
                'annulet-auth-customer': store.get('auth-customer')
            }),
            addedFile: function() {
                self.processing(true);
            },
            complete: function(response) {
                logger.silly('auxiliary document finished, relisting documents');
                //hack: parse the xhr response
                var file = JSON.parse(response.xhr.response)
                    .data;
                self.file(file._id);
                self.extension(file.extension);
                if (!self.displayName()) {
                    self.displayName(file.displayName);
                }
                self.processing(false);
            },
            uploadComplete: function(file){
                this.removeFile(file);
            }
        };

        self.fileList = ko.observableArray([]);


    };
    module.exports = exports = auxDoc;
});
