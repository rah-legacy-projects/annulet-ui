define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        moment = require('moment');
    require('knockout-validation');

    var auxDoc = function() {
        var self = this;

        self._id = ko.observable();
        self._rangeId = ko.observable();
        self.file = ko.observable();
        self.displayName = ko.observable();
        self.description = ko.observable();
        self.modified = ko.observable();
        self.isDeleted = ko.observable();
        self.processing = ko.observable(false);
        self.startDate = ko.observable();
        self.endDate = ko.observable();
        self.startForever = ko.observable();
        self.endForever = ko.observable();

        self.lastModifiedDisplay = function() {
            return "Last modified " + moment(self.modified())
                .format('MM/DD/YYYY h:mm a');
        };

        self.descriptionDisplay = ko.computed(function() {
            if ((self.description() || '') == '') {
                return 'No description given.';
            }
            return self.description();
        });

        self.setup = function(data, cb) {
            self._id(data._id);
            self._rangeId(data._rangeId);
            self.file(data.file);
            self.displayName(data.displayName);
            self.description(data.description);
            self.modified(data.modified);
            if (!/forever/i.test(data.startDate)) {
                self.startDate(moment(data.startDate)
                    .toDate());
            } else {
                self.startForever(true);
            }
            if (!/forever/i.test(data.endDate)) {
                self.endDate(moment(data.endDate)
                    .toDate());
            } else {
                self.endForever(true);
            }
            cb();
        };


        self.startDateDisplay = ko.computed(function() {
            if (self.startForever()) {
                return 'forever'
            }
            return moment(self.startDate()).format('MM-DD-YYYY');
        });
        self.endDateDisplay = ko.computed(function() {
            if (self.endForever()) {
                return 'forever'
            }
            return moment(self.endDate()).format('MM-DD-YYYY');
        });


        self.removeQuestion = ko.computed(function() {
            return "Remove " + self.displayName() + '?';
        });
        self.remove = function(cb) {
            self.processing(true);
            api.admin.auxiliaryDocument.close(self._id(), function(err) {
                self.isDeleted(true);
                if (!!cb) {
                    cb();
                }
                self.processing(false);
            });
        };

        self.createDraft = function() {
            self.processing(true);
            api.admin.auxiliaryDocument.create({
                _id: self._id
            }, function(err, data) {
                //redirect to data ID for edit
                require('../../../../app')
                    .router.setLocation('/administration/auxiliaryDocuments/' + data._id);
                self.processing(false);
            });
        };

        self.downloadLink = ko.computed(function() {
            return (new URITemplate(new URI(api.baseApiUriString)
                    .valueOf() + 'auxiliaryDocument/{file}?access_token={token}&annulet_auth_customer={customer}')
                .expand({
                    file: self._id(),
                    token: store.get('auth-token'),
                    customer: store.get('auth-customer')
                }));
        });
    };
    module.exports = exports = auxDoc;
});
