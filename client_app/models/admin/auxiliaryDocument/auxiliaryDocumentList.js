define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        async = require('async'),
        marked = require('marked'),
        api = require('../../../api/api'),
        Tuple = require('./tuple/tuple'),
        moment = require('moment');
    require('knockout-validation');

    var auxDocList = function() {
        var self = this;
        self.processing = ko.observable(false);
        self.documents = ko.observableArray([]);

        self.setup = function(cb) {
            api.admin.auxiliaryDocument.list(function(err, docs) {
                logger.silly('docs retrieved');
                async.parallel(_.map(docs, function(doc) {
                    logger.silly('\tmapping...');
                    return function(cb) {
                        var x = new Tuple();
                        x.setup(doc, function(err) {
                            cb(err, x);
                        });
                    };
                }), function(err, r) {
                    logger.silly('done creating doc models');
                    self.documents(r);
                    cb(err);
                });
            });
        };

        self.shownDocuments = ko.computed(function(){
            return _.filter(self.documents(), function(doc){
               return doc.show(); 
            }).length;
        });

        self.create = function(id) {
            self.processing(true);
            api.admin.auxiliaryDocument.create({
                _id: 'new'
            }, function(err, data) {
                //redirect to data ID for edit
                require('../../../app')
                    .router.setLocation('/administration/auxiliaryDocuments/' + data._id);
                self.processing(false);
            });
        };
    };

    exports = module.exports = auxDocList;
});
