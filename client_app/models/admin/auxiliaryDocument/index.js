define(function(require, exports, module) {
	module.exports = exports = {
		AuxiliaryDocumentList: require("./auxiliaryDocumentList"),
		tuple: require("./tuple/index"),
	};
});
