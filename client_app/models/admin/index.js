define(function(require, exports, module) {
	module.exports = exports = {
		auxiliaryDocument: require("./auxiliaryDocument/index"),
		operatingProcedure: require("./operatingProcedure/index"),
		alert: require("./alert/index"),
	};
});
