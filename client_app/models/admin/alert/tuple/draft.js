define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        toastr = require('toastr'),
        moment = require('moment');
    require('bs3-datetime-picker');
    //require('ko-blockUI');
    require('knockout-validation');

    var alert = function() {
        var self = this;

        self._id = ko.observable();
        self._rangeId = ko.observable();
        self.markdown = ko.observable();
        self.alertLevel = ko.observable();
        self.startDate = ko.observable();
        self.endDate = ko.observable();
        self.startForever = ko.observable();
        self.endForever = ko.observable();
        self.modified = ko.observable();
        self.isDeleted = ko.observable();
        self.showPublishModal = ko.observable(false);

        self.errors = ko.validation.group(self);
        self.enforceErrors = ko.observable(false);

        self.possibleLevels = ko.observable([
            'Informational',
        ]);

        //{{{ model validation
        self.markdown.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.alertLevel.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.startDate.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors() && !!self.startForever();
                }
            }
        });
        self.endDate.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors() && !!self.endForever();
                }
            }
        });
        //}}}


        self.startDateDisplay = ko.computed(function() {
            if (self.startForever()) {
                return 'forever'
            }
            return moment(self.startDate()).format('MM-DD-YYYY');
        });
        self.endDateDisplay = ko.computed(function() {
            if (self.endForever()) {
                return 'forever'
            }
            return moment(self.endDate()).format('MM-DD-YYYY');
        });

        self.lastModifiedDisplay = function() {
            return "Last modified " + moment(self.modified())
                .format('MM/DD/YYYY h:mm a');
        };

        self.markdownDisplay = ko.computed(function() {
            if ((self.markdown() || '') == '') {
                return 'No alert text specified.';
            }
            return self.markdown();
        });

        self.setup = function(data, cb) {
            self._id(data._id);
            self._rangeId(data._rangeId);
            self.alertLevel(data.alertLevel);
            self.markdown(data.markdown);
            self.modified(data.modified);
            if (!/forever/i.test(data.startDate)) {
                self.startDate(moment(data.startDate)
                    .toDate());
            } else {
                self.startForever(true);
            }
            if (!/forever/i.test(data.endDate)) {
                self.endDate(moment(data.endDate)
                    .toDate());
            } else {
                self.endForever(true);
            }

            cb();
        };

        self.processing = ko.observable(false);

        self.save = function(cb) {
            self.processing(true);
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {

                var saveBody = {
                    _id: self._id(),
                    _rangeId: self._rangeId(),
                    markdown: self.markdown(),
                    alertLevel: self.alertLevel(),
                    startDate: self.startForever() ? 'forever' : self.startDate(),
                    endDate: self.endForever() ? 'forever' : self.endDate()
                };
                logger.silly('saving: ' + JSON.stringify(saveBody, null, 3));

                api.admin.alert.update(saveBody, function(err, data) {
                    if (!!cb && _.isFunction(cb)) {
                        cb();
                    } else {
                        toastr.success('Document saved.');
                        self.processing(false);
                    }
                });
            } else {
                toastr.error('Please correct the required data and try again.');
                logger.silly('there are auxdox errors still');
                self.processing(false);
            }
        };

        //hack: need a better name for publish click or better convention for event handlers
        self.publishClick = function() {
            self.enforceErrors(true);
            if (self.errors()
                .length == 0) {
                self.showPublishModal(true);
            } else {
                //errors are enforced.
            }
        };

        self.publish = function() {
            self.processing(true);
            self.showPublishModal(false);
            self.save(function(err) {
                api.admin.alert.publish({
                    _id: self._id(),
                    _rangeId: self._rangeId()
                }, function(err, data) {
                    require('../../../../app')
                        .router.setLocation('/administration/alerts');
                    self.processing(false);
                });
            });
        };


        self.showRemove = ko.observable(false);

        self.shortenedDisplay = ko.computed(function() {
            var shortened = self.markdownDisplay() || '';
            if (shortened == 'No alert text specified.' || shortened.length <= 20) {
                //use as-is.
            } else {
                shortened = shortened.slice(0, 20) + '...';
            }

            return shortened;
        });
        self.removeQuestion = ko.computed(function() {

            return 'Remove "' + self.shortenedDisplay() + '"?';
        });
        self.publishQuestion = ko.computed(function() {
            return 'Publish "' + self.shortenedDisplay() + '"?';
        });

        self.discard = function(cb) {
            self.processing(true);
            api.admin.alert.close(self._id(), function(err) {
                self.isDeleted(true);
                if (!!cb) {
                    cb(err);
                } else {
                    self.processing(false);
                }
            });
        };

        self.detailDiscard = function() {
            self.discard(function(err) {
                require('../../../../app')
                    .router.setLocation('/administration/alerts');
                self.processing(false);
            });
        };

        self.editLink = ko.computed(function() {
            return '/administration/alerts/' + self._id();
        });
    };
    module.exports = exports = alert;
});
