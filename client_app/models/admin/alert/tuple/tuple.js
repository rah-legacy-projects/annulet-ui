define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        async = require('async'),
        moment = require('moment'),
        Draft = require('./draft'),
        Published = require('./published');
    require('knockout-validation');

    var tuple = function() {
        var self = this;

        self.published = ko.observable();
        self.draft = ko.observable();
        self._processing = ko.observable(false);
        self.show = ko.computed(function() {
            //so long as the published version exists that is not deleted or a draft that is not deleted, show it
            return (!!self.published() && !self.published()
                    .isDeleted()) ||
                (!!self.draft());
        });

        self.allowCreateDraft = ko.computed(function() {
            return !!self.published() && !self.draft();
        });

        self.makeCollapsedMember = function(member, options) {
            if (!!self[member]) {
                return;
            }
            self[member] = ko.computed(function() {
                var d = ((((self.draft() || {})[member]) || ((self.published() || {})[member])) || function(){return '';})();
                if (/^\s*$/.test(d)) {
                    d = (options||{}).notSetText || 'No ' + member + ' provided';
                }
                return d;
            });
        };

        self.makeCollapsedMember('markdown', {notSetText: 'No alert text provided.'});
        self.makeCollapsedMember('alertLevel', {notSetText: 'No alert level specified.'});
        self.makeCollapsedMember('startDateDisplay', {notSetText:'No start date specified.'});
        self.makeCollapsedMember('endDateDisplay', {notSetText:'No end date specified.'});

        self.processing = ko.computed({
            read: function() {
                return self._processing() || (!!self.draft() && self.draft()
                    .processing()) || (!!self.published() && self.published()
                    .processing());
            },
            write: function(value) {
                self._processing(value);
            }
        });

        self.showDiscardDraftModal = ko.observable(false);
        self.discardDraft = function() {
            self.processing(true);
            self.draft()
                .discard(function(err) {
                    self.draft(null);
                    self.processing(false);
                });
        };

        self.showRemoveDocumentModal = ko.observable(false);
        self.remove = function() {
            self.processing(true);
            self.published()
                .remove(function(err) {
                    self.processing(false);
                });
        };

        self.setup = function(data, cb) {
            self.processing(true);
            var tasks = [];
            logger.silly('setting up tuple');
            if (!!data.published) {
                tasks.push(function(cb) {
                    var p = new Published();
                    p.setup(data.published, function(err) {
                        self.published(p);
                        cb(err);
                    });
                });
            }
            if (!!data.draft) {
                tasks.push(function(cb) {
                    var d = new Draft();
                    d.setup(data.draft, function(err) {
                        self.draft(d);
                        cb(err);
                    });
                });
            }

            logger.silly('tupletasks: ' + tasks.length);
            async.parallel(tasks, function(err) {
                self.processing(false);
                cb(err);
            });
        };
    };
    module.exports = exports = tuple;
});
