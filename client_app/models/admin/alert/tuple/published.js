define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        api = require('../../../../api/api'),
        URI = require('URI'),
        URITemplate = require('URITemplate'),
        store = require('store'),
        moment = require('moment');
    require('knockout-validation');

    var alert = function() {
        var self = this;

        self._id = ko.observable();
        self._rangeId = ko.observable();
        self.markdown = ko.observable();
        self.alertLevel = ko.observable();
        self.modified = ko.observable();
        self.isDeleted = ko.observable();
        self.processing = ko.observable(false);
        self.startDate = ko.observable();
        self.endDate = ko.observable();
        self.startForever = ko.observable();
        self.endForever = ko.observable();

        self.lastModifiedDisplay = function() {
            return "Last modified " + moment(self.modified())
                .format('MM/DD/YYYY h:mm a');
        };

        self.markdownDisplay = ko.computed(function() {
            if ((self.markdown() || '') == '') {
                return 'No markdown given.';
            }
            return self.markdown();
        });
        self.shortenedDisplay = ko.computed(function() {
            var shortened = self.markdownDisplay() || '';
            if (shortened == 'No alert text specified.' || shortened.length <= 20) {
                //use as-is.
            } else {
                shortened = shortened.slice(0, 20) + '...';
            }

            return shortened;
        });

        self.setup = function(data, cb) {
            self._id(data._id);
            self._rangeId(data._rangeId);
            self.alertLevel(data.alertLevel);
            self.markdown(data.markdown);
            self.modified(data.modified);

            if (!/forever/i.test(data.startDate)) {
                self.startDate(moment(data.startDate)
                    .toDate());
            } else {
                self.startForever(true);
            }
            if (!/forever/i.test(data.endDate)) {
                self.endDate(moment(data.endDate)
                    .toDate());
            } else {
                self.endForever(true);
            }
            cb();
        };

        self.startDateDisplay = ko.computed(function() {
            if (self.startForever()) {
                return 'forever'
            }
            return moment(self.startDate()).format('MM-DD-YYYY');
        });
        self.endDateDisplay = ko.computed(function() {
            if (self.endForever()) {
                return 'forever'
            }
            return moment(self.endDate()).format('MM-DD-YYYY');
        });

        self.removeQuestion = ko.computed(function() {
            return 'Remove "' + self.shortenedDisplay() + '"?';
        });
        self.remove = function(cb) {
            self.processing(true);
            api.admin.alert.close(self._id(), function(err) {
                self.isDeleted(true);
                if (!!cb) {
                    cb();
                }
                self.processing(false);
            });
        };

        self.createDraft = function() {
            self.processing(true);
            api.admin.alert.create({
                _id: self._id
            }, function(err, data) {
                //redirect to data ID for edit
                require('../../../../app')
                    .router.setLocation('/administration/alerts/' + data._id);
                self.processing(false);
            });
        };
    };
    module.exports = exports = alert;
});
