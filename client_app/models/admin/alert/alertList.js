define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        async = require('async'),
        marked = require('marked'),
        api = require('../../../api/api'),
        Tuple = require('./tuple/tuple'),
        moment = require('moment');
    require('knockout-validation');

    var alertList = function() {
        var self = this;
        self.processing = ko.observable(false);
        self.alerts = ko.observableArray([]);

        self.setup = function(cb) {
            api.admin.alert.list(function(err, alerts) {
                logger.silly('alerts retrieved');
                async.parallel(_.map(alerts, function(alert) {
                    logger.silly('\tmapping...');
                    return function(cb) {
                        var x = new Tuple();
                        x.setup(alert, function(err) {
                            cb(err, x);
                        });
                    };
                }), function(err, r) {
                    logger.silly('done creating alert models');
                    self.alerts(r);
                    cb(err);
                });
            });
        };

        self.shownAlerts = ko.computed(function(){
            return _.filter(self.alerts(), function(alert){
               return alert.show(); 
            }).length;
        });

        self.create = function(id) {
            self.processing(true);
            api.admin.alert.create({
                _id: 'new'
            }, function(err, data) {
                //redirect to data ID for edit
                require('../../../app')
                    .router.setLocation('/administration/alerts/' + data._id);
                self.processing(false);
            });
        };
    };

    exports = module.exports = alertList;
});
