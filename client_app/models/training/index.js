define(function(require, exports, module) {
    module.exports = exports = {
        sectionTypes: require("./sectionTypes/index"),
        Training: require("./training"),
    };
});
