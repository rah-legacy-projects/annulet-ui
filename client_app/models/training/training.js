define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../../logger'),
        ko = require('knockout'),
        async = require('async'),
        toastr = require('toastr'),
        $ = require('jquery'),
        sectionTypes = require('./sectionTypes/index'),
        Chrome = require('../../chrome'),
        api = require('../../api/api');
    require('knockout-validation');

    var training = function(workflowItem) {
        var self = this;
        self.title = ko.observable();
        self._id = ko.observable();
        self._rangeId = ko.observable();

        self.selectedSection = ko.observable();
        self.processing = ko.observable(true);
        self.animationInProgress = ko.observable(false);
        self.animationDelay = ko.observable(9);
        self.sections = ko.observableArray([]);
        self.title = ko.observable();

        self.offCanvasActive = ko.observable(false);

        self.tableOfContents = ko.computed(function() {
            return _.map(self.sections(), function(section) {
                return {
                    title: ko.observable(section.title())
                };
            });
        });

        self.errors = ko.validation.group(self);

        self._selectedSection = ko.computed(function() {
            var trip = self.selectedSection();
            if (self.sections()
                .length > 0 && (!!self.selectedSection() || self.selectedSection() == 0)) {
                self.processing(true);
                self.sections()[self.selectedSection()].views(self.sections()[self.selectedSection()].views() + 1);
                self.sections()[self.selectedSection()]
                    .save(function(err) {
                        if (!err) {
                            self.processing(false);
                        }
                    });
            }
        });

        self.isLast = ko.computed(function() {
            if (!!self.sections && !!self.sections()) {
                return self.selectedSection() == (self.sections()
                    .length - 1);
            }
            return false;
        });

        self.toIndex = function(leavingAnimation, comingAnimation, ix, cb) {
            if (self.animationInProgress()) {
                return;
            }

            var leavingThing = self.sections()[self.selectedSection()];
            var comingThing = self.sections()[ix];
            self.animationInProgress(true);

            async.parallel({
                leaving: function(cb) {
                    logger.silly('leaving - ' + leavingThing.title());
                    leavingThing.animation([leavingAnimation]);
                    leavingThing.toggleState();
                    leavingThing.after(function(animation) {
                        leavingThing.after(function() {});
                        leavingThing.isVisible(false);
                        logger.silly('left - ' + leavingThing.title());
                        cb();
                    });
                },
                coming: function(cb) {
                    setTimeout(function() {
                        logger.silly('coming - ' + comingThing.title());
                        comingThing.animation([comingAnimation]);
                        comingThing.isVisible(true);
                        comingThing.toggleState();
                        logger.silly('coming - state toggled');
                        comingThing.after(function(animation) {
                            logger.silly('came - ' + comingThing.title());
                            comingThing.after(function() {});
                            cb();
                        });
                    }, self.animationDelay());
                }
            }, function(err, r) {
                self.selectedSection(ix);
                self.animationInProgress(false);
                logger.silly('animation complete!')
                if (!!cb) {
                    cb();
                }
            });
        };

        self.goTo = function(ix) {
            //hack: use jquery to scroll to the top of the application div
            $('#application').scrollTop(0);

            logger.silly('going to ' + ix);
            if (ix < self.selectedSection()) {
                self.toIndex('bounceOutRight', 'bounceInLeft', ix);
            } else if (ix > self.selectedSection()) {
                self.toIndex('bounceOutLeft', 'bounceInRight', ix);
            } else {
                //do nothing.  indices are equal.
                //could do a wobble or something.
            }
        };

        self.next = function() {
            if (!self.isLast()) {
                logger.silly('next!');
                self.goTo(self.selectedSection() + 1);
            }
        };
        self.previous = function() {
            //if this is the first section, return
            if (self.selectedSection() == 0) {
                return;
            }
            self.goTo(self.selectedSection() - 1);
        };

        self.finish = function() {
            _.each(self.sections(), function(section) {
                section.enforceErrors(true);
            });

            var everythingViewed = (self.errors()
                .length === 0);
            logger.silly('everything viewed? ' + everythingViewed);

            if (self.allowNext() && everythingViewed) {
                self.processing(true);
                Chrome.processing(true);
                //if so, finish
                logger.silly('finishing - range: ' + self._rangeId());
                api.training.finalize(self._rangeId(), function(err, data) {
                    logger.silly('finish - training finalized');
                    if (!!err) {
                        self.processing(false);
                        logger.error('problem finalizing training: ' + JSON.stringify(err));
                        //todo: nice ui helpers to show what the server issue was
                    } else {
                        self.processing(false);
                        //go to summary
                        require('../../app')
                            .router.setLocation('/workflow/' + data.workflowItem);
                    }
                });
            } else {
                toastr.error('Please make sure you read all sections before completing this training.');
            }
        };
        self.allowNext = ko.computed(function() {
            return true;
        });

        self.setup = function(workflowItem, cb) {
            logger.silly('getting training');
            api.training.getTraining(workflowItem, function(err, data) {


                async.parallel(_.map(data.sections, function(section) {
                    return function(cb) {
                        logger.silly('section type: ' + section.__t);
                        section.type = section.__t || 'content';
                        var sectionTypeName = section.type.split('.')
                            .reverse()[0];
                        sectionTypeName = sectionTypeName.charAt(0)
                            .toUpperCase() + sectionTypeName.slice(1);

                        var createdSection = null;
                        logger.silly('section type: ' + sectionTypeName);
                        if (!!sectionTypes[sectionTypeName]) {
                            //if the type exists, use that constructor to make self
                            createdSection = new sectionTypes[sectionTypeName]();
                        } else {
                            createdSection = new sectionTypes.content();
                        }
                        createdSection.setup(section, function(err) {
                            createdSection.sectionType(sectionTypeName);

                            cb(err, createdSection);
                        });
                    };
                }), function(err, sections) {
                    self.sections(sections);
                    self.title(data.title);
                    self._id(data._id);
                    self._rangeId(data._rangeId);
                    self.selectedSection(0);
                    self.sections()[self.selectedSection()].isVisible(true);
                    self.animationDelay(1000);
                    self.processing(false);

                    if (!!cb) {
                        cb();
                    }
                });
            });
        };
        return self;
    };

    module.exports = exports = training;
});
