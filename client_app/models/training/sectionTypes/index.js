define(function(require, exports, module) {
    module.exports = exports = {
        Content: require("./content"),
        MarkdownContent: require('./markdownContent')
    };
});
