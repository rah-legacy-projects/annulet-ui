define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../../../logger'),
        ko = require('knockout'),
        marked = require('marked'),
        Content = require('./content');
    ko.mapping = require('ko-mapping');
    module.exports = exports = function() {
        var self = new Content();
        self._contentSetup = self.setup;
        //hack: md must be at least a string, otherwise marked bombs out
        self.markdown = ko.observable('');
        self.renderedMarkdown = ko.computed(function() {
            return marked(self.markdown());
        });

        self.setup = function(data, cb) {
            self._contentSetup(data, function(err) {
                self.markdown(data.markdown);
                if (!!cb) {
                    cb();
                }
            });
        };
        return self;
    };
});
