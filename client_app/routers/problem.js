define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.exception = ko.observable();

        app.router.get('/problem/coerce', function(context) {
            Chrome.processing(true);
            app.setTemplateName('problem', function(err) {
                api.problem.coerce(context.params.ticket, function(err, exception) {
                    app.exception(exception);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/problem/:ticket', function(context) {
            Chrome.processing(true);
            app.setTemplateName('problem', function(err) {
                api.problem.detail(context.params.ticket, function(err, exception) {
                    app.exception(exception);
                    Chrome.processing(false);
                });
            });
        });

        
    }
});
