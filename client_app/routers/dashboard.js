define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.dashboard = ko.observable();

        app.router.get('/dashboard', function() {
            logger.silly('getting dashboard');
            app.area('dashboard');
            app.setTemplateName('dashboard', function(err) {
                Chrome.processing(true);
                logger.silly('setting up dashboard');
                var d = new models.Dashboard();
                d.setup(function(err){
                    app.dashboard(d);
                    Chrome.processing(false);
                });
            });
        });
    };
});
