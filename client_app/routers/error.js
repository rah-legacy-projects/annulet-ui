define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        api = require('../api/api'),
        Chrome = require('../chrome'),
        Table = require('../binding_handlers/ko.datatable'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.router.get('/error/unauthorized', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.unauthorized', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/notFound', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.notFound', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/nonpayment', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.nonpayment', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/conflict', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.conflict', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/general', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.general', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/notImplemented', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.notImplemented', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/unavailable', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.unavailable', function(err) {
                Chrome.processing(false);
            });
        });
        app.router.get('/error/timeout', function(context) {
            Chrome.processing(true);
            app.area('error');
            app.setTemplateName('error.timeout', function(err) {
                Chrome.processing(false);
            });
        });
    }
});
