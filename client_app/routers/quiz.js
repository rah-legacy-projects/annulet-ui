define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.quiz = ko.observable();
        app.quizSummary = ko.observable();

        app.router.get('/quiz/:quiz', function(context) {
            Chrome.processing(true);
            logger.silly('getting quiz');
            app.area('quiz');
            app.setTemplateName('quiz', function(err) {
                var q = new models.quiz.Attempt();
                q.setup(context.params.quiz, function(err){
                    logger.silly('putting q into the quiz');
                    app.quiz(q);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/quiz/summary/:attempt', function(context) {
            Chrome.processing(true);
            logger.silly('getting quiz summary');
            app.area('quiz');
            app.setTemplateName('quizSummary', function(err) {
                var qs = new models.quiz.QuizSummary();
                qs.setup(context.params.attempt, function(err){
                    app.quizSummary(qs);
                    Chrome.processing(false);
                });
            });
        });
    };
});
