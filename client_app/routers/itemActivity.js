define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.adminOutstandingOPs = ko.observable();
        app.adminOutstandingTraining = ko.observable();
        app.adminOPActivity = ko.observable();
        app.adminTrainingActivity = ko.observable();

        app.router.get('/admin/outstandingOPs', function(context) {
            Chrome.processing(true);
            app.setTemplateName('itemActivity.adminOutstandingOPs', function(err) {
                var al = new models.itemActivity.AdminOPStatusList();
                al.setup(null, function(err) {
                    app.adminOutstandingOPs(al);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/admin/outstandingTraining', function(context) {
            Chrome.processing(true);
            app.setTemplateName('itemActivity.adminOutstandingTraining', function(err) {
                var al = new models.itemActivity.AdminTrainingStatusList();
                al.setup(null, function(err) {
                    app.adminOutstandingTraining(al);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/admin/opActivity', function(context) {
            Chrome.processing(true);
            app.setTemplateName('itemActivity.adminOPActivity', function(err) {
                var al = new models.itemActivity.AdminOPActivityList();
                al.setup(null, function(err) {
                    app.adminOPActivity(al);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/admin/trainingActivity', function(context) {
            Chrome.processing(true);
            app.setTemplateName('itemActivity.adminTrainingActivity', function(err) {
                var al = new models.itemActivity.AdminTrainingActivityList();
                al.setup(null, function(err) {
                    app.adminTrainingActivity(al);
                    Chrome.processing(false);
                });
            });
        });



    }
});
