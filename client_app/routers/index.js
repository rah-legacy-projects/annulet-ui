define(function(require, exports, module) {
	module.exports = exports = {
		AuxiliaryDocument: require("./auxiliaryDocument"),
		Dashboard: require("./dashboard"),
		ErrorSampler: require("./errorSampler"),
		Workflow: require("./workflow"),
		Quiz: require("./quiz"),
		ForgotPassword: require("./forgotPassword"),
		Account: require("./account"),
		Problem: require("./problem"),
		Error: require("./error"),
		Training: require("./training"),
		ItemActivity: require("./itemActivity"),
		Complaint: require("./complaint"),
		Activation: require("./activation"),
		UserManagement: require("./userManagement"),
		OperatingProcedure: require("./operatingProcedure"),
		Auth: require("./auth"),
		Alert: require("./alert"),
		admin: require("./admin/index"),
	};
});
