define(function(require, exports, module) {
    var ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.forgotPassword = ko.observable();
        app.setPassword = ko.observable();
        app.cancelPasswordMessage = ko.observable();

        app.router.get('/forgot/reset/:slug', function(context) {
            Chrome.processing(true);
            app.setTemplateName('forgotPassword.setPassword', function(err) {
                logger.silly('view retrieved, validating slug...');
                api.forgotPassword.validateSlug(context.params.slug, function(err, data) {
                    app.setPassword(new models.forgotPassword.SetPassword(context.params.slug));
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/forgot/submitted', function() {
            Chrome.processing(true);
            logger.silly('forgot submitted');
            app.setTemplateName('forgotPassword.submitted', function(err) {
                Chrome.processing(false);
            });
        });

        app.router.get('/forgot/success', function() {
            Chrome.processing(true);
            logger.silly('success!');
            app.setTemplateName('forgotPassword.success', function(err) {
                Chrome.processing(false);
            });
        });

        app.router.get('/forgot/cancel/:slug', function(context) {
            Chrome.processing(true);
            logger.silly('canceling');
            app.setTemplateName('forgotPassword.cancel', function(err) {
                api.forgotPassword.cancel(context.params.slug, function(err) {
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/forgot', function() {
            Chrome.processing(true);
            logger.silly('forgot called');
            app.setTemplateName('forgotPassword.forgot', function(err) {
                app.forgotPassword(new models.forgotPassword.ForgotPassword());
                Chrome.processing(false);
            });
        });
    };
});
