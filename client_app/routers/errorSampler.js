define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.router.get('/errorSampler', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            app.setTemplateName('errorSampler', function(err) {
                Chrome.processing(false);
            });
        });


        app.router.get('/errorSampler/401', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get401(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });
        app.router.get('/errorSampler/402', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get402(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });

        app.router.get('/errorSampler/403', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get403(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });

        app.router.get('/errorSampler/404', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get404(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });
        app.router.get('/errorSampler/409', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get409(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });



        app.router.get('/errorSampler/500', function(context) {
            logger.silly('hello from ye olde 500');
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get500(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });
        app.router.get('/errorSampler/501', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get501(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });
        app.router.get('/errorSampler/502', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get502(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });

        app.router.get('/errorSampler/503', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get503(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });
        app.router.get('/errorSampler/504', function(context) {
            Chrome.processing(true);
            app.area('errorSampler');
            api.errorSampler.get504(function(err) {
                logger.error('this should not happen');
                Chrome.processing(false);
            });

        });



    };

});
