define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.account = ko.observable();

        app.router.get('/account', function(context) {
            Chrome.processing(true);
            app.setTemplateName('account', function(err) {
                logger.silly('account template set');
                app.account(new models.accountManagement.AccountManagement());
                app.account().setup(function(err) {
                    logger.silly('account setup complete.');
                    Chrome.processing(false);
                });
            });
        });
    }
});
