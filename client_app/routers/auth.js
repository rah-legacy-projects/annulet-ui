define(function(require, exports, module) {
    var ko = require('knockout'),
        sammy = require('sammy'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.auth = ko.observable(new models.Auth(app, null));
        app.router.get('/login', function() {

            Chrome.processing(true);
            logger.silly('log in called');
            var setview = false;
            if (!app.auth || !app.auth() || !app.auth()
                .loggedIn()) {
                //hack: auth should always exist prior to login...?
                setview = true;
            } else {
                if (!!app.auth()
                    .token()) {
                    logger.silly('redirecting to ' + app.lastLocation());
                    app.router.setLocation(app.lastLocation());
                } else {
                    setview = true;
                }
            }

            if (setview) {
                logger.silly('setting login');
                app.area('login');
                app.setTemplateName('login', function(err) {
                    Chrome.processing(false);
                });
            }else{
                Chrome.processing(false);
            }
        });

        app.router.get('/logout', function() {
            app.auth()
                .logOut();
        });

        app.router.get('/customerSelection', function(){
            Chrome.processing(true);
            logger.silly('getting customer selection view');
            app.setTemplateName('customerSelection', function(err){
                Chrome.processing(false);
                logger.silly('customer selection view retrieved');
            });
        });
    };
});
