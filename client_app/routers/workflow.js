define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.workflow = ko.observable();
        app.workflowList = ko.observable();

        app.router.get('/workflow', function(context) {
            Chrome.processing(true);
            logger.silly('getting wf listing');
            app.setTemplateName('workflow.list', function(err) {
                var ut = new models.workflow.WorkflowList();
                ut.setup(null, function(err) {
                    app.workflowList(ut);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/workflow/byDef/:workflowId', function(context) {
            Chrome.processing(true);
            api.workflow.getWorkflowByDef(context.params.workflowId, function(err, workflow) {
                app.router.setLocation('/workflow/' + workflow._id);
            });
        });

        app.router.get('/workflow/:workflowId', function(context) {
            Chrome.processing(true);
            logger.silly('getting workflow');
            app.area('workflow');
            app.setTemplateName('workflow.detail', function(err) {
                var wf = new models.workflow.Workflow();
                wf.setup(context.params.workflowId, function(err) {
                    app.workflow(wf);
                    Chrome.processing(false);
                });
            });
        });
    };
});
