define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.operatingProcedure = ko.validatedObservable();
        app.operatingProcedureList = ko.observable();

        app.router.get('/operatingProcedure', function(context) {
            Chrome.processing(true);
            logger.silly('getting operatingProcedure list');
            app.area('operatingProcedure');
            app.setTemplateName('operatingProcedure.list', function(err) {
                logger.silly('creating operatingProcedure observable');
                var op = new models.operatingProcedure.OperatingProcedureList();
                op.setup(function(err) {
                    app.operatingProcedureList(op);
                    Chrome.processing(false);
                });

            });
        });

        app.router.get('/operatingProcedure/byDef/:operatingProcedure', function(context) {
            Chrome.processing(true);
            //stamp the OPs for this user by calling into the user OP list
            var op = new models.operatingProcedure.OperatingProcedureList();
            op.setup(function(err) {
                //find the op in the op list with the passed-in definition
                var i = _.find(op.operatingProcedures(), function(p){
                    return p.operatingProcedureDefinition() == context.params.operatingProcedure;
                });
                //redirect to /operatingProcedure/:instanceId
                app.router.setLocation('/operatingProcedure/'+i._id());
            });
        });

        app.router.get('/operatingProcedure/:operatingProcedure', function(context) {
            Chrome.processing(true);
            logger.silly('getting operatingProcedure');
            app.area('operatingProcedure');
            app.setTemplateName('operatingProcedure.detail', function(err) {
                logger.silly('creating operatingProcedure observable');
                var op = new models.operatingProcedure.OperatingProcedure();
                op.setup(context.params.operatingProcedure, function(err) {
                    app.operatingProcedure(op);
                    Chrome.processing(false);
                });

            });
        });
    };
});
