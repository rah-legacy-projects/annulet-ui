define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.alertList = ko.observable();

        app.router.get('/alerts', function(context) {
            Chrome.processing(true);
            app.setTemplateName('alert.list', function(err) {
                var al = new models.alert.AlertInstanceList();
                al.setup(null, function(err) {
                    app.alertList(al);
                    Chrome.processing(false);
                });
            });
        });
    }
});
