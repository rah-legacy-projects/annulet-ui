define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../../logger'),
        Chrome = require('../../chrome'),
        api = require('../../api/api'),
        models = require('../../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app = _.defaultsDeep(app, {
            admin: {
                auxiliaryDocument: {}
            }
        });
        app.admin.auxiliaryDocument.list = ko.observable();
        app.admin.auxiliaryDocument.detail = ko.observable();

        app.router.get('/administration/auxiliaryDocuments', function(context) {
            logger.silly('going to auxdox admin list!');
            app.area('auxiliaryDocuments');
            Chrome.processing(true);
            app.setTemplateName('admin.auxiliaryDocument.list', function(err) {
                var al = new models.admin.auxiliaryDocument.AuxiliaryDocumentList();
                al.setup(function(err) {
                    app.admin.auxiliaryDocument.list(al);
                    logger.silly('aux dox list set');
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/administration/auxiliaryDocuments/:id', function(context) {
            app.area('auxiliaryDocuments');
            Chrome.processing(true);
            app.setTemplateName('admin.auxiliaryDocument.detail', function(err) {
                var al = new models.admin.auxiliaryDocument.tuple.Draft();
                //hack; for now, reuse the tuple setup
                api.admin.auxiliaryDocument.loadTuple(context.params.id, function(err, tuple) {
                    al.setup(tuple.draft, function(err) {
                        app.admin.auxiliaryDocument.detail(al);
                        Chrome.processing(false);
                    });
                });
            });
        });
    }
});
