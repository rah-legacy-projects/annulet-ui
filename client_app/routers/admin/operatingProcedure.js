define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../../logger'),
        Chrome = require('../../chrome'),
        models = require('../../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.admin.operatingProcedureList = ko.observable();

        app.router.get('/administration/operatingProcedure', function(context) {
            Chrome.processing(true);
            logger.silly('admin - getting operatingProcedure list');
            app.area('operatingProcedure');
            app.setTemplateName('admin.operatingProcedure.list', function(err) {
                logger.silly('creating operatingProcedure observable');
                var op = new models.admin.operatingProcedure.OperatingProcedureList();
                op.setup(function(err) {
                    app.admin.operatingProcedureList(op);
                    Chrome.processing(false);
                });

            });
        });
    };
});
