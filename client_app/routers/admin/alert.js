define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../../logger'),
        Chrome = require('../../chrome'),
        api = require('../../api/api'),
        models = require('../../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app = _.defaultsDeep(app, {
            admin: {
                alert: {}
            }
        });
        app.admin.alert.list = ko.observable();
        app.admin.alert.detail = ko.observable();

        app.router.get('/administration/alerts', function(context) {
            logger.silly('going to auxdox admin list!');
            app.area('alerts');
            Chrome.processing(true);
            app.setTemplateName('admin.alert.list', function(err) {
                var al = new models.admin.alert.AlertList();
                al.setup(function(err) {
                    app.admin.alert.list(al);
                    logger.silly('aux dox list set');
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/administration/alerts/:id', function(context) {
            app.area('alerts');
            Chrome.processing(true);
            app.setTemplateName('admin.alert.detail', function(err) {
                var al = new models.admin.alert.tuple.Draft();
                //hack; for now, reuse the tuple setup
                api.admin.alert.loadTuple(context.params.id, function(err, tuple) {
                    al.setup(tuple.draft, function(err) {
                        app.admin.alert.detail(al);
                        Chrome.processing(false);
                    });
                });
            });
        });
    }
});
