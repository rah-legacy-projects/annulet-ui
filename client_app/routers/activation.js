define(function(require, exports, module) {
    var ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.activation = ko.observable();
        app.activationSetPassword = ko.observable();
        app.activationCancelPasswordMessage = ko.observable();
        
        app.router.get('/activation/success', function() {
            Chrome.processing(true);
            logger.silly('success!');
            app.setTemplateName('activation.success', function(err) {
                Chrome.processing(false);
            });
        });

        app.router.get('/activation/:slug', function(context) {
            Chrome.processing(true);
            app.setTemplateName('activation.setPassword', function(err) {
                logger.silly('view retrieved, validating slug...');
                api.activation.validateSlug(context.params.slug, function(err, data) {
                    logger.silly('slug validated.');
                    app.activationSetPassword(new models.activation.SetPassword(context.params.slug));
                    Chrome.processing(false);
                });
            });
        });

    };
});
