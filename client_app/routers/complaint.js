define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        Table = require('../binding_handlers/ko.datatable'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.complaintList = ko.observable();
        app.complaint = ko.observable();

        app.router.get('/complaints', function(context) {
            Chrome.processing(true);
            logger.silly('getting complaint listing');
            app.area('complaint');
            app.setTemplateName('complaintList', function(err) {
                if(!app.complaintList()){
                    app.complaintList(new Table({
                        //rowModel: models.complaint.Complaint,
                        rowCreator: function(data, cb){
                            var c = new models.complaint.Complaint();
                            c.setup(data, function(err){
                                cb(err, c);
                            });
                        },
                        dataMethod: function(parameters, cb){
                            logger.silly(JSON.stringify(parameters));
                            api.complaint.listComplaints(parameters, cb);
                        },
                        defaultSortBy:'created'
                    }));
                }
                Chrome.processing(false);
            });
        });
        
        app.router.get('/complaint/new', function(context){
            Chrome.processing(true)
            logger.silly('new complaint');
            app.area('complaint');
            app.setTemplateName('complaintDetail', function(err){
                var newComplaint = new models.complaint.Complaint();
                newComplaint.isOpenForEdit(true);
                app.complaint(newComplaint);
                Chrome.processing(false);
            });
        });

        app.router.get('/complaint/:complaint', function(context){
            Chrome.processing(true);
            logger.silly('getting complaint');
            app.area('complaint');
            app.setTemplateName('complaintDetail', function(err){
                api.complaint.getComplaint(context.params.complaint, function(err, complaint){
                    var c = new models.complaint.Complaint();
                    c.setup(complaint, function(err){
                        app.complaint(c);
                        Chrome.processing(false);
                    });
                });
            });
        });

        
    };
});
