define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.training = ko.validatedObservable();

        app.router.get('/training/:workflowItem', function(context) {
            Chrome.processing(true);
            logger.silly('getting training');
            app.area('training');
            app.setTemplateName('training', function(err) {
                var t = new models.training.Training();
                t.setup(context.params.workflowItem, function(err) {
                    app.training(t);
                    Chrome.processing(false);
                });
            });
        });
    };
});
