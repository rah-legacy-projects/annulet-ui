define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    require('knockout-validation');
    module.exports = exports = function(app) {
        app.auxiliaryDocumentList = ko.observable();
        app.auxiliaryDocumentAdminList = ko.observable();

        app.router.get('/auxiliaryDocuments', function(context) {
            app.area('auxiliaryDocuments');
            Chrome.processing(true);
            app.setTemplateName('auxiliaryDocument.list', function(err) {
                var al = new models.auxiliaryDocument.AuxiliaryDocumentList();
                al.setup(function(err) {
                    app.auxiliaryDocumentList(al);
                    Chrome.processing(false);
                });
            });
        });

        

    }
});
