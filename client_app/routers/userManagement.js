define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        api = require('../api/api'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.userManagement = ko.observable();
        app.userToEdit = ko.observable();

        app.router.get('/userManagement', function(context) {
            Chrome.processing(true);
            app.area('userManagement');
            app.setTemplateName('userManagement.list', function(err) {
                var um = new models.userManagement.UserManagement();
                um.setup(function(err) {
                    app.userManagement(um);
                    Chrome.processing(false);
                });
            });
        });

        app.router.get('/userManagement/edit/:user', function(context) {
            Chrome.processing(true);
            app.area('userManagement');
            app.setTemplateName('userManagement.edit', function(err) {
                var u = new models.userManagement.User();
                api.userManagement.getUser(context.params.user, function(err, data) {
                    u.setup(new models.userManagement.UserManagement, data, function(err) {
                        app.userToEdit(u);
                        Chrome.processing(false);
                    });
                });
            });
        });

        app.router.get('/userManagement/create', function(context) {
            Chrome.processing(true);
            app.area('userManagement');
            app.setTemplateName('userManagement.edit', function(err) {
                var u = new models.userManagement.User();
                u.setup(new models.userManagement.UserManagement, null, function(err) {
                    u.isNew(true);
                    app.userToEdit(u);
                    Chrome.processing(false);
                });
            });
        });
    };
});
