    require.config({
        waitSeconds: 45,
        paths: {
            jquery: '/bower_components/jquery/dist/jquery',
            'jquery-ui': '/bower_components/jquery-ui/jquery-ui',
            sammy: '/bower_components/sammy/lib/sammy',
            knockout: '/bower_components/knockout/dist/knockout',
            'sammy-cache': '/bower_components/sammy/lib/plugins/sammy.cache',
            'sammy-store': '/bower_components/sammy/lib/plugins/sammy.storage',
            moment: '/bower_components/moment/moment',
            URI: '/bower_components/uri.js/src/URI',
            URITemplate: '/bower_components/uri.js/src/URITemplate',
            'ko-date': 'binding_handlers/date',
            'ko-datePicker': 'binding_handlers/datePicker',
            'ko-mapping': '/bower_components/knockout-mapping/knockout.mapping',
            'ko.stringTemplateEngine': '/client_app/binding_handlers/ko.stringTemplateEngine',
            'ko-heightHack': '/client_app/binding_handlers/ko.heightHack',
            punycode: '/bower_components/uri.js/src/punycode',
            IPv6: '/bower_components/uri.js/src/IPv6',
            SecondLevelDomains: '/bower_components/uri.js/src/SecondLevelDomains',
            async: '/bower_components/async/lib/async',
            sprintf: '/bower_components/sprintf/src/sprintf',
            log: '/bower_components/log/log',
            hello: '/bower_components/hello/dist/hello',
            lodash: '/bower_components/lodash/lodash',
            bootstrap: '/bower_components/bootstrap/dist/js/bootstrap',
            store: '/bower_components/store-js/store',
            json: '/bower_components/json3/lib/json3',
            'underscore.string': '/bower_components/underscore.string/lib/underscore.string',
            stripe: [
                'https://js.stripe.com/v2/stripe',
                '/client_app/fallbacks/stripe'
            ],
            'knockout-validation': '/bower_components/Knockout-Validation/dist/knockout.validation',
            'marked': '/bower_components/marked/lib/marked',
            'bs3-datetime-picker': '/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',
            modernizr: '/bower_components/modernizr/modernizr',
            toastr: '/bower_components/toastr/toastr',
            'knockout-animate': '/bower_components/knockout-animate/src/knockout.animate',
            knockstrap: '/bower_components/knockstrap/build/knockstrap',
            dropzone: '/bower_components/dropzone/dist/dropzone-amd-module',
            //blockUI: '/bower_components/blockui/jquery.blockUI',
            //'ko-blockUI': 'binding_handlers/ko.blockUI'
        },
        shim: {
            'ko-mapping': {
                deps: ['knockout'],
                exports: 'ko-mapping'
            },
            'knockout-validation': {
                deps: ['knockout'],
                exports: 'knockout-validation'
            },
            URI: {
                deps: ['punycode', 'IPv6', 'SecondLevelDomains'],
                exports: 'URI'
            },
            store: {
                deps: ['json'],
                exports: "store"
            },

            bootstrap: ['jquery'],
            'underscore-string': ['lodash'],

            'bs3-datetime-picker': ['jquery', 'moment', 'bootstrap'],

            //'blockUI': ['jquery'],
            //'ko-blockUI': ['jquery', 'knockout', 'blockUI']
        },
    });

    require(['./spa-setup']);
