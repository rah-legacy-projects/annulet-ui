define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {

        var getView = function(errcode, cb) {
            var endpoint = '';

            endpoint = new URITemplate(new URI(apiUriString)
                .valueOf() + 'errorSampler/{errcode}')
                .expand({
                    errcode: errcode,
                });

            $.ajax({
                url: endpoint,
                type: 'GET',
                dataType: 'text',
                headers: {
                    'annulet-auth-token': store.get('auth-token'),
                    'annulet-auth-customer': store.get('auth-customer')
                }
            })
                .done(function(data) {
                    cb(data.errors, data);
                })
                .error(function(err) {
                    Chrome.handleError(null, err, cb);
                });
        };

        return {
            get401: function(cb) {
                getView('401', cb)
            },
            get402: function(cb) {
                getView('402', cb)
            },
            get403: function(cb) {
                getView('403', cb)
            },
            get404: function(cb) {
                getView('404', cb)
            },
            get409: function(cb) {
                getView('409', cb)
            },
            get500: function(cb) {
                getView('500', cb)
            },
            get501: function(cb) {
                getView('501', cb)
            },
            get502: function(cb) {
                getView('502', cb)
            },
            get503: function(cb) {
                getView('503', cb)
            },
            get504: function(cb) {
                getView('504', cb)
            },
        }
    }
});
