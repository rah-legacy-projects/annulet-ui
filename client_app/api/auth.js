define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(authUriString) {

        return {
            login: function(email, password, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(authUriString)
                        .valueOf() + '/login')
                        .expand(),
                    data: {
                        email: email,
                        password: password
                    }
                })
                    .done(function(data) {
                        logger.silly('data: ' + JSON.stringify(data));
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        logger.error('problem logging in: ' + JSON.stringify(err));
                        Chrome.handleError({isLogin:true}, err, cb);
                    });
            },
            logout: function(cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(authUriString)
                        .valueOf() + '/logout')
                        .expand(),
                    headers: {
                        'annulet-auth-token': store.get('auth-token')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    });
            },
            getUserInfo: function(cb) {
                logger.silly('getting user info: ' + store.get('auth-token'));
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(authUriString)
                        .valueOf() + '/getUserInfo')
                        .expand(),
                    headers: {
                        'annulet-auth-token': store.get('auth-token')
                    }
                })
                    .done(function(data) {
                        logger.silly('got user info');
                        logger.silly(JSON.stringify(data));
                        logger.silly('callback: ' + !!cb + ', ' + _.isFunction(cb));

                        logger.silly('calling callback');
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        store.set('auth-token', null);
                        Chrome.handleError(null, err, cb);
                    });
            }
        };
    }
});
