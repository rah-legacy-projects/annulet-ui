define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            validateSlug: function(slug, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + '/activation/validateSlug/{slug}')
                        .expand({
                            slug: slug
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            changePassword: function(slug, options, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + '/activation/change/{slug}')
                        .expand({slug: slug}),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                    data: options
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            }
        }
    }
});
