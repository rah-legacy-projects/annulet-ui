define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            getUsers: function(cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'admin/userManagement')
                        .expand({}),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        logger.silly('[um api] got users');
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            getUser: function(user, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'admin/userManagement/{user}')
                        .expand({
                            user: user
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        logger.silly('[um api] got user');
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            saveUser: function(user, roles, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'admin/userManagement/save')
                        .expand({}),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                    data: {
                        user: user,
                        roles: roles
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            removeUser: function(user, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'admin/userManagement/remove')
                        .expand({}),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                    data: {
                        user: user,
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            }
        }
    }
});
