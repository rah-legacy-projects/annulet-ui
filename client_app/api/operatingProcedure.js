define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            getOperatingProcedureList: function(cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'operatingProcedure/list')
                        .expand({}),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer'),
                        'annulet-user-agent': navigator.userAgent
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            getOperatingProcedure: function(operatingProcedure, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'operatingProcedure/{operatingProcedure}')
                        .expand({
                            operatingProcedure: operatingProcedure
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            saveView: function(options, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'operatingProcedure/section/{sectionType}/{sectionRange}')
                        .expand({
                            sectionType: options.sectionType,
                            sectionRange: options.sectionRange
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            finalize: function(operatingProcedureRange, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'operatingProcedure/finalize/{operatingProcedureRange}')
                        .expand({
                            operatingProcedureRange:operatingProcedureRange
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
        };

    }
});
