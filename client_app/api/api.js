define(function(require, exports, module) {
    var async = require('async'),
        logger = require('../logger');

    var api = new(function() {
        var self = this;

        self.setup = function(cb) {
            async.parallel({
                    baseApiUriString: function(cb) {
                        $.get('/apiuri', function(baseUriString) {
                            if (!baseUriString) {
                                baseUriString = 'http://api.annulet.io';
                            }
                            cb(null, baseUriString);
                        })
                    },
                    domain: function(cb) {
                        $.get('/domain', function(domain) {
                            if (!domain) {
                                domain = 'annulet.io';
                            }
                            cb(null, domain);
                        })
                    },
                    authUriString: function(cb) {
                        $.get('/authuri', function(baseUriString) {
                            if (!baseUriString) {
                                baseUriString = 'http://auth.annulet.io';
                            }
                            logger.silly('auth uri: ' + baseUriString);
                            cb(null, baseUriString);
                        })
                    },
                },
                function(err, r) {
                    logger.silly('setting document.domain to: ' + r.domain)

                    //set up the domain...
                    document.domain = r.domain;

                    self.domain = r.domain;
                    self.baseApiUriString = r.baseApiUriString;
                    self.authUriString = r.authUriString;
                    
                    self.views = require('./views')();
                    self.auth = require('./auth')(self.authUriString);
                    self.customer = require('./customer')(self.baseApiUriString);
                    self.workflow = require('./workflow')(self.baseApiUriString);
                    self.quiz = require('./quiz')(self.baseApiUriString);
                    self.training = require('./training')(self.baseApiUriString);
                    self.userManagement = require('./userManagement')(self.baseApiUriString);
                    self.operatingProcedure = require('./operatingProcedure')(self.baseApiUriString);
                    self.complaint = require('./complaint')(self.baseApiUriString);
                    self.errorSampler = require('./errorSampler')(self.baseApiUriString);
                    self.alert = require('./alert')(self.baseApiUriString);
                    self.account = require('./account')(self.baseApiUriString);
                    self.auxiliaryDocument = require('./auxiliaryDocument')(self.baseApiUriString);
                    self.forgotPassword = require('./forgotPassword')(self.authUriString);
                    self.activation = require('./activation')(self.authUriString);
                    self.problem = require('./problem')(self.baseApiUriString);
                    self.quickView = require('./quickView')(self.baseApiUriString);

                    self.admin = {};
                    self.admin.operatingProcedure = require('./admin/operatingProcedure')(self.baseApiUriString);
                    self.admin.auxiliaryDocument = require('./admin/auxiliaryDocument')(self.baseApiUriString);
                    self.admin.alert = require('./admin/alert')(self.baseApiUriString);
                    cb(err);
                });
        };

        return self;

    })();

    module.exports = exports = api;
});
