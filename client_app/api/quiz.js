define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            getQuiz: function(workflowItem, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'quiz/{workflowItem}')
                        .expand({
                            workflowItem: workflowItem
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            getQuizSummary: function(attempt, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'quiz/summary/{attempt}')
                        .expand({
                            attempt: attempt
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            saveAnswer: function(options, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'quiz/answer/{questionType}/{question}')
                        .expand({
                            questionType: options.questionType,
                            question: options.question
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                    data: {
                        answers: options.answers
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            finalize: function(attempt, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'quiz/finalize/{attempt}')
                        .expand({
                            attempt:attempt
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            }
        };

    }
});
