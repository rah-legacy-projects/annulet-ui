define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(viewUriString) {

        var getView = function(viewName, cb) {
            logger.silly('[view api] getting ' + viewName);
            logger.silly('[view api] baseuri: ' + viewUriString);
            var endpoint = '';

            if (!!viewUriString) {
                endpoint = new URITemplate(new URI(viewUriString)
                    .valueOf() + '/views/{viewName}')
                    .expand({
                        viewName: viewName,
                    });
            } else {
                endpoint = '/views/' + viewName;
            }
            logger.silly('[view api] endpoint: ' + endpoint.toString());
            logger.silly('[view api] auth token: ' + store.get('auth-token'));

            $.ajax({
                url: endpoint,
                type: 'GET',
                dataType: 'text',
                headers: {
                    'annulet-auth-token': store.get('auth-token'),
                    'annulet-auth-customer': store.get('auth-customer')
                }
            })
                .done(function(data) {
                    logger.silly('[view api] view retrieved');
                    cb(data.errors, data);
                })
                .error(function(err) {
                    Chrome.handleError(null, err, cb);
                });
        };

        return {
            dashboard: function(cb) {
                logger.silly('getting dashboard');
                getView('dashboard', cb)
            },
            login: function(cb) {
                logger.silly('getting login'),
                getView('login', cb);
            },
            customerSelection: function(cb) {
                getView('customerSelection', cb);
            },
            workflow: {
                detail: function(cb) {
                    logger.silly('getting workflow');
                    getView('workflow/detail', cb);
                },
                list: function(cb) {
                    logger.silly('getting wf list');
                    getView('workflow/list', cb);
                }
            },
            quiz: function(cb) {
                logger.silly('getting quiz');
                getView('quiz', cb);
            },
            quizSummary: function(cb) {
                logger.silly('getting quiz summary');
                getView('quiz/summary', cb);
            },
            training: function(cb) {
                logger.silly('getting training');
                getView('training', cb);
            },
            customerSelection: function(cb) {
                logger.silly('getting customer selection');
                getView('customerSelection', cb);
            },
            userManagement: {
                list: function(cb) {
                    logger.silly('getting user management');
                    getView('userManagement/list', cb);
                },
                edit: function(cb) {
                    getView('userManagement/edit', cb);
                }
            },
            operatingProcedure: {
                detail: function(cb) {
                    getView('operatingProcedure/detail', cb);
                },
                list: function(cb) {
                    getView('operatingProcedure/list', cb);
                }
            },
            complaintList: function(cb) {
                logger.silly('getting complaint list');
                getView('complaintList', cb);
            },
            complaintDetail: function(cb) {
                logger.silly('getting complaint detail');
                getView('complaintDetail', cb);
            },
            error: {
                unauthorized: function(cb) {
                    getView('error/unauthorized', cb);
                },
                notFound: function(cb) {
                    getView('error/notFound', cb);
                },
                nonpayment: function(cb) {
                    getView('error/nonpayment', cb);
                },
                conflict: function(cb) {
                    getView('error/conflict', cb);
                },
                general: function(cb) {
                    getView('error/general', cb);
                },
                notImplemented: function(cb) {
                    getView('error/notImplemented', cb);
                },
                unavailable: function(cb) {
                    getView('error/unavailable', cb);
                },
                timeout: function(cb) {
                    getView('error/timeout', cb);
                }
            },
            errorSampler: function(cb) {
                getView('errorSampler', cb);
            },
            alert: {
                list: function(cb) {
                    getView('alert/list', cb);
                }
            },
            alertAdministration: {
                list: function(cb) {
                    logger.silly('alert admin list');
                    getView('alertAdministration/list', cb);
                },
                detail: function(cb) {
                    getView('alertAdministration/detail', cb);
                }
            },
            account: function(cb) {
                logger.silly('account');
                getView('account', cb);
            },
            auxiliaryDocument: {
                list: function(cb) {
                    getView('auxiliaryDocument/list', cb);
                },
                adminList: function(cb) {
                    logger.silly('getting auxdox admin listing');
                    getView('auxiliaryDocument/admin/list', cb);
                },

            },
            forgotPassword: {
                setPassword: function(cb) {
                    logger.silly('getting set password view');
                    getView('forgotPassword/set', cb);
                },
                submitted: function(cb) {
                    getView('forgotPassword/submitted', cb);
                },
                success: function(cb) {
                    getView('forgotPassword/success', cb);
                },
                forgot: function(cb) {
                    getView('forgotPassword/forgot', cb);
                },
                cancel: function(cb) {
                    getView('forgotPassword/cancel', cb);
                }
            },
            activation: {
                setPassword: function(cb) {
                    logger.silly('getting set password view');
                    getView('activation/set', cb);
                },
                success: function(cb) {
                    getView('activation/success', cb);
                },
            },
            problem: function(cb) {
                getView('problem', cb);
            },
            itemActivity: {
                adminOutstandingOPs: function(cb) {
                    getView('itemActivity/adminOutstandingOPs', cb);
                },
                adminOutstandingTraining: function(cb) {
                    getView('itemActivity/adminOutstandingTraining', cb);
                },
                adminOPActivity: function(cb) {
                    getView('itemActivity/adminOPActivity', cb);
                },
                adminTrainingActivity: function(cb) {
                    getView('itemActivity/adminTrainingActivity', cb);
                }
            },
            admin: {
                operatingProcedure: {
                    list: function(cb) {
                        getView('admin/operatingProcedure/list', cb);
                    }
                },
                auxiliaryDocument: {
                    list: function(cb) {
                        getView('admin/auxiliaryDocument/list', cb);
                    },
                    detail: function(cb) {
                        getView('admin/auxiliaryDocument/detail', cb);
                    },
                },
                alert: {
                    list: function(cb) {
                        getView('admin/alert/list', cb);
                    },
                    detail: function(cb) {
                        getView('admin/alert/detail', cb);
                    },
                }
            }
        };
    }
});
