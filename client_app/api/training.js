define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        Chrome = require('../chrome'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            getTraining: function(workflowItem, cb) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'training/{workflowItem}')
                        .expand({
                            workflowItem: workflowItem
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            saveView: function(options, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'training/section/{sectionType}/{section}')
                        .expand({
                            sectionType: options.sectionType,
                            section: options.section
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            },
            finalize: function(rangeId, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'training/finalize/{rangeId}')
                        .expand({
                            rangeId: rangeId
                        }),
                    headers: {
                        'annulet-auth-token': store.get('auth-token'),
                        'annulet-auth-customer': store.get('auth-customer')
                    },
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        Chrome.handleError(null, err, cb);
                    });
            }
        };

    }
});
