define(function(require, exports, module) {
	module.exports = exports = {
		AuxiliaryDocument: require("./auxiliaryDocument"),
		OperatingProcedure: require("./operatingProcedure"),
		Alert: require("./alert"),
	};
});
