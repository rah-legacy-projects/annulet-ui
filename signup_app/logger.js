define(function(require, exports, module) {
    var logger = new(function() {
        var self = {};

        var log = require('log'),
            _ = require('lodash'),
            sprintf = require('sprintf')
            .sprintf;

        self.options = {
            level:'silly'
        };

        self.setup = function(options) {
            self.options = _.defaults(self.options, {
                level: 'silly',
            });
        };

        var levels = {
            silly: 0,
            debug: 100,
            info: 200,
            warn: 300,
            error: 400,
            fatal: 500,
            silent: 1000
        };

        self.silly = function(msg, parts) {
            if (levels.silly >= levels[self.options.level]) {
                log(sprintf('[c="color: purple"]' + msg + '[c]', parts));
            }
        };
        self.debug = function(msg, parts) {
            if (levels.debug >= levels[self.options.level]) {
                log(sprintf('[c="color: blue"]' + msg + '[c]', parts))
            }
        };
        self.info = function(msg, parts) {
            if (levels.info >= levels[self.options.level]) {
                log(sprintf('[c="color: green"]' + msg + '[c]', parts));
            }
        };
        self.warn = function(msg, parts) {
            if (levels.warn >= levels[self.options.level]) {
                log(sprintf('[c="color: yellow; background:black"]' + msg + '[c]', parts));
            }
        };
        self.error = function(msg, parts) {
            if (levels.error >= levels[self.options.level]) {
                log(sprintf('[c="color: red"]' + msg + '[c]', parts));
            }
        };
        self.fatal = function(msg, parts) {
            if (levels.fatal >= levels[self.options.level]) {
                log(sprintf('[c="color: black; background:red"]*' + msg + '*[c]', parts));
            }
        };
        return self;
    })();
    module.exports = exports = logger;
});
