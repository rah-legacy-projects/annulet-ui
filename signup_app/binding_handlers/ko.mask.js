define(function(require, exports, module) {
    var $ = require('jquery'),
        jqmasked = require('jquery.maskedinput'),
        ko = require('knockout');

    ko.bindingHandlers.mask = {
        init: function(element, valueAccessor, allBindingsAccessor) {
            var mask = allBindingsAccessor()
                .mask || {};
            $(element)
                .mask(mask, {
                    autoclear: false,
                    placeholder: ' '
                });
        },
    };
});
