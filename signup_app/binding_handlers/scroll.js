// --------------------------------------------------------
//	Navigation Bar
// -------------------------------------------------------- 	
define(function(require, exports, module) {
    var $ = require('jquery');
    $(window)
        .scroll(function() {
            "use strict";
            var scroll = $(window)
                .scrollTop();
            if (scroll > 60) {
                $(".navbar")
                    .addClass("scroll-fixed-navbar");
            } else {
                $(".navbar")
                    .removeClass("scroll-fixed-navbar");
            }
        });

    //hack: close menus on click
    $(".dropdown-menu a")
        .click(function() {
            $(this)
                .closest(".dropdown-menu")
                .prev()
                .dropdown("toggle");
        });
});
