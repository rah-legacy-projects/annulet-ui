define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        _ = require('lodash'),
        async = require('async');
    ko.mapping = require('ko-mapping');

    var AdvancedSearchModel = function(parameters) {
        var self = this;

        self.members = ko.observableArray(ko.mapping.fromJS(parameters.members));
        self.selectedMember = ko.observable();

        self.search = ko.observable();

        self.getSearchTerm = function() {
            if (!!self.selectedMember()) {
                return {
                    on: self.selectedMember()
                        .memberName(),
                    search: self.search()
                };
            }
            return null;
        };

        return self;

    };

    var TableModel = function(parameters) {

        //parameters = {
        //    dataMethodPath: dataMethodPath,
        //    rowModel: rowModel,
        //    rowCreator: rowCreator
        //    defaultSortBy: defaultSortBy,
        //    getServerParameterCallback: getServerParameterCallback,
        //    afterPageSelectCallback: afterPageSelectCallback,
        //    noItemsFoundDisplay: noItemsFoundDisplay,
        //    verb: 'POST|GET|PUT' etc.  Defaults to GET.
        //    dataType: 'json|jsonp' Defautls to json.
        //    members: [{memberName: name, displayName: display}] of member names.  Won't allow advanced search without it.
        //};

        var self = this;
        self.parameters = parameters;
        self.getServerParameterCallback = self.parameters.getServerParameterCallback;
        self.rowModel = self.parameters.rowModel;
        self.rowCreator = self.parameters.rowCreator;
        self.dataMethod = self.parameters.dataMethod;
        self.echo = ko.observable(1);
        self.processing = ko.observable(false);
        self.advancedSearch = ko.observable(false);



        self.firstBindComplete = ko.computed(function() {
            return self.echo() > 1;
        });
        self.selectedPage = ko.observable(1);
        self.totalRecords = ko.observable(0);
        self.filteredRecords = ko.observable(0);


        //how many to display/stats
        self.displayRecordOptions = ko.observableArray([
            '5', '10', '25', '50', '100'
        ]);
        self._SelectedDisplayRecordOption = ko.observableArray(['5']);
        self.selectedDisplayRecordOption = ko.computed({
            read: function() {
                return self._SelectedDisplayRecordOption();
            },
            write: function(value) {
                self._SelectedDisplayRecordOption(value);
                if (self.firstBindComplete()) {
                    //one of the side effects of options:/value: is that it persists the value option
                    //to what it is bound to, which would cause the first bind action to happen twice
                    //at class instantiation time.
                    self.pageSelect(self.selectedPage());
                }
            },
            owner: self
        });

        self.from = ko.computed(function() {
            if (self.totalRecords() === 0)
                return 0;
            return ((self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10)) - parseInt(self.selectedDisplayRecordOption(), 10)) + 1;
        });
        self.to = ko.computed(function() {
            var retval = (self.selectedPage() * parseInt(self.selectedDisplayRecordOption(), 10));
            if (retval > self.totalRecords()) {
                return self.totalRecords();
            }
            return retval;
        });

        //sorting
        self.sortBy = ko.observable(self.parameters.defaultSortBy);
        self.order = ko.observable('ascending');
        self.setSortBy = function(sortBy) {
            if (sortBy === self.sortBy()) {
                if (self.order() === 'ascending') {
                    self.order('descending');
                } else {
                    self.order('ascending');
                }
            } else {
                self.order('ascending');
            }
            self.sortBy(sortBy);
            self.pageSelect(self.selectedPage());
        };

        //search bar
        self._search = ko.observable('');
        self._previousSearch = ko.observable({});
        self._timer = null;

        self.search = ko.computed({
            read: function() {
                return self._search();
            },
            write: function(value) {
                if (self._previousSearch() === null || self._previousSearch() !== value) {
                    if (self._timer !== null) {
                        window.clearTimeout(self._timer);
                    }
                    self._previousSearch(self._search());
                    self._search(value);
                    self._timer = window.setTimeout(function() {
                        self.pageSelect(self.selectedPage());
                    }, 1250);
                }
            },
            owner: self
        });

        self.canDoAdvancedSearch = ko.observable((parameters.members || [])
            .length > 0);
        self.advancedSearch = ko.observableArray([]);
        self.addAdvancedSearch = function() {
            self.advancedSearch.push(new AdvancedSearchModel({
                members: parameters.members
            }));
        };
        self.applyAdvancedSearch = function() {
            self.pageSelect(self.selectedPage());
        };

        //data
        self.rows = ko.observableArray([]);

        self.visibleRows = ko.computed(function() {
            return self.rows();
        });

        //pagination
        self.pages = ko.computed(function() {
            var pages = [];
            for (var i = 1; i <= Math.ceil(self.filteredRecords() / self.selectedDisplayRecordOption()); i++) {
                pages.push(i);
            }

            return pages;
        });

        self.selectablePages = ko.computed(function() {
            var page = self.selectedPage() - 1;
            var displayPages = 5;
            var sliceStart = 0,
                sliceEnd = displayPages;

            if (self.pages()
                .length > 5 && page > (Math.floor(displayPages / 2))) {
                if (page + Math.ceil(displayPages / 2) > self.pages()
                    .length) {
                    sliceStart = self.pages()
                        .length - 5;
                    sliceEnd = self.pages()
                        .length;
                } else {
                    sliceStart = page - Math.floor(displayPages / 2);
                    sliceEnd = page + Math.ceil(displayPages / 2);
                }
            }
            if (self.pages()
                .length < self.selectedPage()) {
                self.selectedPage(1);
            }
            return self.pages()
                .slice(sliceStart, sliceEnd);
        });

        self.first = function() {
            if (!self.processing()) {
                self.pageSelect(1);
            }
        };
        self.last = function() {
            if (!self.processing()) {
                self.pageSelect(self.pages()
                    .length);
            }
        };
        self.next = function() {
            if (!self.processing()) {
                if (self.selectedPage() + 1 <= self.pages()
                    .length) {
                    self.pageSelect(self.selectedPage() + 1);
                }
            }
        };
        self.previous = function() {
            if (!self.processing()) {
                console.log(self.selectedPage() + ' to ' + (self.selectedPage() - 1))
                if (self.selectedPage() - 1 > 0) {
                    self.pageSelect(self.selectedPage() - 1);
                }
            }
        };

        self.processing = ko.observable(false);

        self.processResult = function(data) {
            if (self.echo() === parseInt(data.echo, 10)) {
                self.filteredRecords(data.filteredRecords);
                self.totalRecords(data.totalRecords);
                //remove all rows currently shown
                self.rows.removeAll();

                async.waterfall([

                    function(cb) {
                        //add all rows returned by the data method
                        if (data.items !== undefined && data.items !== null) {
                            //if the row model exists, use that
                            if (!!self.rowModel) {
                                $.each(data.items, function(index, row) {
                                    self.rows.push(new self.rowModel(row));
                                });
                                cb();
                            } else if (!!self.rowCreator) {
                                async.parallel(_.map(data.items, function(item) {
                                    return function(cb) {
                                        self.rowCreator(item, function(err, row) {
                                            cb(err, row);
                                        });
                                    };
                                }), function(err, r) {
                                    self.rows(r);
                                    cb();
                                });
                            } else {
                                //otherwise, use the mappping plugin to map the results
                                self.rows(ko.mapping.fromJS(data.items));
                                cb();
                            }
                        }
                    },
                    function(cb) {
                        self.echo(parseInt(data.echo, 10) + 1);

                        if (self.parameters.afterPageSelectCallback !== undefined && self.parameters.afterPageSelectCallback !== null) {
                            self.parameters.afterPageSelectCallback();
                        }

                        cb();
                    }

                ], function(err, p) {
                    self.processing(false);
                });

            }
        };

        self.pageSelect = function(page) {
            if (self.processing()) {
                return;
            }
            self.processing(true);
            self.selectedPage(page);

            var dataMethodParameters = {
                echo: self.echo(),
                search: (!!self.advancedSearch() && self.advancedSearch()
                    .length > 0) ? null : self.search(),
                advancedSearch: self.advancedSearch() ? _.map(self.advancedSearch(), function(as) {
                    return as.getSearchTerm();
                }) : null,
                limit: parseInt(self.selectedDisplayRecordOption(), 10),
                skip: ((self.selectedPage() - 1) * parseInt(self.selectedDisplayRecordOption(), 10)),
                orderByClauses: [{
                    orderByName: self.sortBy(),
                    orderByDirection: self.order()
                }]
            };

            if (!!self.getServerParametersCallback) {
                var parameters = self.getServerParameterCallback();
                for (var parameter in parameters) {
                    dataMethodParameters[parameter] = parameters[parameter];
                }
            }

            if (_.isString(self.dataMethod)) {
                $.ajax({
                    type: self.parameters.verb || 'GET',
                    contentType: "application/json; charset=utf-8",
                    url: self.dataMethod,
                    data: dataMethodParameters,
                    dataType: self.parameters.dataType || "json",
                })
                    .done(function(data) {
                        self.processResult(data);
                    })
                    .fail(function(i, e) {
                        self.processing(false);
                        console.log('setup error: ' + e);
                    });
            } else if (_.isFunction(self.dataMethod)) {
                self.dataMethod(dataMethodParameters, function(err, data) {
                    self.processResult(data);
                });
            }
        };
        self.noItemsFound = ko.computed(function() {
            return self.visibleRows()
                .length === 0;
        });
        if (!!parameters.noItemsFoundDisplay && parameters.noItemsFoundDisplay != -1 && parameters.noItemsFoundDisplay.jquery !== undefined) {
            //if the item is a jquery selector, use the html innards
            self.noItemsHTML = ko.observable(parameters.noItemsFoundDisplay.html());
        } else {
            //otherwise, use jquery to get the html from whatever was passed
            //wrap in a div to fake out 'outerhtml' behavior
            self.noItemsHTML = ko.observable($('<div>')
                .append($(parameters.noItemsFoundDisplay))
                .html());
        }


        /*
        //why is this causing infinite request?
        self._advancedSearchWatch = ko.computed(function() {
            //ensures that switching from/to advanced search refreshes the results
            self.advancedSearch();
            self.pageSelect(self.selectedPage());
        });
        */
        //finally, invoke the first page selection
        self.pageSelect(1);
    };




    module.exports = exports = TableModel;
});
