define(function(require, exports, module) {
    var ko = require('ko'),
        $ = require('jquery');
    ko.bindingHandlers.date = {
        update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
            var value = valueAccessor();
            var allBindings = allBindingsAccessor();
            var valueUnwrapped = ko.utils.unwrapObservable(value);

            // Date formats: http://momentjs.com/docs/#/displaying/format/
            var pattern = allBindings.format || 'MM/DD/YYYY HH:mm:ss';

            var output = "-";
            if (valueUnwrapped !== null && valueUnwrapped !== undefined && (!isNaN(valueUnwrapped) || valueUnwrapped.length > 0)) {
                if (isNaN(valueUnwrapped)) {
                    output = moment(valueUnwrapped)
                        .format(pattern);
                } else {
                    output = moment(new Date(valueUnwrapped))
                        .format(pattern);
                }
            }

            if ($(element)
                .is("input") === true) {
                $(element)
                    .val(output);
            } else {
                $(element)
                    .text(output);
            }
        }
    };

});
