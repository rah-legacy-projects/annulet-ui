define(function(require, exports, module) {
    var sammy = require('sammy'),
        store = require('store'),
        ko = require('knockout'),
        URI = require('URI'),
        _ = require('lodash'),
        models = require('./models/index'),
        routers = require('./routers/index'),
        async = require('async'),
        logger = require('./logger'),
        Chrome = require('./chrome'),
        api = require('./api/api');

    require('ko.stringTemplateEngine');
    require('./binding_handlers/ko.heightHack');
    require('./binding_handlers/ko.mask');

    var app = new(function() {
        var self = this;

        self.Chrome = Chrome;

        self.initializing = ko.observable(true);

        self.offCanvasActive = ko.observable(false);
        self.area = ko.observable();
        //self.problemAreas = ko.observableArray([]);
        self.ownerIssue = ko.observable(false);
        self.companyIssue = ko.observable(false);
        self.paymentIssue = ko.observable(false);

        self.otherIssue = ko.observable();

        self.feedback = ko.observable();

        self.setup = function(cb) {
            self.getTemplate = function(value, cb) {
                logger.silly('looking for ' + value);
                if (!ko.templates[value]) {
                    logger.silly('template not found, retrieving');
                    var view = value.split('.')
                        .reduce(function(obj, i) {
                                return obj[i];
                            }, api
                            .views)
                    if (!!view) {
                        logger.silly('view route exists.');
                        view(function(err, templateString) {
                            logger.silly('view retrieved');
                            ko.templates[value] = templateString;
                            cb(null);
                        });
                    } else {
                        logger.warn('view ' + value + ' not found!');
                        cb('notFound');
                    }
                } else {
                    cb(null);
                }

            };

            self.setTemplateName = function(value, cb) {
                logger.debug('[set template] ' + value);
                self.getTemplate(value, function(err) {
                    if (!!err) {
                        self._templateName(err);
                    } else {
                        self._templateName(value);
                    }
                    if (!!cb) {
                        cb(null);
                    }
                });
            };

            self._templateName = ko.observable();
            self.templateName = ko.computed({
                read: function() {
                    var trip = self._templateName();
                    if (!self.initializing.peek()) {
                        logger.silly('retrieving template name: ' + self._templateName());
                        if (!self._templateName()) {
                            logger.warn('template name not defined, going to not found ');
                            self.setTemplateName('notFound');
                        } else {
                            return self._templateName();
                        }
                    }
                    return null;
                },
                write: function(value) {
                    throw Error('Do not attempt to set template name directly, use setTemplateName instead')
                }
            });

            self.router = sammy();
            self.router.get('/', function() {
                this.redirect('/dashboard');
            });

            async.series({
                apis: function(cb) {
                    routers.Signup(self);
                    cb(null);
                },
            }, function(err) {
                logger.silly('initializing complete!');
                logger.silly('running route');
                self.router.run();
                self.initializing(false);
                logger.silly('init set to false');
                cb(err);

            });
        };
        return self;
    })();

    logger.silly('application created.');
    module.exports = exports = app;
});
