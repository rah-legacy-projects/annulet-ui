define(function(require, exports, module) {
    var _ = require('lodash'),
        ko = require('knockout'),
        async = require('async'),
        logger = require('../logger'),
        Chrome = require('../chrome'),
        models = require('../models/index');

    module.exports = exports = function(app) {
        app.signup = ko.observable();

        app.router.get('/companyInformation', function(context) {
            app.area('companyInformation');
            app.setTemplateName('companyInformation', function(err) {
                Chrome.processing(true);
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/disclaimer', function(context) {
            app.area('disclaimer');
            Chrome.processing(true);
            app.setTemplateName('disclaimer', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/privacy', function(context) {
            app.area('privacy');
            Chrome.processing(true);
            app.setTemplateName('privacy', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });
        
        app.router.get('/ownerInformation', function(context) {
            app.area('ownerInformation');
            Chrome.processing(true);
            app.setTemplateName('ownerInformation', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/payment', function(context) {
            app.area('payment');
            Chrome.processing(true);
            app.setTemplateName('payment', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/termsOfUse', function(context) {
            app.area('termsOfUse');
            Chrome.processing(true);
            app.setTemplateName('termsOfUse', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/signup', function(context) {
            app.area('signup');
            Chrome.processing(true);
            app.setTemplateName('signup', function(err) {
                if (!app.signup()) {
                    app.signup(new models.Signup());
                }
                    Chrome.processing(false);
            });
        });

        app.router.get('/done', function(context) {
            logger.silly('done?');
            Chrome.processing(true);
            if (!app.signup()) {
                logger.silly('no app signup, going to signup');
                app.router.setLocation('/signup');
            } else if (!app.signup().stripeResponse()) {
                logger.silly('no stripe response, going to signup');
                app.router.setLocation('/signup');
            } else {
                logger.silly('setting up done area');
                app.area('done');
                app.setTemplateName('done', function(err) {});
                    Chrome.processing(false);
            }
        });

    };
});
