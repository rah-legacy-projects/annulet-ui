    require.config({
        paths: {
            jquery: '/bower_components/jquery/dist/jquery',
            'jquery-ui': '/bower_components/jquery-ui/jquery-ui',
            sammy: '/bower_components/sammy/lib/sammy',
            knockout: '/bower_components/knockout/dist/knockout',
            'sammy-cache': '/bower_components/sammy/lib/plugins/sammy.cache',
            'sammy-store': '/bower_components/sammy/lib/plugins/sammy.storage',
            URI: '/bower_components/uri.js/src/URI',
            URITemplate: '/bower_components/uri.js/src/URITemplate',
            'ko.stringTemplateEngine': '/signup_app/binding_handlers/ko.stringTemplateEngine',
            punycode: '/bower_components/uri.js/src/punycode',
            IPv6: '/bower_components/uri.js/src/IPv6',
            SecondLevelDomains: '/bower_components/uri.js/src/SecondLevelDomains',
            async: '/bower_components/async/lib/async',
            sprintf: '/bower_components/sprintf/src/sprintf',
            log: '/bower_components/log/log',
            lodash: '/bower_components/lodash/lodash',
            bootstrap: '/bower_components/bootstrap/dist/js/bootstrap',
            store: '/bower_components/store-js/store',
            json: '/bower_components/json3/lib/json3',
            stripe: 'https://js.stripe.com/v2/stripe',
            'knockout-validation': '/bower_components/Knockout-Validation/dist/knockout.validation',
            toastr: '/bower_components/toastr/toastr',
            modernizr: '/bower_components/modernizr/modernizr',
            'knockout-animate': '/bower_components/knockout-animate/src/knockout.animate',
            knockstrap: '/bower_components/knockstrap/build/knockstrap',
            'jquery.maskedinput':'/bower_components/jquery.maskedinput/dist/jquery.maskedinput'
        },
        shim: {
            'ko-mapping': {
                deps: ['knockout'],
                exports: 'ko-mapping'
            },
            'knockout-validation': {
                deps: ['knockout'],
                exports: 'knockout-validation'
            },
            URI: {
                deps: ['punycode', 'IPv6', 'SecondLevelDomains'],
                exports: 'URI'
            },
            store: {
                deps: ['json'],
                exports: "store"
            },

            bootstrap: ['jquery'],
            'underscore-string': ['lodash'],
            'jquery.maskedinput':['jquery']
        },
    });

    require(['./spa-setup']);
