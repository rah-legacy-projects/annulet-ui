define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(apiUriString) {
        return {
            createCustomer: function(options, cb) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: new URITemplate(new URI(apiUriString)
                        .valueOf() + 'signup')
                        .expand({}),
                    data: {
                        customer: options.customer,
                        user: options.user,
                        stripe: options.stripe
                    }
                })
                    .done(function(data) {
                        cb(data.err, data.data);
                    })
                    .error(function(err) {
                        cb(err.responseJSON.err);
                    });
            }
        };

    }
});
