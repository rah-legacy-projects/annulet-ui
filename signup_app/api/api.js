define(function(require, exports, module) {
    var async = require('async'),
        stripe = require('stripe'),
        logger = require('../logger');

    var api = new(function() {
        var self = this;

        self.setup = function(cb) {
            async.parallel({
                    baseApiUriString: function(cb) {
                        $.get('/apiuri', function(baseUriString) {
                            if (!baseUriString) {
                                baseUriString = 'http://api.annulet.io';
                            }
                            cb(null, baseUriString);
                        })
                    },
                    domain: function(cb) {
                        $.get('/domain', function(domain) {
                            if (!domain) {
                                domain = 'annulet.io';
                            }
                            cb(null, domain);
                        })
                    },
                    authUriString: function(cb) {
                        $.get('/authuri', function(baseUriString) {
                            if (!baseUriString) {
                                baseUriString = 'http://auth.annulet.io';
                            }
                            logger.silly('auth uri: ' + baseUriString);
                            cb(null, baseUriString);
                        })
                    },
                    stripePublicKey: function(cb) {
                        $.get('/stripekey', function(stripeKey) {
                            cb(null, stripeKey);
                        })
                    }
                },
                function(err, r) {
                    logger.silly('setting document.domain to: ' + r.domain)

                    //set up the domain...
                    document.domain = r.domain;

                    self.domain = r.domain;
                    self.baseApiUriString = r.baseApiUriString;
                    self.authUriString = r.authUriString;
                    self.stripePublicKey = r.stripePublicKey;
                    stripe.setPublishableKey(r.stripePublicKey);

                    self.views = require('./views')();
                    self.signup = require('./signup')(self.baseApiUriString);

                    cb(err);
                });
        };

        return self;

    })();

    module.exports = exports = api;
});
