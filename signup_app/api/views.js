define(function(require, exports, module) {
    var logger = require('../logger'),
        URI = require('URI'),
        $ = require('jquery'),
        store = require('store'),
        URITemplate = require('URITemplate');

    module.exports = exports = function(viewUriString) {

        var getView = function(viewName, cb) {
            console.log(viewName);
            logger.silly('[view api] getting ' + viewName);
            logger.silly('[view api] baseuri: ' + viewUriString);
            var endpoint = '';

            if (!!viewUriString) {
                endpoint = new URITemplate(new URI(viewUriString)
                    .valueOf() + '/views/{viewName}')
                    .expand({
                        viewName: viewName,
                    });
            } else {
                endpoint = '/views/' + viewName;
            }
            logger.silly('[view api] endpoint: ' + endpoint.toString());

            $.ajax({
                url: endpoint,
                type: 'GET',
                dataType: 'text',
            })
                .done(function(data) {
                    logger.silly('[view api] view retrieved');
                    cb(data.errors, data);
                });
        };

        return {
            signup: function(cb) {
                getView('signup', cb);
            },
            companyInformation: function(cb) {
                getView('signup/companyInformation', cb);
            },
            privacy: function(cb) {
                getView('signup/privacy', cb);
            },
            disclaimer: function(cb) {
                getView('signup/disclaimer', cb);
            },
            ownerInformation: function(cb) {
                getView('signup/ownerInformation', cb);
            },
            payment: function(cb) {
                getView('signup/payment', cb);
            },
            termsOfUse: function(cb) {
                getView('signup/termsOfUse', cb);
            },
            done:function(cb){
                getView('signup/done',cb);

            }
        };
    }
});
