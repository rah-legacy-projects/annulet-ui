define(function(require, exports, module) {
    var store = require('store'),
        _ = require('lodash'),
        async = require('async'),
        logger = require('./logger'),
        api = require('./api/api'),
        ko = require('knockout');


    var Chrome = function() {
        var self = this;
        self.processing = ko.observable(true);
        self.error = ko.observable();


        self.handleError = function(options, err, cb) {
            //only require app when needed
            var app = require('./app');
            self.error(err);
            switch (err.status) {
                case 401:
                case 403:
                    if (!!options && options.isLogin) {
                        cb(err);
                        break;
                    } else {
                        app.router.setLocation('/error/unauthorized');
                        break;
                    }
                case 404:
                    app.router.setLocation('/error/notFound');
                    break;
                case 402:
                    app.router.setLocation('/error/nonpayment');
                    break;
                case 409:
                    app.router.setLocation('/error/conflict');
                    break;
                case 500:
                    if (!!options && options.isLogin) {
                        cb(err);
                        break;
                    } else {
                        //is error fatal?
                        //if (!!err.fatal) {
                        app.router.setLocation('/error/general');
                        break;
                        //}
                    }
                    //if not, fall through to default case
                case 501:
                    app.router.setLocation('/error/notImplemented');
                    break;
                case 502:
                case 503:
                    app.router.setLocation('/error/unavailable');
                    break;
                case 504:
                    app.router.setLocation('/error/timeout');
                    break;
                default:
                    if (!!cb) {
                        cb(err);
                    } else {
                        app.router.setLocation('/error/general');
                    }
                    break;
            }
        };


    };

    module.exports = exports = new Chrome();

});
