define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../logger'),
        ko = require('knockout'),
        User = require('./user'),
        Customer = require('./customer'),
        CreditCard = require('./creditCard'),
        api = require('../api/api'),
        toastr = require('toastr'),
        stripe = require('stripe'),
        Chrome = require('../chrome');

    logger.silly('creating signup');

    require('knockout-validation');
    var signup = function() {
        var app = require('../app');
        var self = this;
        self.user = ko.validatedObservable(new User());
        self.customer = ko.validatedObservable(new Customer());
        self.creditCard = ko.validatedObservable(new CreditCard());


        self.enforceErrors = ko.computed({
            read: function() {
                return self.creditCard()
                    .enforceErrors() && self.customer()
                    .enforceErrors() && self.user()
                    .enforceErrors();
            },
            write: function(value) {
                logger.silly('setting show errors: ' + value);
                self.creditCard()
                    .enforceErrors(value);
                self.customer()
                    .enforceErrors(value);
                self.user()
                    .enforceErrors(value);
            }
        });
        self.processing = ko.observable(false);

        self.stripeResponse = ko.observable();

        self.submitToServer = function(cb) {
            //do *not* include the credit card data on purpose
            //only include the stripe data

            //hack: use the signup code from the cc and give it to the customer
            var customer = ko.toJS(self.customer);
            customer.signupCode = self.creditCard().signupCode();

            api.signup.createCustomer({
                customer: customer,
                user: ko.toJS(self.user),
                stripe: ko.toJS(self.stripeResponse)
            }, function(err, r) {
                self.processing(false);
                cb(err);
            })
        };

        self.submitToStripe = function(cb) {
            stripe.card.createToken({
                number: self.creditCard()
                    .creditCardNumber(),
                cvc: self.creditCard()
                    .cvc(),
                exp_month: self.creditCard()
                    .expiryMonth()
                    .value,
                exp_year: self.creditCard()
                    .expiryYear()
                    .value
            }, function(status, response) {
                if (!!response.error) {
                    logger.silly('stripe problem: ' + response.error.message);
                    logger.error(response.error);
                    self.creditCard()
                        .paymentErrors(response.error.message);
                    toastr.error('Problem processing payment: ' + response.error.message);
                    cb(response.error);
                } else {
                    self.stripeResponse(response);
                    cb(null);
                }
            });
        };

        self.finish = function() {
            toastr.options = {
                'cloaseButton': true,
                'positionClass': 'toast-top-full-width',
                'preventDuplicates': false,
                'showEasing': 'swing',
                'hideEasing': 'linear',
                'showMethod': 'fadeIn',
                'hideMethod': 'fadeOut',
                'tapToDismiss': true,
                'timeOut': 5000,
                'extendedTimeOut': 5000
            };

            logger.silly('finishing!');
            self.processing(true);
            self.creditCard()
                .paymentErrors(null);

            self.enforceErrors(true);
            if (self.creditCard.isValid() && self.user.isValid() && self.customer.isValid()) {
                Chrome.processing(true);
                self.submitToStripe(function(err, r) {
                    if (!err) {
                        logger.silly('no error, submitting to server');
                        self.submitToServer(function(err, r) {
                            if (!err) {
                                require('../app')
                                    .router.setLocation('/signup/done');
                            } else {
                                //todo: handle server error
                                app.otherIssue('There was an issue saving customer and user information.  Please contact support.');
                            }
                            self.processing(false);
                            Chrome.processing(false);
                        });
                    } else {
                        logger.silly('there was a problem: ' + JSON.stringify(err));
                        //app.otherIssue('There was an issue processing payment.  Please try again later.');
                        self.processing(false);
                        Chrome.processing(false);

                    }
                });
            } else {
                self.creditCard.errors.showAllMessages();
                self.customer.errors.showAllMessages();
                self.user.errors.showAllMessages();

                logger.silly(!!app.ownerIssue);

                app.ownerIssue(!self.user.isValid());
                app.companyIssue(!self.customer.isValid());
                app.paymentIssue(!self.creditCard.isValid());

                logger.silly(self.creditCard.errors());
                logger.silly(self.creditCard()
                    .paymentErrors());
                logger.silly(self.creditCard.isValid() + ' ' + self.user.isValid() + ' ' + self.customer.isValid());

                toastr.error('Please correct the highlighted areas before finishing.');
                self.processing(false);
            }
        };
    };

    module.exports = exporst = signup;
});
