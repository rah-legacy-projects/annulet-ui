define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../logger'),
        ko = require('knockout'),
        api = require('../api/api');

    require('ko.stringTemplateEngine');
    var customer = function() {
        var self = this;
        self.name = ko.observable();
        self.address = {
            street1: ko.observable(),
            street2: ko.observable(),
            city: ko.observable(),
            state: ko.observable(),
            zip: ko.observable()
        };
        self.phone = ko.observable();
        self.numberOfLocations = ko.observable(3);
        self.numberOfEmployees = ko.observable(3);

        self.enforceErrors = ko.observable(false);

        self.name.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.street1.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.city.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.state.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.address.zip.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.phone.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.numberOfLocations.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            number: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.numberOfEmployees.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            number: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        return self;
    };

    module.exports = exports = customer;
});
