define(function(require, expots, module) {
    var _ = require('lodash'),
        logger = require('../logger'),
        ko = require('knockout'),
        api = require('../api/api');

    require('ko.stringTemplateEngine');
    var user = function() {
        var self = this;
        self.email = ko.observable();
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.password = ko.observable();
        self.passwordAgain = ko.observable();

        self.enforceErrors = ko.observable(false);

        self.email.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            email: {
                params: true,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });
        self.firstName.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.lastName.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.password.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
        });
        self.passwordAgain.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                }
            },
            equal: {
                params: self.password,
                onlyIf: function() {
                    return self.enforceErrors();
                }
            }
        });

        return self;
    };

    module.exports = exports = user;
});
