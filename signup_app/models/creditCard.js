define(function(require, exports, module) {
    var _ = require('lodash'),
        logger = require('../logger'),
        ko = require('knockout'),
        api = require('../api/api'),
        stripe = require('stripe');

    require('knockout-validation');

    var creditCard = function() {
        var self = this;

        self.paymentErrors = ko.observable();
        self.creditCardNumber = ko.observable();
        self.cvc = ko.observable();
        self.expiryMonth = ko.observable();
        self.expiryYear = ko.observable();
        self.signupCode = ko.observable();
        self.enforceErrors = ko.observable(false);


        self.paymentErrors.extend({
            validation: {
                validator: function(validatedValue, validatedAgainst) {
                    return !validatedValue;
                },
                onlyIf: function() {
                    return self.enforceErrors();
                },
            }
        });

        self.creditCardNumber.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
            minLength: {
                params: 16,
                onlyIf: function() {
                    logger.silly('\t\tcc check show: ' + self.enforceErrors());
                    return self.enforceErrors();
                },
            }
        });
        self.cvc.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
            minLength: {
                params: 2,
                onlyIf: function() {
                    return self.enforceErrors();
                },
            }
        });

        self.expiryMonth.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
        });
        self.expiryYear.extend({
            required: {
                onlyIf: function() {
                    return self.enforceErrors();
                },
            },
        });

        self.months = ko.observableArray([{
            value: '01',
            display: 'Jan (01)'
        }, {
            value: '02',
            display: 'Feb (02)'
        }, {
            value: '03',
            display: 'Mar (03)'
        }, {
            value: '04',
            display: 'Apr (04)'
        }, {
            value: '05',
            display: 'May (05)'
        }, {
            value: '06',
            display: 'Jun (06)'
        }, {
            value: '07',
            display: 'Jul (07)'
        }, {
            value: '08',
            display: 'Aug (08)'
        }, {
            value: '09',
            display: 'Sep (09)'
        }, {
            value: '10',
            display: 'Oct (10)'
        }, {
            value: '11',
            display: 'Nov (11)'
        }, {
            value: '12',
            display: 'Dec (12)'
        }]);

        self.years = ko.observableArray([]);
        var year = (new Date())
            .getFullYear();
        for (var i = 0; i < 10; i++) {
            self.years.push({
                value: (year + i)
                    .toString(),
                display: (year + i)
                    .toString()
            });
        }



        return self;
    };

    module.exports = exports = creditCard;
});
