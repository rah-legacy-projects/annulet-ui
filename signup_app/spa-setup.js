define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        bs3 = require('bootstrap'),
        logger = require('./logger'),
        app = require('./app'),
        api = require('./api/api'),
        stripe = require('stripe');

        require('knockout-validation');
    api.setup(function(err) {
        logger.setup();
        logger.silly('logger set up');
    
        logger.silly('stripe public key set');
        ko.validation.init({
            errorElementClass: 'has-error',
            errorMessageClass: 'help-block',
            decorateElement: true,

            messagesOnModified:false,
            decorateElementOnModified:false,
            grouping:{deep:true}
        });
        app.setup(function(err) {
            ko.applyBindings(app, $('#application')
                .get(0));
            logger.silly('bindings applied');
        });
    });
});
