var logger = require('winston');

module.exports = exports = {
    privacy: function(req, res) {
        res.render('signup/privacy');
    },
    disclaimer: function(req, res) {
        res.render('signup/warrantyDisclaimer');
    },
    payment: function(req, res) {
        res.render('signup/payment');
    },
    ownerInformation: function(req, res) {
        res.render('signup/ownerInformation');
    },
    termsOfUse: function(req, res) {
        res.render('signup/termsOfUse');
    },
    signup: function(req, res) {
        res.render('signup/overview');
    },
    done: function(req, res) {
        res.render('signup/done');
    },
    companyInformation: function(req, res){
        res.render('signup/companyInformation');
    }

};
