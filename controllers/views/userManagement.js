var logger = require('winston');

module.exports = exports = {
    list: function(req, res) {
        res.render('userManagement/index');
    },
    edit: function(req, res){
        res.render('userManagement/edit');
    }
};
