var logger = require('winston');

module.exports = exports = {
    list: function(req, res) {
        res.render('admin/auxiliaryDocument/list');
    },
    detail:function(req, res){
        res.render('admin/auxiliaryDocument/detail');
    },
};
