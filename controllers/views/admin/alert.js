var logger = require('winston');

module.exports = exports = {
    list: function(req, res) {
        res.render('admin/alert/list');
    },
    detail:function(req, res){
        res.render('admin/alert/detail');
    },
};
