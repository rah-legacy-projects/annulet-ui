var logger = require('winston');

module.exports = exports = {
    list: function(req, res) {
        logger.silly('getting op admin list');
        res.render('admin/operatingProcedure/list');
    },
};
