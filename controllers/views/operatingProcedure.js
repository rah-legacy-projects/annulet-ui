var logger = require('winston');

module.exports = exports = {
    detail: function(req, res) {
        res.render('operatingProcedure/detail');
    },
    list: function(req, res) {
        logger.silly('getting op list');
        res.render('operatingProcedure/list');
    },
};
