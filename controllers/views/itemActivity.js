var logger = require('winston');

module.exports = exports = {
    adminOutstandingOPs: function(req, res){
        res.render('itemActivity/adminOutstandingOPs');
    },
    adminOutstandingTraining: function(req, res){
        res.render('itemActivity/adminOutstandingTraining');
    },
    adminOPActivity: function(req, res){
        res.render('itemActivity/adminOPActivity');
    },
    adminTrainingActivity: function(req, res){
        res.render('itemActivity/adminTrainingActivity');
    }
};
