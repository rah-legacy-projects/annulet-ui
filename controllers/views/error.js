var logger = require('winston');

module.exports = exports = {
    unauthorized: function(req, res) {
        res.render('error/unauthorized');
    },
    notFound: function(req, res) {
        res.render('error/notFound');
    },
    nonpayment: function(req, res) {
        res.render('error/nonpayment');
    },
    conflict: function(req, res) {
        res.render('error/conflict');
    },
    general: function(req, res) {
        res.render('error/general');
    },
    notImplemented: function(req, res) {
        res.render('error/notImplemented');
    },
    unavailable: function(req, res) {
        res.render('error/unavailable');
    },
    timeout: function(req, res) {
        res.render('error/timeout');
    },

};
