var logger = require('winston');

module.exports = exports = {
    detail: function(req, res) {
        res.render('workflow/index');
    },
    list: function(req, res) {
        res.render('workflow/list');
    }
};
