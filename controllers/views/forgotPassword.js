var logger = require('winston');

module.exports = exports = {
    setPassword: function(req, res) {
        res.render('forgotPassword/setPassword');
    },
    submitted: function(req, res) {
        res.render('forgotPassword/submitted');
    },
    success: function(req, res) {
        res.render('forgotPassword/success');
    },
    forgot: function(req, res) {
        res.render('forgotPassword/forgot');
    },
    cancel: function(req, res) {
        res.render('forgotPassword/cancel');
    },
};
