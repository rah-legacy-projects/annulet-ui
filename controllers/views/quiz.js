var logger = require('winston');

module.exports = exports = {
    get: function(req, res) {
        res.render('quiz/index');
    },
    getSummary: function(req, res) {
        res.render('quiz/quizSummary');
    }

};
