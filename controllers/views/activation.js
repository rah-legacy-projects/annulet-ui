var logger = require('winston');

module.exports = exports = {
    setPassword: function(req, res) {
        res.render('activation/setPassword');
    },
    success: function(req, res) {
        res.render('activation/success');
    },
};
