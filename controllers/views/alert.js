var logger = require('winston');

module.exports = exports = {
    instanceList: function(req, res) {
        res.render('alert/instanceList');
    },
    definitionList:function(req, res){
        res.render('alert/definitionList');
    },
    definitionDetail: function(req, res){
        res.render('alert/definitionDetail');
    }
};
