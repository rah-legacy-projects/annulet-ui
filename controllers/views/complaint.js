var logger = require('winston');

module.exports = exports = {
    list: function(req, res) {
        res.render('complaint/list');
    },
    detail: function(req, res) {
        res.render('complaint/detail');
    }

};
