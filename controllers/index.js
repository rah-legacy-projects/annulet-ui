module.exports = exports = {
	clientSetup: require("./clientSetup"),
	views: require("./views"),
	signup: require('./signup'),
	health: require('./health')
};
