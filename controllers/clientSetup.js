var config = require('annulet-config');

module.exports.home = function(req, res) {
    res.render('home/index');
};

module.exports.apiUri = function(req, res) {
    res.json(config.configuration
        .paths.apiUri());
};

module.exports.domain = function(req, res) {
    res.json(config.configuration
        .paths.domain());
};

module.exports.authUri = function(req, res){
    res.json(config.configuration.paths.authUri());
};

module.exports.stripePublicKey = function(req,res){
    res.json(config.configuration.stripe.publicKey());
};
