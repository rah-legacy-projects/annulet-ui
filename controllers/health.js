var heapdump = require('heapdump'),
    path = require('path'),
    fs = require('fs');
module.exports = exports = {
    get: function(req, res) {
        res.status(200)
            .json({
                status: 'healthy'
            });
    },
    heapdump: function(req, res) {
        heapdump.writeSnapshot(path.resolve(__dirname, '..', 'heapdumps', Date.now() + '.heapsnapshot'), function(err, file) {
            if (!!err) {
                res.status(500)
                    .json(err);
            } else {
                res.status(200)
                    .json({
                        message: 'Heap dumped to ' + file
                    });
            }
        });
    },
    forceGC: function(req, res){
        if(typeof(gc)=='function'){
            gc();
            return res.status(200).json({message: 'gc ran'});
        }

        return res.status(404).json({message: 'gc not exposed'});
    }
};
